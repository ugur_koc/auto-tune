#!/bin/bash

for (( i = 1; i <= 5; i++ )); do
	python3 ggnn_sparse.py --data_dir data --valid_file cbmc-test-$i.json --train_file cbmc-train-$i.json
done