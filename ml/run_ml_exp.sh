#!/opt/local/bin/bash

java -jar autotuneML.jar symbiotic pca-off conservative regression 5.0 4573
java -jar autotuneML.jar symbiotic pca-off conservative regression 5.0 2348
java -jar autotuneML.jar symbiotic pca-off conservative regression 5.0 1037
java -jar autotuneML.jar symbiotic pca-off conservative regression 5.0 4352
java -jar autotuneML.jar symbiotic pca-off conservative regression 5.0 6732
java -jar autotuneML.jar symbiotic pca-off conservative regression 5.0 9671
java -jar autotuneML.jar symbiotic pca-off conservative regression 5.0 4932

java -jar autotuneML.jar symbiotic pca-on conservative regression 5.0 4573
java -jar autotuneML.jar symbiotic pca-on conservative regression 5.0 2348
java -jar autotuneML.jar symbiotic pca-on conservative regression 5.0 1037
java -jar autotuneML.jar symbiotic pca-on conservative regression 5.0 4352
java -jar autotuneML.jar symbiotic pca-on conservative regression 5.0 6732
java -jar autotuneML.jar symbiotic pca-on conservative regression 5.0 9671
java -jar autotuneML.jar symbiotic pca-on conservative regression 5.0 4932

java -jar autotuneML.jar symbiotic pca-off conservative classification 5.0 2348
java -jar autotuneML.jar symbiotic pca-off conservative classification 5.0 1037
java -jar autotuneML.jar symbiotic pca-off conservative classification 5.0 4352
java -jar autotuneML.jar symbiotic pca-off conservative classification 5.0 6732
java -jar autotuneML.jar symbiotic pca-off conservative classification 5.0 9671
java -jar autotuneML.jar symbiotic pca-off conservative classification 5.0 4932
java -jar autotuneML.jar symbiotic pca-on conservative classification 5.0 4573
java -jar autotuneML.jar symbiotic pca-on conservative classification 5.0 2348
java -jar autotuneML.jar symbiotic pca-on conservative classification 5.0 1037
java -jar autotuneML.jar symbiotic pca-on conservative classification 5.0 4352
java -jar autotuneML.jar symbiotic pca-on conservative classification 5.0 6732
java -jar autotuneML.jar symbiotic pca-on conservative classification 5.0 9671
java -jar autotuneML.jar symbiotic pca-on conservative classification 5.0 4932

# java -jar autotuneML.jar cbmc pca-off conservative regression 5.0 4573
# java -jar autotuneML.jar jbmc pca-off conservative regression 5.0 4573
# java -jar autotuneML.jar jayhorn pca-off conservative regression 5.0 4573
# java -jar autotuneML.jar cbmc pca-off conservative classification 5.0 4573
# java -jar autotuneML.jar jbmc pca-off conservative classification 5.0 4573
# java -jar autotuneML.jar jayhorn pca-off conservative classification 5.0 4573
# java -jar autotuneML.jar cbmc pca-on conservative regression 5.0 4573
# java -jar autotuneML.jar jbmc pca-on conservative regression 5.0 4573
# java -jar autotuneML.jar jayhorn pca-on conservative regression 5.0 4573
# java -jar autotuneML.jar cbmc pca-on conservative classification 5.0 4573
# java -jar autotuneML.jar jbmc pca-on conservative classification 5.0 4573
# java -jar autotuneML.jar jayhorn pca-on conservative classification 5.0 4573

# java -jar autotuneML.jar cbmc pca-off conservative regression 5.0 2348
# java -jar autotuneML.jar jbmc pca-off conservative regression 5.0 2348
# java -jar autotuneML.jar jayhorn pca-off conservative regression 5.0 2348
# java -jar autotuneML.jar cbmc pca-off conservative classification 5.0 2348
# java -jar autotuneML.jar jbmc pca-off conservative classification 5.0 2348
# java -jar autotuneML.jar jayhorn pca-off conservative classification 5.0 2348
# java -jar autotuneML.jar cbmc pca-on conservative regression 5.0 2348
# java -jar autotuneML.jar jbmc pca-on conservative regression 5.0 2348
# java -jar autotuneML.jar jayhorn pca-on conservative regression 5.0 2348
# java -jar autotuneML.jar cbmc pca-on conservative classification 5.0 2348
# java -jar autotuneML.jar jbmc pca-on conservative classification 5.0 2348
# java -jar autotuneML.jar jayhorn pca-on conservative classification 5.0 2348

# java -jar autotuneML.jar cbmc pca-off conservative regression 5.0 1037
# java -jar autotuneML.jar jbmc pca-off conservative regression 5.0 1037
# java -jar autotuneML.jar jayhorn pca-off conservative regression 5.0 1037
# java -jar autotuneML.jar cbmc pca-off conservative classification 5.0 1037
# java -jar autotuneML.jar jbmc pca-off conservative classification 5.0 1037
# java -jar autotuneML.jar jayhorn pca-off conservative classification 5.0 1037
# java -jar autotuneML.jar cbmc pca-on conservative regression 5.0 1037
# java -jar autotuneML.jar jbmc pca-on conservative regression 5.0 1037
# java -jar autotuneML.jar jayhorn pca-on conservative regression 5.0 1037
# java -jar autotuneML.jar cbmc pca-on conservative classification 5.0 1037
# java -jar autotuneML.jar jbmc pca-on conservative classification 5.0 1037
# java -jar autotuneML.jar jayhorn pca-on conservative classification 5.0 1037

# java -jar autotuneML.jar cbmc pca-off conservative regression 5.0 4352
# java -jar autotuneML.jar jbmc pca-off conservative regression 5.0 4352
# java -jar autotuneML.jar jayhorn pca-off conservative regression 5.0 4352
# java -jar autotuneML.jar cbmc pca-off conservative classification 5.0 4352
# java -jar autotuneML.jar jbmc pca-off conservative classification 5.0 4352
# java -jar autotuneML.jar jayhorn pca-off conservative classification 5.0 4352
# java -jar autotuneML.jar cbmc pca-on conservative regression 5.0 4352
# java -jar autotuneML.jar jbmc pca-on conservative regression 5.0 4352
# java -jar autotuneML.jar jayhorn pca-on conservative regression 5.0 4352
# java -jar autotuneML.jar cbmc pca-on conservative classification 5.0 4352
# java -jar autotuneML.jar jbmc pca-on conservative classification 5.0 4352
# java -jar autotuneML.jar jayhorn pca-on conservative classification 5.0 4352

# java -jar autotuneML.jar cbmc pca-off conservative regression 5.0 6732
# java -jar autotuneML.jar jbmc pca-off conservative regression 5.0 6732
# java -jar autotuneML.jar jayhorn pca-off conservative regression 5.0 6732
# java -jar autotuneML.jar cbmc pca-off conservative classification 5.0 6732
# java -jar autotuneML.jar jbmc pca-off conservative classification 5.0 6732
# java -jar autotuneML.jar jayhorn pca-off conservative classification 5.0 6732
# java -jar autotuneML.jar cbmc pca-on conservative regression 5.0 6732
# java -jar autotuneML.jar jbmc pca-on conservative regression 5.0 6732
# java -jar autotuneML.jar jayhorn pca-on conservative regression 5.0 6732
# java -jar autotuneML.jar cbmc pca-on conservative classification 5.0 6732
# java -jar autotuneML.jar jbmc pca-on conservative classification 5.0 6732
# java -jar autotuneML.jar jayhorn pca-on conservative classification 5.0 6732


# java -jar autotuneML.jar cbmc pca-off conservative regression 5.0 9671
# java -jar autotuneML.jar jbmc pca-off conservative regression 5.0 9671
# java -jar autotuneML.jar jayhorn pca-off conservative regression 5.0 9671
# java -jar autotuneML.jar cbmc pca-off conservative classification 5.0 9671
# java -jar autotuneML.jar jbmc pca-off conservative classification 5.0 9671
# java -jar autotuneML.jar jayhorn pca-off conservative classification 5.0 9671
# java -jar autotuneML.jar cbmc pca-on conservative regression 5.0 9671
# java -jar autotuneML.jar jbmc pca-on conservative regression 5.0 9671
# java -jar autotuneML.jar jayhorn pca-on conservative regression 5.0 9671
# java -jar autotuneML.jar cbmc pca-on conservative classification 5.0 9671
# java -jar autotuneML.jar jbmc pca-on conservative classification 5.0 9671
# java -jar autotuneML.jar jayhorn pca-on conservative classification 5.0 9671


# java -jar autotuneML.jar cbmc pca-off conservative regression 5.0 4932
# java -jar autotuneML.jar jbmc pca-off conservative regression 5.0 4932
# java -jar autotuneML.jar jayhorn pca-off conservative regression 5.0 4932
# java -jar autotuneML.jar cbmc pca-off conservative classification 5.0 4932
# java -jar autotuneML.jar jbmc pca-off conservative classification 5.0 4932
# java -jar autotuneML.jar jayhorn pca-off conservative classification 5.0 4932
# java -jar autotuneML.jar cbmc pca-on conservative regression 5.0 4932
# java -jar autotuneML.jar jbmc pca-on conservative regression 5.0 4932
# java -jar autotuneML.jar jayhorn pca-on conservative regression 5.0 4932
# java -jar autotuneML.jar cbmc pca-on conservative classification 5.0 4932
# java -jar autotuneML.jar jbmc pca-on conservative classification 5.0 4932
# java -jar autotuneML.jar jayhorn pca-on conservative classification 5.0 4932



# parallel java -jar autotuneML.jar {1} {2} {3} {4} {5} {6} ::: cbmc jbmc jayhorn ::: pca-off pca-on ::: base greedy conservative ::: regression classification ::: 5 10 15 20 ::: 4932 3501 4390 2146 7095
# parallel java -jar autotuneML.jar {1} pca-off conservative regression {2} 4932 ::: cbmc jbmc jayhorn ::: 5 10
# parallel java -jar autotuneML.jar {1} pca-off conservative classification {2} 4932 ::: cbmc jbmc jayhorn ::: 0.2 0.3
# java -jar autotuneML.jar cbmc pca-off conservative regression 5.0 4932 > log_autotuneML_cbmc_reg_t5.txt 2>&1 &
# java -jar autotuneML.jar jbmc pca-off conservative classification 0.2 4932 > log_autotuneML_jbmc_class_t5.txt 2>&1 &
# java -jar autotuneML.jar jayhorn pca-on conservative classification 0.4 4932 > log_autotuneML_jayhorn_class_t0.4.txt 2>&1 &