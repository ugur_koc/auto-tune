# auto-tune

This repository contains the source of the auto-tune tool and the artifacts from the experiments.
The source code of autotune is under Autotune directory. This directory is a maven project that can be imported to Java IDEs such as Eclipse.

There is also the `autotune.jar` file under the tools directory. This jar can be run by a commnad like this;
	`java -jar autotune.jar cbmc pca-off greedy classification 0.3 2971 improve-off`

Each directory contains a README file that gives the instructions about using the material under that directory.

Please contact Ugur Koc at ukoc@cs.umd.edu if the instructions are not accurate or you have any other questions.