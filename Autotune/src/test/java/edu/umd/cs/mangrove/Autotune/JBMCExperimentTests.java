package edu.umd.cs.mangrove.Autotune;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.LinkedHashMap;
import java.util.List;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit tests
 */
public class JBMCExperimentTests extends TestCase {
	static String configSpaceFile;
	static LinkedHashMap<String, String[]> configMap;
	static int classIndex;
	static String trainingsets = Utils.PROJECT_DIR + "/data-ml/jbmc/train-XYZ.arff";
	static String testsets = Utils.PROJECT_DIR + "/data-ml/jbmc/test-XYZ.arff";
	static String testFilesFile = Utils.PROJECT_DIR + "/data-ml/jbmc/test-tasks-XYZ.txt";
	static String[] configDef;
	static String saveFile = "newdata.txt";
	static String category = "assert";
	static int seed = 4621;
	private double thresh = 5;

	public JBMCExperimentTests(String testName) {
		super(testName);
	}

	/**
	 * @return the suite of tests being tested
	 * @throws IOException
	 */
	public static Test suite() throws IOException {
		initializeForTests();
		return new TestSuite(JBMCExperimentTests.class);
	}

	private static void initializeForTests() throws IOException {
		Utils.BENCH_DIR = Utils.HOME_DIR + "/sv/sv-comp/sv-benchmarks-java/java";
		Utils.featureFile = Utils.HOME_DIR + "/sv/data-ml/java-program-features.txt";
		Utils.TOOL = Utils.HOME_DIR + "/sv/sv-comp/jbmc/jbmc";
		Utils.cmdPattern = "--propertyfile " + Utils.BENCH_DIR + "/assert.prp " + Utils.BENCH_DIR + "/common"
				+ Utils.BENCH_DIR + "/TARGETFILE";

		classIndex = 32 + 27;
		configSpaceFile = Utils.PROJECT_DIR + "/doms/JBMC-27opts.txt";
		configMap = Utils.readConfigFile(configSpaceFile);
		String defConfigFile = Utils.PROJECT_DIR + "/doms/JBMC-default-config.txt";

		List<String> lines = Files.readAllLines(new File(defConfigFile).toPath(), Charset.defaultCharset());
		configDef = new String[lines.size()];
		for (int i = 0; i < lines.size(); i++)
			configDef[i] = lines.get(i).split(" ")[1];
	}

	public void testWekaRegressor() throws Exception {
		WekaRegressor ch = new WekaRegressor(classIndex, "REPTree", seed);
		ch.trainModel(trainingsets.replace("XYZ", "1"));
		ch.testModel(testsets.replace("XYZ", "1"));

		WekaRegressor filter = new WekaRegressor(classIndex, "REPTree", true, seed);
		filter.trainModel(trainingsets.replace("XYZ", "1"));
		filter.testModel(testsets.replace("XYZ", "1"));

		String task = "MinePump/spec1-5_product46";
		SimultedAnnealing exp = new SimultedAnnealing(ch, filter, thresh, seed, configMap);
		exp.search(task, category, configDef, false);
	}
}