package edu.umd.cs.mangrove.Autotune;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;


public class Experiment100Samples {

	static List<HashMap<String, String>> progs;
	static String reachSafety = "ReachSafety", memSafety = "MemSafety", noOverflow = "NoOverflows",
			termination = "Termination", concurrencySafety = "ConcurrencySafety";
	static String domFile = Utils.PROJECT_DIR + "/doms/cbmc/CBMC-21opts-list.txt";
	static String defConfigFile = Utils.PROJECT_DIR + "/doms/cbmc/CBMC-default-config.txt";
	static String trainFilePtrn = Utils.PROJECT_DIR + "/data-ml/cbmc/ABC/precision-train-XYZ.arff";
	static String testFilePtrn = Utils.PROJECT_DIR + "/data-ml/cbmc/ABC/precision-test-XYZ.arff";
	static int classIndex = 34 + 45;
	static String saveFile = "newdata.txt";

	public static void main(String[] args) throws Exception {

		//double treshhold = 2.0;//Double.parseDouble(args[0]);
		LinkedHashMap<String, String[]> configMap = Utils.readConfigFile(domFile);
		String[] configDef = Utils.readDefaultConfig(defConfigFile);

		prepTestMap();
		//runRegressionFilter(configMap, configDef, (double) 8.0);
		runClassificationFilter(configMap, configDef, (double) 0.4, 4621);
		for (int i = 1; i <= 5; i++) {
			//runClassification(configMap, configDef, (double) i * 0.1, "Weka");
			//runClassification(configMap, configDef, (double) i * 0.1, "MOA");
		}
		//		for (int i = 1; i <= 3; i++) {
		//			runRegression(configMap, configDef, (double) i * 2.0, "Weka");
		//			runRegression(configMap, configDef, (double) i * 2.0, "MOA");
		//		}
	}

	public static void runRegression(LinkedHashMap<String, String[]> configMap, String[] configDef, double treshhold,
			String framework) throws IOException, ClassNotFoundException, Exception {
		AsbractModel ch;
		String app = "regression";
		String trainFilePtrn2 = trainFilePtrn.replace("ABC", app);
		String testFilePtrn2 = testFilePtrn.replace("ABC", app);
		for (int i = 1; i <= 5; i++) {
			ch = new WekaRegressor(classIndex, "REPTree", 1111);
			ch.trainModel(trainFilePtrn2.replace("XYZ", "" + i));
			ch.testModel(testFilePtrn2.replace("XYZ", "" + i));
//			SimultedAnnealingExperimental sa = new SimultedAnnealingExperimental(ch, treshhold);
//			for (Entry<String, String> e : progs.get(i - 1).entrySet())
//				sa.annealRegression(e.getKey(), e.getValue(), configMap, configDef);
		}
	}

	public static void runClassification(LinkedHashMap<String, String[]> configMap, String[] configDef, double treshhold,
			String framework, int seed) throws IOException, ClassNotFoundException, Exception {
		AsbractModel ch;
		String app = "classification";
		String trainFilePtrn2 = trainFilePtrn.replace("ABC", app);
		String testFilePtrn2 = testFilePtrn.replace("ABC", app);
		for (int i = 1; i <= 5; i++) {
			ch = new WekaClassifier(classIndex, "RandomForest", seed);
			ch.trainModel(trainFilePtrn2.replace("XYZ", "" + i));
			ch.testModel(testFilePtrn2.replace("XYZ", "" + i));
//			SimultedAnnealingExperimental sa = new SimultedAnnealingExperimental(ch, treshhold);
//			for (Entry<String, String> e : progs.get(i - 1).entrySet())
//				sa.annealClassification(e.getKey(), e.getValue(), configMap, configDef);
		}
	}

	public static void runClassificationFilter(LinkedHashMap<String, String[]> configMap, String[] configDef,
			double treshhold, int seed) throws IOException, ClassNotFoundException, Exception {
		AsbractModel ch, filter;
		String app = "classification";
		String trainFilePtrn2 = trainFilePtrn.replace("ABC", app);
		String testFilePtrn2 = testFilePtrn.replace("ABC", app);
		for (int i = 1; i <= 5; i++) {
			ch = new WekaClassifier(classIndex, "RandomForest", seed);
			ch.trainModel(trainFilePtrn2.replace("XYZ", "" + i));
			ch.testModel(testFilePtrn2.replace("XYZ", "" + i));
			filter = new WekaClassifier(classIndex, "RandomForest", true, seed);
			filter.trainModel(trainFilePtrn2.replace("XYZ", "" + i));
			filter.testModel(testFilePtrn2.replace("XYZ", "" + i));

			SimultedAnnealing sa = new SimultedAnnealing(ch, filter, treshhold, 1111, configMap);
			for (Entry<String, String> e : progs.get(i - 1).entrySet())
				sa.search(e.getKey(), e.getValue(), configDef, e.getKey().contains("true-"));
		}
	}

	public static void runRegressionFilter(LinkedHashMap<String, String[]> configMap, String[] configDef,
			String[] goodConfig, double treshhold, int seed) throws IOException, ClassNotFoundException, Exception {
		AsbractModel ch, filter;
		String app = "regression";
		String trainF = trainFilePtrn.replace("ABC", app);
		String testF = testFilePtrn.replace("ABC", app);
		for (int i = 1; i <= 5; i++) {
			ch = new WekaRegressor(classIndex, "REPTree", 1111);
			ch.trainModel(trainF.replace("XYZ", "" + i));
			ch.testModel(testF.replace("XYZ", "" + i));
			filter = new WekaRegressor(classIndex, "REPTree", true, seed);
			filter.trainModel(trainF.replace("XYZ", "" + i));
			filter.testModel(testF.replace("XYZ", "" + i));

			SimultedAnnealing sa = new SimultedAnnealing(ch, filter, treshhold, 1111, configMap);
			for (Entry<String, String> e : progs.get(i - 1).entrySet())
				sa.search(e.getKey(), e.getValue(), configDef, e.getKey().contains("true-"));
		}
	}

	private static void prepTestMap() {
		progs = new ArrayList<HashMap<String, String>>();
		for (int i = 0; i < 5; i++)
			progs.add(new HashMap<String, String>());

		progs.get(0).put("pthread-wmm/mix050_tso.opt_false-unreach-call.i", reachSafety);
		progs.get(0).put("loop-acceleration/diamond_true-unreach-call2.i", reachSafety);
		progs.get(0).put("pthread-wmm/mix011_pso.oepc_false-unreach-call.i", reachSafety);
		progs.get(0).put("pthread-wmm/mix003_rmo.oepc_false-unreach-call.i", reachSafety);
		progs.get(0).put("eca-rers2012/Problem13_label46_true-unreach-call.c", reachSafety);
		progs.get(0).put("eca-rers2012/Problem06_label53_true-unreach-call.c", reachSafety);
		progs.get(0).put("seq-pthread/cs_time_var_mutex_true-unreach-call.i", reachSafety);
		progs.get(0).put("termination-memory-linkedlists/cll_traverse-alloca_false-termination.c.i", termination);
		progs.get(0).put("pthread-wmm/safe008_pso.opt_false-unreach-call.i", reachSafety);
		progs.get(0).put("recursive-simple/id_b3_o2_false-no-overflow.c", noOverflow);
		progs.get(0).put("pthread-wmm/mix024_rmo.oepc_false-unreach-call.i", reachSafety);
		progs.get(0).put("pthread-wmm/mix025_power.oepc_false-unreach-call.i", reachSafety);
		progs.get(0).put("pthread-wmm/safe027_tso.opt_true-unreach-call.i", reachSafety);
		progs.get(0).put("eca-rers2012/Problem18_label24_true-unreach-call.c", reachSafety);
		progs.get(0).put("termination-crafted-lit/NoriSharma-FSE2013-Fig7_false-no-overflow.c", noOverflow);
		progs.get(0).put("pthread-wmm/mix019_pso.opt_false-unreach-call.i", reachSafety);
		progs.get(0).put("eca-rers2012/Problem16_label37_false-unreach-call.c", reachSafety);
		progs.get(0).put("eca-rers2012/Problem12_label00_false-unreach-call.c", reachSafety);
		progs.get(0).put("pthread-wmm/mix050_tso.oepc_false-unreach-call.i", reachSafety);
		progs.get(0).put("termination-memory-alloca/openbsd_cstrlen-alloca_true-termination.c.i", termination);

		progs.get(1).put("termination-crafted-lit/ChawdharyCookGulwaniSagivYang-ESOP2008-aaron1_false-no-overflow.c",
				noOverflow);
		progs.get(1).put("eca-rers2012/Problem10_label37_true-unreach-call.c", reachSafety);
		progs.get(1).put("array-memsafety/reverse_array_unsafe_false-valid-deref.i", memSafety);
		progs.get(1).put("eca-rers2012/Problem15_label16_true-unreach-call.c", reachSafety);
		progs.get(1).put("floats-cbmc-regression/float-div1_true-unreach-call.i", reachSafety);
		progs.get(1).put("recursive-simple/fibo_20_true-unreach-call.c", reachSafety);
		progs.get(1).put("termination-15/cstrncmp_diffterm_alloca_true-termination.c.i", termination);
		progs.get(1).put("pthread-wmm/thin001_power.opt_false-unreach-call.i", reachSafety);
		progs.get(1).put("eca-rers2012/Problem07_label14_true-unreach-call.c", reachSafety);
		progs.get(1).put("eca-rers2012/Problem07_label35_false-unreach-call.c", reachSafety);
		progs.get(1).put("pthread-wmm/safe021_power.oepc_false-unreach-call.i", reachSafety);
		progs.get(1).put("eca-rers2012/Problem19_label11_false-unreach-call.c", reachSafety);
		progs.get(1).put("pthread-wmm/safe024_power.opt_false-unreach-call.i", reachSafety);
		progs.get(1).put("eca-rers2012/Problem13_label20_true-unreach-call.c", reachSafety);
		progs.get(1).put("eca-rers2012/Problem05_label34_true-unreach-call.c", reachSafety);
		progs.get(1).put("eca-rers2012/Problem09_label16_true-unreach-call.c", reachSafety);
		progs.get(1).put("eca-rers2012/Problem03_label31_false-unreach-call.c", reachSafety);
		progs.get(1).put("pthread-wmm/mix039_pso.oepc_false-unreach-call.i", reachSafety);
		progs.get(1).put("pthread-wmm/safe003_tso.oepc_true-unreach-call.i", reachSafety);
		progs.get(1).put("array-memsafety/lis_unsafe_false-valid-deref.i", memSafety);

		progs.get(2).put("array-tiling/skipped_true-unreach-call.i", reachSafety);
		progs.get(2).put("array-examples/standard_two_index_01_true-unreach-call.i", reachSafety);
		progs.get(2).put("termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex2.17_false-no-overflow.c", noOverflow);
		progs.get(2).put("pthread-wmm/mix032_rmo.oepc_false-unreach-call.i", reachSafety);
		progs.get(2).put("seq-mthreaded/rekh_aso_true-unreach-call.3.M1.c", reachSafety);
		progs.get(2).put("eca-rers2012/Problem04_label51_true-unreach-call.c", reachSafety);
		progs.get(2).put("pthread-wmm/safe021_rmo.oepc_false-unreach-call.i", reachSafety);
		progs.get(2).put("ssh/s3_srvr.blast.02_false-unreach-call.i.cil.c", reachSafety);
		progs.get(2).put("eca-rers2012/Problem15_label06_true-unreach-call.c", reachSafety);
		progs.get(2).put("pthread-wmm/mix018_pso.oepc_false-unreach-call.i", reachSafety);
		progs.get(2).put("termination-15/cstrpbrk_diffterm_alloca_true-termination.c.i", termination);
		progs.get(2).put("pthread-wmm/safe012_power.opt_false-unreach-call.i", reachSafety);
		progs.get(2).put("eca-rers2012/Problem05_label25_true-unreach-call.c", reachSafety);
		progs.get(2).put("pthread-wmm/mix051_power.opt_false-unreach-call.i", reachSafety);
		progs.get(2).put("eca-rers2012/Problem18_label01_false-unreach-call.c", reachSafety);
		progs.get(2).put("pthread-wmm/podwr000_pso.oepc_false-unreach-call.i", reachSafety);
		progs.get(2).put("ldv-memsafety/memleaks_test15_false-valid-memtrack.i", memSafety);
		progs.get(2).put("eca-rers2012/Problem07_label37_false-unreach-call.c", reachSafety);
		progs.get(2).put("loop-acceleration/array3_false-valid-deref.i", memSafety);
		progs.get(2).put("pthread-wmm/mix054_pso.oepc_false-unreach-call.i", reachSafety);

		progs.get(3).put("ldv-memsafety/ArraysOfVariableLength2_false-valid-deref-read.c", memSafety);
		progs.get(3).put("termination-recursive-malloc/chunk3_true-termination.c.i", termination);
		progs.get(3).put("pthread-wmm/rfi010_pso.opt_false-unreach-call.i", reachSafety);
		progs.get(3).put("pthread-wmm/rfi007_rmo.oepc_false-unreach-call.i", reachSafety);
		progs.get(3).put("eca-rers2012/Problem10_label24_false-unreach-call.c", reachSafety);
		progs.get(3).put("pthread-wmm/mix004_power.opt_false-unreach-call.i", reachSafety);
		progs.get(3).put("pthread-wmm/mix026_tso.oepc_false-unreach-call.i", reachSafety);
		progs.get(3).put("termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-counterex1a_false-no-overflow.c",
				noOverflow);
		progs.get(3).put("seq-mthreaded/rekcba_aso_false-unreach-call.4.M1.c", reachSafety);
		progs.get(3).put("pthread-wmm/mix025_rmo.opt_false-unreach-call.i", reachSafety);
		progs.get(3).put("termination-memory-alloca/easySum-alloca_true-termination.c.i", termination);
		progs.get(3).put("pthread-wmm/mix044_pso.opt_false-unreach-call.i", reachSafety);
		progs.get(3).put("termination-memory-alloca/HarrisLalNoriRajamani-2010SAS-Fig1-alloca_true-termination.c.i",
				termination);
		progs.get(3).put("seq-mthreaded/pals_STARTPALS_Triplicated_true-unreach-call.ufo.UNBOUNDED.pals.c", reachSafety);
		progs.get(3).put("termination-15/cstrlen_malloc_true-termination.c.i", termination);
		progs.get(3).put("termination-crafted/Pure3Phase_false-no-overflow.c", noOverflow);
		progs.get(3).put("bitvector/jain_7_false-no-overflow.i", noOverflow);
		progs.get(3).put("pthread-wmm/safe026_tso.oepc_true-unreach-call.i", reachSafety);
		progs.get(3).put("recursive-simple/fibo_25_true-unreach-call.c", reachSafety);
		progs.get(3).put("termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-aaron3_false-no-overflow.c",
				noOverflow);

		progs.get(4).put("pthread-wmm/mix040_power.oepc_false-unreach-call.i", reachSafety);
		progs.get(4).put("eca-rers2012/Problem04_label01_true-unreach-call.c", reachSafety);
		progs.get(4).put("eca-rers2012/Problem07_label50_true-unreach-call.c", reachSafety);
		progs.get(4).put("eca-rers2012/Problem19_label19_false-unreach-call.c", reachSafety);
		progs.get(4).put("array-examples/sorting_bubblesort_false-unreach-call_ground.i", reachSafety);
		progs.get(4).put("eca-rers2012/Problem05_label31_true-unreach-call.c", reachSafety);
		progs.get(4).put("eca-rers2012/Problem13_label37_true-unreach-call.c", reachSafety);
		progs.get(4).put("pthread-wmm/mix002_rmo.opt_false-unreach-call.i", reachSafety);
		progs.get(4).put("floats-esbmc-regression/digits_bad_while_false-unreach-call.i", reachSafety);
		progs.get(4).put("termination-memory-alloca/cstrncpy-alloca_true-termination.c.i", termination);
		progs.get(4).put("ldv-races/race-2_3-container_of_false-unreach-call.i", reachSafety);
		progs.get(4).put("pthread-wmm/safe005_tso.opt_true-unreach-call.i", reachSafety);
		progs.get(4).put("pthread-wmm/rfi008_pso.oepc_false-unreach-call.i", reachSafety);
		progs.get(4).put("eca-rers2012/Problem19_label35_true-unreach-call.c", reachSafety);
		progs.get(4).put("pthread-ext/30_Function_Pointer3_vs_true-unreach-call.i", reachSafety);
		progs.get(4).put("pthread-wmm/safe007_rmo.opt_false-unreach-call.i", reachSafety);
		progs.get(4).put("eca-rers2012/Problem12_label16_true-unreach-call.c", reachSafety);
		progs.get(4).put("eca-rers2012/Problem05_label49_true-unreach-call.c", reachSafety);
		progs.get(4).put("pthread-wmm/mix015_pso.opt_false-unreach-call.i", reachSafety);
		progs.get(4).put("termination-crafted/Binary_Search_false-no-overflow.c", noOverflow);
	}
}