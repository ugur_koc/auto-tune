package edu.umd.cs.mangrove.Autotune;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.LinkedHashMap;
import java.util.List;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit tests
 */
public class CBMCExperimentTests extends TestCase {
	static String reachSafety = "ReachSafety", memSafety = "MemSafety", noOverflow = "NoOverflows",
			termination = "Termination", concurrencySafety = "ConcurrencySafety";
	static String configSpaceFile;
	static LinkedHashMap<String, String[]> configMap;
	static int classIndex;
	static String trainingFile = Utils.PROJECT_DIR + "/data-ml/cbmc/precision-train-XYZ.arff";
	static String testFile = Utils.PROJECT_DIR + "/data-ml/cbmc/precision-test-XYZ.arff";
	static String testFilesFile = Utils.PROJECT_DIR + "/data-ml/cbmc/cbmc-test-pw-fn-XYZ-1000.txt";
	static String[] configDef;
	static String saveFile = "newdata.txt";

	public CBMCExperimentTests(String testName) {
		super(testName);
	}

	/**
	 * @return the suite of tests being tested
	 * @throws IOException
	 */
	public static Test suite() throws IOException {
		initializeForTests();
		return new TestSuite(CBMCExperimentTests.class);
	}

	private static void initializeForTests() throws IOException {
		classIndex = 34 + 45;
		configSpaceFile = Utils.PROJECT_DIR + "/doms/cbmc/CBMC-21opts-list.txt";
		configMap = Utils.readConfigFile(configSpaceFile);
		String defConfigFile = Utils.PROJECT_DIR + "/doms/cbmc/CBMC-default-config.txt";
		List<String> lines = Files.readAllLines(new File(defConfigFile).toPath(), Charset.defaultCharset());
		configDef = new String[lines.size()];
		for (int i = 0; i < lines.size(); i++)
			configDef[i] = lines.get(i).split(" ")[1];
	}

	//	public void testWekaRegressor() throws Exception {
	//		run(new RegressorHandlerWeka(classIndex, "REPTree"), "regression", 4);
	//	}
	//
	//	public void testWekaClassifier() throws Exception {
	//		run(new ClassifierHandlerWeka(classIndex, "RandomForest"), "classification", 0.4);
	//	}
	//
	//	public void testMOARegressor() throws Exception {
	//		run(new RegressorHandlerMOA(classIndex + 1, "RandomAMRules"), "regression", 4);
	//	}
	//
	//	public void testMOAClassifier() throws Exception {
	//		run(new ClassifierHandlerMOA(classIndex + 1, "AdaptiveRandomForest"), "classification", 0.4);
	//	}

	//	public void testFilter() throws Exception {
	//		String app = "classification";
	//		String trainF = trainingFile.replace("ABC", app);
	//		String testF = testFile.replace("ABC", app);
	//		System.out.println(trainF);
	//		ClassifierHandlerWeka ch = new ClassifierHandlerWeka(classIndex, "RandomForest");
	//		ch.trainModel(trainF);
	//		ch.testModel(testF);
	//
	//		ClassifierHandlerWeka falseFilter = new ClassifierHandlerWeka(classIndex, "RandomForest", true);
	//		falseFilter.trainModel(trainF);
	//		falseFilter.testModel(testF);
	//
	//		String cFile = "seq-pthread/cs_time_var_mutex_true-unreach-call.i";
	//		double thresh = 0.3;
	//		SimultedAnnealing exp = new SimultedAnnealing(ch, thresh);
	//		exp.annealClassificationFilter(falseFilter, cFile, "ReachSafety", configMap, configDef);
	//	}

	public void run(AsbractModel ch, String app, double thresh) throws Exception {
		ch.trainModel(trainingFile.replace("ABC", app));
		ch.testModel(testFile.replace("ABC", app));

		String cFile = "seq-pthread/cs_time_var_mutex_true-unreach-call.i";
		SimultedAnnealing exp = new SimultedAnnealing(ch, null, 0, 123, configMap);
		if (app.equals("regression"))
			exp.search(cFile, "ReachSafety", configDef, true);
		else exp.search(cFile, "ReachSafety", configDef, true);
	}

	public void test1000Samples() throws Exception {
		int seed = 4621;
		for (int i = 1; i <= 5; i++) {
			String trainF = trainingFile.replace("XYZ", "" + i);
			String testF = testFile.replace("XYZ", "" + i);
			String testFilesF = testFilesFile.replace("XYZ", "" + i);

			AsbractModel ch = new WekaClassifier(classIndex, "RandomForest", seed);
			ch.trainModel(trainF);
			ch.testModel(testF);

			AsbractModel filter = new WekaClassifier(classIndex, "RandomForest", true, seed);
			filter.trainModel(trainF);
			filter.testModel(testF);

			SimultedAnnealing sa = new SimultedAnnealing(ch, filter, 0.4, 1111, configMap);
			List<String> files = Files.readAllLines(new File(testFilesF).toPath(), Charset.defaultCharset());
			for (String file : files)
				sa.search(file, getCategory(file), configDef, file.contains("true-"));//TODO read the good config file
		}
	}

	private static String getCategory(String file) {
		if (file.contains("overflow"))
			return noOverflow;
		else if (file.contains("valid-"))
			return memSafety;
		else if (file.contains("termination"))
			return termination;
		else if (file.contains("pthread"))
			return concurrencySafety;
		else return reachSafety;
	}
}