package edu.umd.cs.mangrove.Autotune.experimental;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import edu.umd.cs.mangrove.Autotune.Utils;

public class ExperimentRunner {
	//cbmc pca-on conservative regression 5.0 4932
	public static void main(String[] args) throws Exception {

		CommandLineParser parser = new DefaultParser();
		Options options = Utils.prepareExperimentOptions();
		CommandLine cmd = null;
		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("java -jar autotune.jar [options]", options);
			return;
		}

		String search = cmd.getOptionValue("search");
		String neighStrat = cmd.hasOption("anneal-neighbor-strat") ? cmd.getOptionValue("anneal-neighbor-strat") : "base";
		String tool = cmd.getOptionValue("tool");
		boolean pcaOn = cmd.hasOption("pca");
		String model = cmd.getOptionValue("ml-model");
		double threshold = Double.parseDouble(cmd.getOptionValue("threshold"));
		int seed = Integer.parseInt(cmd.getOptionValue("seed"));
		boolean improve = cmd.hasOption("improve");

		Experiment exp = null;

		Utils.debugStr = search + "," + tool + "," + pcaOn + "," + threshold + "," + seed + "," + neighStrat + "-" + model
				+ "," + improve;
		Utils.logFile = Utils.PROJECT_DIR + "/tools/log_" + Utils.debugStr.replaceAll(",", "_") + ".txt";
		Utils.log("Initialzing subject: " + tool + ", search:" + search + ", PCA :" + pcaOn + ", threshold:" + threshold
				+ ", seed:" + seed + ", model:" + model + ", neighbor generation strategy:" + neighStrat);
		if (tool.equalsIgnoreCase("cbmc"))
			exp = new ExperimentCBMC(search, pcaOn, neighStrat, threshold, seed, improve);
		else if (tool.equalsIgnoreCase("jbmc"))
			exp = new ExperimentJBMC(search, pcaOn, neighStrat, threshold, seed, improve);
		else if (tool.equalsIgnoreCase("jayhorn"))
			exp = new ExperimentJayHorn(search, pcaOn, neighStrat, threshold, seed, improve);
		else exp = new ExperimentSymbiotic(search, pcaOn, neighStrat, threshold, seed, improve);

		String algorithm = model.equalsIgnoreCase("regression") ? "REPTree" : "RandomForest";
		exp.run(model, algorithm);
	}
}