package edu.umd.cs.mangrove.Autotune.experimental;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Random;
import java.util.Set;

import edu.umd.cs.mangrove.Autotune.AStar;
import edu.umd.cs.mangrove.Autotune.AbstractSearchAlgorithm;
import edu.umd.cs.mangrove.Autotune.AsbractModel;
import edu.umd.cs.mangrove.Autotune.SimultedAnnealing;
import edu.umd.cs.mangrove.Autotune.Utils;
import edu.umd.cs.mangrove.Autotune.WekaClassifier;
import edu.umd.cs.mangrove.Autotune.WekaRegressor;

public abstract class Experiment {
	static String domFile;
	static String defConfigFile;
	static String trainSetPtrn;
	static String testSetPtrn;
	static String testSplitsFile;
	static String defConfigCacheFile;
	static Set<String> completedSet;
	static Random random;
	static int classIndex;
	LinkedHashMap<String, String[]> configMap;
	int seed;
	String[] defConfig;
	String search;
	String neighStrat;
	double threshold;
	boolean pcaOn = false;
	boolean improveOn = false;
	AbstractSearchAlgorithm sa;

	public Experiment(String search, boolean pcaOn, String neighStrat, double threshold, int seed, boolean improveOn)
			throws IOException {
		this.search = search;
		this.pcaOn = pcaOn;
		this.seed = seed;
		this.neighStrat = neighStrat;
		this.threshold = threshold;
		this.improveOn = improveOn;
		random = new Random();
		random.setSeed(seed);
	}

	public void run(String model, String algorithm) throws Exception {
		AsbractModel ch, filter;
		boolean isRegression = model.equalsIgnoreCase("regression");
		String trainFilePtrn2 = trainSetPtrn.replace("ABC", model);
		String testFilePtrn2 = testSetPtrn.replace("ABC", model);
		if (new File(Utils.logFile).exists()) {
			Files.readAllLines(new File(Utils.logFile).toPath(), Charset.defaultCharset()).stream()
					.filter(line -> line.startsWith("Final result:")).forEach(l -> completedSet.add(l.split(",")[1]));
		}
		for (int i = 1; i <= 5; i++) {
			String testFilesF = testSplitsFile.replace("XYZ", "" + i);
			String trainFileI = trainFilePtrn2.replace("XYZ", "" + i);
			String testFileI = testFilePtrn2.replace("XYZ", "" + i);

			ch = isRegression ? new WekaRegressor(classIndex, algorithm, seed)
					: new WekaClassifier(classIndex, algorithm, seed);
			if (pcaOn) ch.initializePCA();
			ch.trainModel(trainFileI);
			ch.testModel(testFileI);

			filter = isRegression ? new WekaRegressor(classIndex, algorithm, true, seed)
					: new WekaClassifier(classIndex, algorithm, true, seed);
			if (pcaOn) filter.initializePCA();
			filter.trainModel(trainFileI);
			filter.testModel(testFileI);

			sa = getSearchAlgorithm(search, ch, filter);
			List<String> files = Files.readAllLines(new File(testFilesF).toPath(), Charset.defaultCharset());
			for (String line : files) {
				String[] fileVerdict = line.split(",");
				if (!completedSet.contains(fileVerdict[0]))
					sa.search(fileVerdict[0], getCategory(fileVerdict[0]), defConfig, isTrue(line));
				else Utils.log("Skipping " + fileVerdict[0] + " alread in completed set.");
			}
		}
	}

	private AbstractSearchAlgorithm getSearchAlgorithm(String search, AsbractModel ch, AsbractModel filter) {
		if (search.equals("astar")) {
			return new AStar(ch, filter, threshold, random.nextInt(), configMap);
		} else if (search.equals("tabu")) {
			return new AStar(ch, filter, threshold, random.nextInt(), configMap);
		} else if (search.equals("genetic")) { return new AStar(ch, filter, threshold, random.nextInt(), configMap); }
		return new SimultedAnnealing(ch, filter, threshold, random.nextInt(), configMap);
	}

	public abstract String getCategory(String file);

	public abstract boolean isTrue(String str);

	public boolean checkDefConfig(String cFile, long start, String[] config, String threshold,
			LinkedHashMap<String, String[]> configMap, String category) {
		double timeSc;
		int verifResult = Utils.runVerifier(configMap, cFile, category, config);
		Utils.log("Starting verification with default config for:" + cFile);
		if (verifResult == 10 || verifResult == 0) {
			timeSc = (double) (System.currentTimeMillis() - start) / 1000;
			Utils.log("Final result:" + cFile + ":" + verifResult + ":" + threshold + ":" + timeSc);
			Utils.log("With config:" + Utils.arr2String(config));
		} else Utils.log("Default config did not complete (" + verifResult + ").");
		return verifResult == 10 || verifResult == 0;
	}

}
