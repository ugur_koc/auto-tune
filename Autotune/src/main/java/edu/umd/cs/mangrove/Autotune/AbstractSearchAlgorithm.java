package edu.umd.cs.mangrove.Autotune;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;

public abstract class AbstractSearchAlgorithm {
	double threshold;
	protected Random random;
	protected Set<String> correctClasses, incorrectClasses;
	protected AsbractModel fitnessF, filter;
	protected LinkedHashMap<String, String[]> optionMap;
	protected HashSet<String> testedConfigs;

	public AbstractSearchAlgorithm(AsbractModel fitnessF, AsbractModel filterF, double threshold, int seed,
			LinkedHashMap<String, String[]> optionMap) {
		this.optionMap = optionMap;
		this.threshold = threshold;
		this.fitnessF = fitnessF;
		this.filter = filterF;

		random = new Random();
		random.setSeed(seed);
		testedConfigs = new HashSet<String>();
		correctClasses = new HashSet<String>();
		correctClasses.add("TP");
		correctClasses.add("TN");

		incorrectClasses = new HashSet<String>();
		incorrectClasses.add("FP");
		incorrectClasses.add("FN");
	}

	public abstract int search(String task, String category, String[] configDef, boolean verdict) throws Exception;

	String[] generateRandomConfig(LinkedHashMap<String, String[]> configMap) {
		String[] config = new String[configMap.size()];
		Iterator<Entry<String, String[]>> iterator = configMap.entrySet().iterator();
		int indx = 0;
		while (iterator.hasNext()) {
			String[] values = iterator.next().getValue();
			int randIndx = random.nextInt(values.length);
			config[indx++] = values[randIndx];
		}
		return config;
	}
}