package edu.umd.cs.mangrove.Autotune;

import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class Autotune {

	public static void main(String[] args) {
		CommandLineParser parser = new DefaultParser();
		Options options = Utils.prepareOptions();
		try {
			parser.parse(options, args);
		} catch (ParseException e) {
			System.err.println(e.getMessage());
		}
	}
}
