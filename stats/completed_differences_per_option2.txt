--mm ['--mm sc', '--mm tso', '--mm pso']
--mm sc
(0, 1)
--mm tso
(0, 0)
--mm pso
(0, 9)
--max-nondet-tree-depth ['--max-nondet-tree-depth 100', '--max-nondet-tree-depth 1000', '--max-nondet-tree-depth 2000', '--max-nondet-tree-depth 5000']
--max-nondet-tree-depth 100
(0, 1)
--max-nondet-tree-depth 1000
(0, 1)
--max-nondet-tree-depth 2000
(0, 11)
--max-nondet-tree-depth 5000
(0, 1)
round ['--round-to-nearest', '--round-to-plus-inf', '--round-to-minus-inf', '--round-to-zero']
--round-to-nearest
(0, 3)
--round-to-plus-inf
(0, 1)
--round-to-minus-inf
(0, 6)
--round-to-zero
(0, 2)
--no-built-in-assertions ['--no-built-in-assertions']
--no-built-in-assertions
(1, 0)
--no-pretty-names ['--no-pretty-names']
--no-pretty-names
(1, 0)
--no-self-loops-to-assumptions ['--no-self-loops-to-assumptions']
--no-self-loops-to-assumptions
(0, 1)
--full-slice ['--full-slice']
--full-slice
(1, 5)
--paths ['--paths lifo', '--paths fifo']
--paths lifo
(2, 1)
--paths fifo
(1, 2)
--refine-strings ['--refine-strings']
--refine-strings
(0, 1)
--partial-loops ['--partial-loops']
--partial-loops
(0, 86)
--nondet-static ['--nondet-static']
--nondet-static
(0, 29)
solvert ['--cprover-smt2', '--boolector', '--cvc4', '--mathsat', '--yices', '--z3']
--cprover-smt2
(1, 2)
--boolector
(0, 16)
--cvc4
(0, 3)
--mathsat
(0, 11)
--yices
(0, 5)
--z3
(0, 13)
reachability-slice ['--reachability-slice', '--reachability-slice-fb']
--reachability-slice
(1, 0)
--reachability-slice-fb
(0, 3)
--drop-unused-functions ['--drop-unused-functions']
--drop-unused-functions
(0, 0)
--no-assumptions ['--no-assumptions']
--no-assumptions
(0, 1)
--refine ['--refine']
--refine
(0, 0)
--depth ['--depth 100', '--depth 1000', '--depth 2000', '--depth 5000']
--depth 100
(9, 0)
--depth 1000
(0, 11)
--depth 2000
(0, 10)
--depth 5000
(0, 12)
arrays-uf ['--arrays-uf-never', '--arrays-uf-always']
--arrays-uf-never
(1, 1)
--arrays-uf-always
(0, 2)
--min-null-tree-depth ['--min-null-tree-depth 5', '--min-null-tree-depth 10', '--min-null-tree-depth 20', '--min-null-tree-depth 100']
--min-null-tree-depth 5
(0, 2)
--min-null-tree-depth 10
(0, 0)
--min-null-tree-depth 20
(0, 1)
--min-null-tree-depth 100
(0, 5)
--unwind ['--unwind 1', '--unwind 5', '--unwind 10', '--unwind 20', '--unwind 100']
--unwind 1
(1, 1)
--unwind 5
(0, 8)
--unwind 10
(1, 1)
--unwind 20
(0, 3)
--unwind 100
(1, 1)
--slice-formula ['--slice-formula']
--slice-formula
(0, 1)