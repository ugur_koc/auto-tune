--mm ['--mm sc', '--mm tso', '--mm pso']
(0, 1)
(9, 1)
(9, 0)
--max-nondet-tree-depth ['--max-nondet-tree-depth 100', '--max-nondet-tree-depth 1000', '--max-nondet-tree-depth 2000', '--max-nondet-tree-depth 5000', 'Not--max-nondet-tree-depth']
(0, 0)
(11, 1)
(1, 1)
(2, 0)
(11, 1)
(1, 1)
(2, 0)
(1, 11)
(1, 9)
(3, 1)
round ['--round-to-nearest', '--round-to-plus-inf', '--round-to-minus-inf', '--round-to-zero']
(1, 3)
(6, 3)
(2, 3)
(5, 0)
(2, 1)
(0, 4)
--no-built-in-assertions ['--no-built-in-assertions', 'Not--no-built-in-assertions']
(1, 0)
--no-pretty-names ['--no-pretty-names', 'Not--no-pretty-names']
(1, 0)
--no-self-loops-to-assumptions ['--no-self-loops-to-assumptions', 'Not--no-self-loops-to-assumptions']
(0, 1)
--full-slice ['--full-slice', 'Not--full-slice']
(1, 5)
--paths ['--paths lifo', '--paths fifo']
(2, 1)
--refine-strings ['--refine-strings', 'Not--refine-strings']
(0, 1)
--partial-loops ['--partial-loops', 'Not--partial-loops']
(0, 86)
--nondet-static ['--nondet-static', 'Not--nondet-static']
(0, 29)
solvert ['--cprover-smt2', '--boolector', '--cvc4', '--mathsat', '--yices', '--z3']
(14, 0)
(1, 0)
(10, 1)
(4, 1)
(11, 0)
(0, 13)
(1, 6)
(0, 11)
(4, 7)
(9, 1)
(3, 1)
(10, 0)
(1, 7)
(6, 4)
(8, 0)
reachability-slice ['--reachability-slice', '--reachability-slice-fb']
(3, 0)
--drop-unused-functions ['--drop-unused-functions', 'Not--drop-unused-functions']
(0, 0)
--no-assumptions ['--no-assumptions', 'Not--no-assumptions']
(0, 1)
--refine ['--refine', 'Not--refine']
(0, 0)
--depth ['--depth 100', '--depth 1000', '--depth 2000', '--depth 5000', 'Not--depth']
(11, 0)
(10, 0)
(12, 0)
(19, 0)
(0, 1)
(2, 1)
(9, 1)
(3, 1)
(10, 1)
(8, 1)
arrays-uf ['--arrays-uf-never', '--arrays-uf-always']
(1, 0)
--min-null-tree-depth ['--min-null-tree-depth 5', '--min-null-tree-depth 10', '--min-null-tree-depth 20', '--min-null-tree-depth 100', 'Not--min-null-tree-depth']
(0, 2)
(0, 1)
(4, 1)
(4, 1)
(1, 0)
(5, 0)
(5, 0)
(4, 0)
(5, 1)
(5, 5)
--unwind ['--unwind 1', '--unwind 5', '--unwind 10', '--unwind 20', '--unwind 100']
(8, 1)
(0, 0)
(3, 1)
(0, 0)
(1, 8)
(2, 7)
(1, 8)
(3, 1)
(0, 0)
(1, 3)
--slice-formula ['--slice-formula', 'Not--slice-formula']
(0, 1)