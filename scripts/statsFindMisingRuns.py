#!/usr/bin/env python2.7

import re,os
from subprocess import call
import Utils_Config as cu

tools=['cbmc', 'symbiotic', 'jayhorn', 'jbmc']
tool = tools[1]
lang='c' # 'c'

statsFile=open('../tools/{}/results/stats-all.txt'.format(tool))
lines = statsFile.read().splitlines()
statsFile.close()

tasksFile = open('../data/tasks/{}-tasks-all.txt'.format(lang),'r')
taskVerdict=tasksFile.read().splitlines()
tasks = set([tasks.split(',')[0] for tasks in taskVerdict])
tasksFile.close()

domFile='../tools/{}/Config-space-16opts.txt'.format(tool)
optionNames, _ = cu.readDomFile(domFile)

f = open('../tools/{}/CA-16opts-t3.txt'.format(tool),'r')
configLines = f.read().splitlines()
f.close()

designMap = {}
for config in configLines:
	designMap[cu.designLine2Config(tool, config, optionNames)] = config

configsMap={}
for line in lines:
	config, task, verdict, result, _ = line.split(',')
	#config, task, propFile, _, code = line.split(',') symbiotic
	assert (designMap.has_key(config)), 'Config is not in designMap:'+ config
	assert (task in tasks), 'C file is not in the samples sets:'+ task

	if not configsMap.has_key(config):
		configsMap[config] = set()
	if (task in configsMap[config]):
		print line

	configsMap[config].add(task)

for tv in taskVerdict:
	task, verdict = tv.split(',')
	for config,taskSet in configsMap.iteritems():
		if not task in taskSet:
			print config+','+task+','+verdict