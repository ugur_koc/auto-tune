#!/opt/local/bin/python2.7

import re,os,csv
import Utils_ML as mlu
import Utils_Config as conf
import subprocess as sp

BASE_DIR='{}/sv'.format(str(os.getenv("HOME")))
saveFileBase = 'Aug14'

tools={'cbmc':['c','21'],'jayhorn':['java','12'], 'jbmc':['java', '27'], 'symbiotic':['c','16']}

def writeArrfFile(file,data,metadata):
	with open(file, "wb") as sf:
		sf.write(metadata+'\n')
		writer = csv.writer(sf)
		writer.writerows(data)
		sf.close()

def createSplits(tool, model, optsStr, lang):
	TOOL_DIR = '{}/tools/{}'.format(BASE_DIR, tool)
	filePattern = '{}/data/splits/{}-testset-XYZ.txt'.format(BASE_DIR, lang)
	testProgs = []
	for x in xrange(1,6): 
		with open(filePattern.replace('XYZ',str(x)),'r') as file: 
			testProgs.append([line.split(',')[0] for line in file.read().splitlines()])

	allDataFile = '{}/data/All-{}-{}.arff'.format(TOOL_DIR, saveFileBase, model)
	metadata, dataset = mlu.readArff(allDataFile, 0)
	testMaps = [[],[],[],[],[]]
	trainMaps = [[],[],[],[],[]]
	for dp in dataset:
		for x in xrange(0,5):
			if dp[-1] in testProgs[x]:
				testMaps[x].append(dp[:-1])
			else:
				trainMaps[x].append(dp[:-1])

	saveFiles = [[TOOL_DIR+'/data/test-'+model+'-'+str(i)+'.arff',TOOL_DIR+'/data/train-'+model+'-'+str(i)+'.arff'] for i in xrange(1,6)]
	for x in xrange(0,5):
		writeArrfFile(saveFiles[x][0],testMaps[x],metadata)
		writeArrfFile(saveFiles[x][1],trainMaps[x],metadata)

for tool,fields in tools.iteritems():
	if tool != 'symbiotic':
		continue
	createSplits(tool, 'classification', fields[1]+'opts', fields[0])
	createSplits(tool, 'regression', fields[1]+'opts', fields[0])