#!/opt/local/bin/python

import re,os
from collections import OrderedDict

scores = {'UNK':0,'TN':2,'TP':1,'FN':-32,'FP':-16}
tools=['cbmc', 'symbiotic', 'jayhorn', 'jbmc']
tool = tools[3]

statsFile=open('../tools/{}/results/summary-Aug10.csv'.format(tool))
lines = statsFile.read().splitlines()[1:] #skipping header
statsFile.close()

statsMap={}
for line in lines:
	threshold, seed, label, task, result, cost, score, time = line.split(',')
	if not statsMap.has_key(threshold):
		intialVals = OrderedDict([('UNK',0),('TN',0),('TP', 0),('FN',0),('FP',0), ('score',0)])
		statsMap[threshold] = {seed:intialVals}
	if not statsMap[threshold].has_key(seed):
		statsMap[threshold][seed] = OrderedDict([('UNK',0),('TN',0),('TP', 0),('FN',0),('FP',0), ('score',0)])
	statsMap[threshold][seed][label] = 1 + statsMap[threshold][seed][label]
	statsMap[threshold][seed]['score'] = scores[label] + statsMap[threshold][seed]['score']

for threshold,seedMap in statsMap.iteritems():
	for seed,stats in seedMap.iteritems():
		print threshold +' & '+ seed +' & ' +' & '.join([str(val) for val in stats.values()]) +'\\\\'