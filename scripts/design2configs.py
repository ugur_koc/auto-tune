#!/usr/bin/env python2.7

import re,os
import Utils_Config as cu

dom='cbmc/Config-space-21opts.txt'
dom='jbmc/Config-space-27opts.txt'
dom='jayhorn/Config-space-12opts.txt'
#dom='symbiotic/Config-space-16opts.txt'

CA='cbmc/CA-21opts-t3.txt'
CA='jbmc/CA-27opts-t3.txt'
CA='jayhorn/CA-12opts-t3.txt'
#CA='symbiotic/CA-16opts-t3.txt'

domFile = '../tools/{}'.format(dom)
designFile = '../tools/{}'.format(CA)

domFile = open(domFile, 'r')
options = [opt.split(':')[0] for opt in domFile.readlines()]
domFile.close()

f = open(designFile,'r')
lines = f.readlines()
f.close()

for line in lines:
	# print cu.designLine2ConfigCBMC(line, options)
	# print cu.designLine2ConfigJBMC(line, options)
	#print cu.designLine2ConfigSymbiotic(line, options)
	print cu.designLine2ConfigJayHorn(line, options)
	