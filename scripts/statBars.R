library(car)
library(plotrix)
library(ggplot2)
library(doBy)
library(plyr)
library(dplyr) 
library(Hmisc)
library(pgirmess)
library(xtable)
library(fmsb)
library(reshape2)
library(wesanderson)
library(ggrepel)
dir <- "/Users/ukoc/sv/";

all_data <- read.table(paste(dir,"ml/stats-ml.csv",sep=""), header=TRUE, sep=",");
data <- do.call(data.frame, aggregate(cbind(m1) ~ tool+filter+model+pca, data = all_data, f1))
data <- data[data$pca=='off',]
data$m1.Median[data$model=='classification'] <- (100.0 - data$m1.Median[data$model=='classification'])
data <- data[order(data$tool),]
data <- data[order(data$tool),]
print(xtable(data),include.rownames=FALSE)


mlStats <- function(){
  #pca,tool,model,split,seed,filter,m1,m2,m3,m4
  all_data <- read.table(paste(dir,"ml/stats-ml.csv",sep=""), header=TRUE, sep=",");
  data <- do.call(data.frame, aggregate(cbind(m1) ~ tool+filter+model+pca, data = all_data, f1))
  data$m1.Median[data$model=='classification'] <- (100.0 - data$m1.Median[data$model=='classification'])
  data <- data[order(data$model),]
  print(xtable(data),include.rownames=FALSE)
}

statsConfigBarPlot <- function(){
  #tool,id,TN,TP,FN,FP,prog,complete,score,falseRate
  all_data <- read.table(paste(dir,"stats/stats-configs-all.csv",sep=""), header=TRUE, sep=",");
  data <- aggregate(cbind(TN,TP,FN,FP,prog,complete,score,falseRate) ~ tool, data = all_data, median)
  data <- melt(data[1:(length(data)-4)], id.vars = c("tool"))
  data$value[data$variable=='FP' | data$variable=='FN'] <- (-1*data$value[data$variable=='FP'| data$variable=='FN'])
  #data <- data[order(data$tool),]
  data$tool <- factor(data$tool, levels = c("cbmc", "symbiotic", "jayhorn", "jbmc"))
  g_acc <- ggplot(data=data, aes(x=tool, y=value, fill=variable)) + geom_bar(stat="identity", width = 0.8) +
    scale_fill_manual(values=wes_palette(n=4, name="Zissou1")) + 
    theme(legend.position="none",
          axis.title.x = element_blank(),
          axis.title.y = element_blank(),
          axis.text.x = element_text(angle = 30, hjust = 1),
          plot.title = element_text(hjust = 0.5))
  print(g_acc)
  dev.print(width = 3, height = 4, device=postscript, file= paste("/Users/ukoc/autotune_paper/figures/configbars.eps",sep=""))
}  
statsProgBarPlot <- function(){
  #tool,id,TN,TP,FN,FP,prog,complete,score,falseRate
  all_data <- read.table(paste(dir,"stats/stats-progs-all.csv",sep=""), header=TRUE, sep=",");
  data <- aggregate(cbind(TN,TP,FN,FP,config,complete,score,falseRate) ~ tool, data = all_data, median)
  data <- melt(data[1:(length(data)-4)], id.vars = c("tool"))
  data$value[data$variable=='FP' | data$variable=='FN'] <- (-1*data$value[data$variable=='FP'| data$variable=='FN'])
  #data <- data[order(data$tool),]
  data$tool <- factor(data$tool, levels = c("cbmc", "symbiotic", "jayhorn", "jbmc"))
  g_acc <- ggplot(data=data, aes(x=tool, y=value, fill=variable)) + geom_bar(stat="identity", width = 0.8) +
    scale_fill_manual(values=wes_palette(n=4, name="Zissou1")) + 
    theme(legend.title = element_blank(),
          legend.position = c(0.92, 0.88),
          legend.text=element_text(size=6),
          axis.title.x = element_blank(),
          axis.title.y = element_blank(),
          legend.key.size = unit(1,"line"),
          legend.background=element_blank(),
          axis.text.x = element_text(angle = 30, hjust = 1))
  print(g_acc)
  dev.print(width = 3, height = 4,device=postscript, file= paste("/Users/ukoc/autotune_paper/figures/progbars.eps",sep=""))
}

autotuneLinePoint <- function(){
  #pca,tool,model,split,seed,filter,m1,m2,m3,m4
  #saveFile="improvement.eps"
  saveFile="rand-start.eps"
  all_data <- read.table(paste(dir,"logs/logs-aug19/summary-complete.csv",sep=""), header=TRUE, sep=",");
  data <- aggregate(cbind(count = label) ~ tool+mode+thresh+label+seed, data = all_data, NROW)
  data <- data[which(data$label!='UNK'),]
  data <- data[order(data$thresh),]
  data <- data[order(data$mode),]
  data <- data[order(data$tool),]
  data$label <- factor(data$label, levels = c("TN", "TP", "FN", "FP"))
  data$tool <- factor(data$tool, levels = c("cbmc", "symbiotic", "jayhorn", "jbmc"))
  g_acc <- ggplot(data=data, aes(thresh, count, colour=label)) + geom_line() + geom_point() +
    geom_text_repel(aes(label=count), size = 2, box.padding=0.12)+
    scale_color_manual(values=wes_palette(n=4, name="Zissou1"))+
    xlab("threshold") + ylab("") +
    facet_grid(tool~(mode), scales="free") + 
    theme(axis.text.x = element_text(angle = 90, hjust = 1), 
          legend.position = "top", legend.title = element_blank(),
          strip.background =element_rect(fill="white"))
  # + stat_summary(fun.data = give.n, geom = "text", position = "identity", size=2)
  print(g_acc)
  dev.print(width = 30, height = 30, device=postscript, file= paste("/Users/ukoc/autotune_paper/figures/", saveFile ,sep=""))
}