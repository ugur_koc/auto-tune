#!/usr/bin/env python2.7

import re,os,csv,copy
import Utils_Config as cu
import Utils_ML as mlu
import subprocess as sp

#FIX fo the new folder organization

f = open('../stats/stats_part1.txt','r') #cbmc-21opts-May14 21opts-Apr2 21opts-Mar4
stats = f.read().splitlines()
f.close()

statsFile=open('../stats/stats_part2.txt')
stats.extend(statsFile.read().splitlines())
statsFile.close()

optionNames, _ = cu.readDomFile('../doms/cbmc/CBMC-21opts-list.txt')

featuresFile='../data-ml/cbmc/cbmc_1000_static_analysis_results.txt'
featureMap = mlu.readFeatures(featuresFile)

f = open('../designs/CBMC-t3-CA-new.txt','r')
configLines = f.read().splitlines()
f.close()

designMap = {}
for cLine in configLines:
	designMap[cu.designLine2Config(cLine,optionNames)] = cLine.split(',')

def getLabelClassification(result,file):
	label = 'UNK'
	if result == '0':
		label = 'TN' if 'true-' in cFile else 'FN'
	elif result == '10':
		label = 'TP' if 'false-' in cFile else 'FP'
	return label

def getLabelRegression(result,file):
	label = '50'
	if result == '0':
		label = '0' if 'true-' in cFile else '100'
	elif result == '10':
		label = '1' if 'false-' in cFile else '99'
	return label

def getFeatureVector(cFile):
	assert (featureMap.has_key(cFile)), '{} is not in the feature map'.format(cFile)
	return featureMap[cFile]

datapoints=[]
for stat in stats:
	config, cFile, propFile, result = stat.split(',')
	assert (designMap.has_key(config)), 'Config is not in designMap:'+ config
	#label = getLabelClassification(result,cFile)
	label = getLabelRegression(result,cFile)
	# else:
	# 	label = result
	
	features = getFeatureVector(cFile)
	# [1 if (result == '0' or result == '10') else 0] + complete
	dp = designMap[config] + features +  [label] + [propFile[:-4]] + [cFile]
	datapoints.append(dp)

saveFile = '../data-ml/cbmc/All-Jul14-reg.arff'
with open(saveFile, "wb") as sf:
    writer = csv.writer(sf)
    writer.writerows(datapoints)
    sf.close()