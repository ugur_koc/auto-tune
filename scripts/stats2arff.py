#!/usr/bin/env python2.7

import re,os,csv,copy
import Utils_Config as cu
import Utils_ML as mlu
import subprocess as sp

BASE_DIR='{}/sv'.format(str(os.getenv("HOME")))
saveFileBase = 'Aug14'

tools={'cbmc':['c','21','0,0,0,0,0,0,0,0,0,0,0,0,100,0,0,0,sc,0,0,--round-to-nearest,0'],
		'jayhorn':['java','12','off,off,10,-1,auto,1,-1,-1,3,eldarica,1,off'], 
		'jbmc':['java', '27', '0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,100,0,0,0,0,0,0,0,0'], 
		'symbiotic':['c','16','off,off,off,off,off,off,off,off,off,off,off,off,inv,1,off,off']}

def getLabelClassification(result, verdict):
	label = 'UNK'
	if result == '0':
		label = 'TN' if verdict else 'FN'
	elif result == '10':
		label = 'TP' if not verdict else 'FP'
	return label

def getLabelRegression(result, verdict):
	label = '50'
	if result == '0':
		label = '0' if verdict else '100'
	elif result == '10':
		label = '1' if not verdict else '99'
	return label

def createAllArrf(tool, model, optsStr, lang, defconfig=None):
	TOOL_DIR='{}/tools/{}'.format(BASE_DIR, tool)
	saveFile = '{}/data/All-{}-{}.arff'.format(TOOL_DIR, saveFileBase, model)

	f = open('{}/data/headers.txt'.format(TOOL_DIR),'r')
	headers = f.read()
	f.close()

	f = open('{}/results/stats-all.txt'.format(TOOL_DIR),'r')
	stats = f.read().splitlines()
	f.close()

	spaceFile='{}/Config-space-{}.txt'.format(TOOL_DIR,optsStr)
	optionNames, _ = cu.readDomFile(spaceFile)

	featuresFile = '{}/data/features/{}-program-features.txt'.format(BASE_DIR, lang)
	featureMap = mlu.readFeatures(featuresFile)

	f = open('{}/CA-{}-t3.txt'.format(TOOL_DIR,optsStr), 'r')
	configLines = f.read().splitlines()
	f.close()

	designMap = {}
	for cLine in configLines:
		designMap[cu.designLine2Config(tool,cLine,optionNames)] = cLine.split(',')
	
	# adding defconfig results into the datasets
	if defconfig != None:
		f = open('{}/results/stats-defconfig.txt'.format(TOOL_DIR),'r')
		stats.extend(f.read().splitlines())
		f.close()
		designMap[cu.designLine2Config(tool,defconfig,optionNames)] = defconfig.split(',')

	datapoints=[]
	for stat in stats:
		config, task, verdict, result, propFile, label= None, None, None, None, None, None
		if lang=='java':
			config, task, verdict, result, propFile = stat.split(',') # java jbmc and jayhorn
			verdict = 'True' == verdict
		elif tool == 'cbmc':
			config, task, propFile, result = stat.split(',') # cbmc
			verdict = '_true-' in task
		elif tool == 'symbiotic':
			config, task, propFile, _, code = stat.split(',')
			verdict = '_true-' in task
			result = '10' if code == 'false' else ('0' if code == 'true' else ('-6' if code == 'timeout' else '-9'))
		if model=='regression':
			label = getLabelRegression(result, verdict)
		else:
			label = getLabelClassification(result, verdict)

		assert (designMap.has_key(config)), 'Config is not in designMap:'+ config
		assert (featureMap.has_key(task)), '{} is not in the feature map'.format(task)
		dp = designMap[config] + featureMap[task] +  [label] + [propFile[:-4]] + [task]
		datapoints.append(dp)

	with open(saveFile, "wb") as sf:
		sf.write(headers+'\n')
		writer = csv.writer(sf)
		writer.writerows(datapoints)
		sf.close()

for tool,fields in tools.iteritems():
	if tool !='symbiotic':
		continue
	createAllArrf(tool, 'classification', fields[1]+'opts', fields[0],defconfig=fields[2])
	createAllArrf(tool, 'regression', fields[1]+'opts', fields[0],defconfig=fields[2])