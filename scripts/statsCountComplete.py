#!/opt/local/bin/python

import re,os
import numpy as np
from subprocess import call
import Utils_ML as mlu
import Utils_Config as cu

tools=['cbmc', 'symbiotic', 'jayhorn', 'jbmc']
tool = tools[1]
lang='c' #'c' 'java'

statsFile=open('../tools/{}/results/stats-all.txt'.format(tool))
stats = statsFile.read().splitlines()
statsFile.close()

featuresFile='../data/features/{}-program-features.txt'.format(lang)
featureMap = mlu.readFeatures(featuresFile)

def getFeatureVector(task):
	assert (featureMap.has_key(task)), '{} is not in the feature map'.format(task)
	return featureMap[task]

domFile='../tools/{}/Config-space-16opts.txt'.format(tool)
optionNames, optionSettings = cu.readDomFile(domFile)

f = open('../tools/{}/CA-16opts-t3.txt'.format(tool),'r')
configLines = f.read().splitlines()
f.close()
designMap = {}
for cLine in configLines:
	designMap[cu.designLine2Config(tool, cLine, optionNames)] = cLine

tasksMap = {}
configsMap={}
for stat in stats:
	config, task, verdict, result, propFile, label= None, None, None, None, None, None
	if lang=='java':
		config, task, verdict, result, propFile = stat.split(',') # java jbmc and jayhorn
		verdict = 'True' == verdict
	elif tool == 'cbmc':
		config, task, propFile, result = stat.split(',') # cbmc
		verdict = '_true-' in task
	elif tool == 'symbiotic':
		config, task, propFile, _, code = stat.split(',')
		verdict = '_true-' in task
		result = '10' if code == 'false' else ('0' if code == 'true' else ('-6' if code == 'timeout' else '-9'))

	assert (designMap.has_key(config)), 'Config is not in designMap:'+ config

	if not configsMap.has_key(config):
		configsMap[config] = [0,0,0,0,0]

	if not tasksMap.has_key(task):
		tasksMap[task] = [0,0,0,0,0]
	
	if result == '0' and verdict:#TN
		configsMap[config][0] = configsMap[config][0]+1
		tasksMap[task][0] = tasksMap[task][0]+1
	elif result == '10'  and not verdict:#TP
		configsMap[config][1] = configsMap[config][1]+1
		tasksMap[task][1] = tasksMap[task][1]+1
	elif result == '0'  and not verdict: #FN
		configsMap[config][2] = configsMap[config][2]+1
		tasksMap[task][2] = tasksMap[task][2]+1
	elif result == '10'  and verdict: #FP
		configsMap[config][3] = configsMap[config][3]+1
		tasksMap[task][3] = tasksMap[task][3]+1

	configsMap[config][4]=1+configsMap[config][4]
	tasksMap[task][4] = tasksMap[task][4]+1
	# print tasksMap[task], task, result

def printProgStats(): # Table 4 of paper
	stats = np.zeros(shape=(len(tasksMap),8))
	index=0
	for task,counts in tasksMap.iteritems():
		complete=counts[0]+counts[1]+counts[2]+counts[3]
	 	score=2.0*counts[0]+counts[1]-32.0*counts[2]-16.0*counts[3]
	 	denom = (counts[0]+counts[1]+counts[2]+counts[3])
		falseRate = 0 if denom==0 else (100.0*(counts[2]+counts[3])/denom)
		stats[index] = counts+[complete, score, falseRate]
		index+=1
	mins= np.min(stats, axis = 0)
	maxs= np.max(stats, axis = 0)
	medians= np.median(stats, axis = 0)
	siqrs = np.subtract(*np.percentile(stats, [75, 25], axis = 0))
	siqrs= np.divide(siqrs, 2)
	for i in [5,7]:
		print 'X{}Y'.format(str(mins[i])) +' & '+ 'X{}Y'.format(str(maxs[i])) +' & '+ 'A{} {}C'.format(str(medians[i]),str(siqrs[i]))

def printConfigStats(): # Table 4 of paper
	stats = np.zeros(shape=(len(configsMap),8))
	index=0
	for task,counts in configsMap.iteritems():
		complete=counts[0]+counts[1]+counts[2]+counts[3]
	 	score=2.0*counts[0]+counts[1]-32.0*counts[2]-16.0*counts[3]
	 	denom = (counts[0]+counts[1]+counts[2]+counts[3])
		falseRate = 0 if denom==0 else (100.0*(counts[2]+counts[3])/denom)
		stats[index] = counts+[complete, score, falseRate]
		index+=1
	mins= np.min(stats, axis = 0)
	maxs= np.max(stats, axis = 0)
	medians= np.median(stats, axis = 0)
	siqrs = np.subtract(*np.percentile(stats, [75, 25], axis = 0))
	siqrs= np.divide(siqrs, 2)
	for i in [5,7]:
		print 'X{}Y'.format(str(mins[i])) +' & '+ 'X{}Y'.format(str(maxs[i])) +' & '+ 'A{}B{}C'.format(str(medians[i]),str(siqrs[i]))


#printProgStats()
#printConfigStats()

# for task,counts in tasksMap.iteritems():
# 	complete=counts[0]+counts[1]+counts[2]+counts[3]
#  	score=2.0*counts[0]+counts[1]-32.0*counts[2]-16.0*counts[3]
#  	denom = (counts[0]+counts[1]+counts[2]+counts[3])
# 	falseRate = 'NA' if denom==0 else (100.0*(counts[2]+counts[3])/denom)
# 	print ','.join([str(x) for x in counts])+','+str(complete)+','+str(score)+','+str(falseRate)+','+','.join(getFeatureVector(task))

# print counter
# print len(tasksMap)

print len(configsMap)
for config,counts in configsMap.iteritems():
	assert (designMap.has_key(config)), 'Config is not in designMap:'+ config
	complete=counts[0]+counts[1]+counts[2]+counts[3]
	score=2.0*counts[0]+counts[1]-32.0*counts[2]-16.0*counts[3]
	falseRate=0.0 if complete==0 else 100.0*(counts[2]+counts[3])/(counts[0]+counts[1]+counts[2]+counts[3])
	print ','.join([str(x) for x in counts])+','+str(complete)+','+str(score)+','+str(falseRate)+','+','.join(designMap[config].split(' '))
	#print ','.join([str(x) for x in counts])+','+str(complete)+','+str(score)+','+str(falseRate)+','+config