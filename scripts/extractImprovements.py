#!/usr/bin/env python2.7

import re,os,csv,copy
from sets import Set

BASE_DIR='{}/sv'.format(str(os.getenv("HOME")))

tools={'cbmc':['c','data/def_completes.txt',None],
		'jayhorn':['java','data/def_completes.txt',None],
		'jbmc':['java', 'data/def_completes.txt','data/unmodelled_skips.txt'], 
		'symbiotic':['c','data/def_completes.txt','data/concurrency_skips.txt']}

completesMap={}

summaryFile='{}/logs/summary-complete-sep29-noon.csv'.format(BASE_DIR)
saveFile=summaryFile.replace("summary","summary-improvements")

f = open(summaryFile, 'r')
stats = f.read().splitlines()[1:]
f.close()

def readDefCompletes(tool, def_completesFile, skipsFile):
	TOOL_DIR='{}/tools/{}'.format(BASE_DIR, tool)
	
	f = open('{}/{}'.format(TOOL_DIR, def_completesFile), 'r')
	completesMap[tool] = Set(f.read().splitlines())
	f.close()

	if skipsFile != None:
		f = open('{}/{}'.format(TOOL_DIR, skipsFile), 'r')
		for line in f.read().splitlines():
			completesMap[tool].add(line)
		f.close()		

for tool,fields in tools.iteritems():
	readDefCompletes(tool, fields[1], fields[2])

#label,task,result,cost,score,time,configCount,revisitCount,acceptCount,bestCount,saRunCount,tool,pca,thresh,seed,mode,improve
with open(saveFile, "wb") as sf:
	for stat in stats:# get the task and tool out of line
		fields = stat.split(',')
		task, tool=fields[1],fields[11]
		if not task in completesMap[tool]:
			sf.write(stat+'\n')