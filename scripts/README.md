# Scripts used to create and process data files

- `design2configs.py` will create actuall configurations from covering array rows and config space files
- `stats2arff.py` will create the Arff file
- `createPWSplits.py` will create test and training sets from the Arff file computed with `stats2arff.py`
- `Utils_ML.py` and `Utils_Config.py` contain some utility functions used by other three scripts

- the folder misc_old contain many other scripts used in ealier experiments

Note that, there are some additional scripts to run each tool under tools directory