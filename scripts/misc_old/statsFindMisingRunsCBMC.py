#!/usr/bin/env python2.7

import re,os
from subprocess import call
import Utils_Config as cu

statsFile=open('../stats/stats_part1.txt')
lines = statsFile.read().splitlines()
statsFile.close()

statsFile=open('../stats/stats_part2.txt')
lines.extend(statsFile.read().splitlines())
statsFile.close()


cFilesFile = open('../data-ml/cbmc/cbmc-1000-samples-all.txt','r')
cFiles = set([cFile.split(',')[0] for cFile in cFilesFile.read().splitlines()])
cFilesFile.close()

def getCategory(cFile):
	proFile='ReachSafety.prp'
	if "overflow" in cFile:
		proFile='NoOverflows.prp'
	elif "valid-"  in cFile:
		proFile='MemSafety.prp'
	elif "termination"  in cFile:
		proFile='Termination.prp'
	elif "pthread"  in cFile:
		proFile= 'Concurrency.prp'
	return proFile

domFile='../doms/cbmc/CBMC-21opts-list.txt'
optionNames, _ = cu.readDomFile(domFile)

f = open('../designs/CBMC-t3-CA-new.txt','r')
configLines = f.read().splitlines()
f.close()
designMap = {}
for cLine in configLines:
	designMap[cu.designLine2Config(cLine,optionNames)]=cLine	

configsMap={}
for line in lines:
	config, cFile, task, result = line.split(',')
	assert (designMap.has_key(config)), 'Config is not in designMap:'+ config
	assert (cFile in cFiles), 'C file is not in the samples sets:'+ cFile

	if not configsMap.has_key(config):
		configsMap[config] = set()
	if (cFile in configsMap[config]):
		print 'Duplicate File!', cFile, configsMap[config]

	configsMap[config].add(cFile)


for cFile in cFiles:
	for config,cFileSet in configsMap.iteritems():
		if not cFile in cFileSet:
			print config+','+cFile+','+ getCategory(cFile)