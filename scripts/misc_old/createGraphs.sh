#!/bin/bash

input="../data-ml/cbmc/cbmc-1000-samples-all.txt"
#input="../data-ml/misc/sv-filenames.txt"
dir="../sv-comp/sv-benchmarks/c"
wFlags="-Wno-logical-op-parentheses -Wno-incompatible-library-redeclaration -Wno-tautological-compare -Wno-return-type -Wno-parentheses-equality -Wno-unknown-attributes"
while IFS= read -r line
do
	IFS=',' read -ra ADDR <<< "$line"
	IFS='/' read -ra ADDR2 <<< "${ADDR[0]}"
	file=${ADDR2[1]}
	fileRPath="${ADDR2[0]}/${ADDR2[1]}"
	echo "$fileRPath"
	if [ ! -f ../graphs/llvm-ir/$file.ll ]; then
		clang $wFlags -S -emit-llvm $dir/$fileRPath -o ../graphs/llvm-ir/$file.ll	
	fi	
	opt -dot-callgraph ../graphs/llvm-ir/$file.ll > tmp.log
	mv callgraph.dot ../graphs/callgraphs/$file.dot
	
	clang $wFlags -cc1 -ast-dump $dir/$fileRPath > ../graphs/asts/$file.dot

	opt -dot-cfg ../graphs/llvm-ir/$file.ll > tmp.log
	for f in *.dot
	do
		mv $f ../graphs/cfgs/$file.$f
	done
done < "$input"