#!/bin/bash

input="../data-ml/cbmc/cbmc-100-samples.txt"
#input="../data-ml/misc/sv-filenames.txt"
dir="../sv-comp/sv-benchmarks/c"
wFlags="-Wno-logical-op-parentheses -Wno-incompatible-library-redeclaration -Wno-tautological-compare -Wno-return-type -Wno-parentheses-equality -Wno-unknown-attributes"
while IFS= read -r line
do
	IFS=',' read -ra ADDR <<< "$line"
	IFS='/' read -ra ADDR2 <<< "${ADDR[0]}"
	file=${ADDR2[1]}
	fileRPath="${ADDR2[0]}/${ADDR2[1]}"
	echo "$fileRPath"
	if [ -f ../pdgs/$file.json ]
	then
		echo "$file.json exists!"
		continue
	else
  		# clang $wFlags -S -emit-llvm $dir/$fileRPath -o ../pdgs/$file.ll
  		# llvm-dg-dump -c ../pdgs/$file.ll > ../pdgs/$file.dot
  		dot -Txdot_json ../pdgs/$file.dot -o ../pdgs/$file.json
  	fi
	#break
done < "$input"