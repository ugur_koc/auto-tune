#!/usr/bin/env python2.7

import re,os
from subprocess import call

f = open('../stats/jbmc/stat_default_config.txt')
stats = f.read().splitlines()
f.close()

fileMap={}
for line in stats:
	items=line.split(',')
	assert (not fileMap.has_key(items[0])), 'dublicate file ' + items[0]
	fileMap[items[0]] = items[1]


f = open('../data-ml/jbmc/tasks.txt')
verditcs = f.read().splitlines()
f.close()

for line in verditcs:
	items=line.split(',')
	assert (fileMap.has_key(items[0])), 'verdict missing for file ' + items[0]
	print line+','+fileMap[items[0]]