#!/usr/bin/env python2.7

import re,os
import Utils_Config as cu
import subprocess as sp

HOME_DIR='/Users/ukoc'
BASE_DIR='{}/sv/'.format(HOME_DIR)
DATA_DIR='{}/sv/data-ml/cbmc'.format(HOME_DIR)

#trainFile = '{}/cbmc-train-1.arff'.format(DATA_DIR)
trainFile = '{}/cbmc-singleline.arff'.format(DATA_DIR)
testFile =  '{}/cbmc-test-1.arff'.format(DATA_DIR)
modelFile= '{}/cbmc-MCC-1.dot'.format(DATA_DIR)
predFile = '{}/cbmc-test-1-pred.arff'.format(DATA_DIR)
predSaveFile='{}/cbmc-test-1-saved-pred.txt'.format(DATA_DIR)
fileNamesFile='{}/cbmc-filenames.txt'.format(DATA_DIR)

questions='?,?,?,?,?,?,?,'

def createFileNames(fileNamesFile):
	f = open(fileNamesFile,'r')
	lines = f.read().splitlines()
	f.close()
	fileMap={}
	featureMap={}
	for line in lines:
		features, file = line.rsplit(',',1)
		features=features.split(',')
		features[11]='1'
		features=','.join(features)
		#if featureMap.has_key(features) and featureMap[features]!=file:
		# 	print featureMap[features], file, 'files with same feature vector'
		# else:
		featureMap[features]=file

		if fileMap.has_key(file) and fileMap[file]!=features:
			print 'Same file with diff feature vectors'
		else:
			fileMap[file]=features

	return featureMap, fileMap
	

def pred2Config(predSaveFile, featureMap, fileMap):
	metadata, dataLines = getMetaAndDataLines(predSaveFile)
	usedFiles=set()
	for line in dataLines:
		features = line.split(',')
		options = features[:7]
		features = features[7:]
		key=','.join(features)
		cfile = featureMap[key]
		print options, features, cfile
		break
		config = 'cbmc --propertyfile ' +BASE_DIR+ fs[2] + '.prp ' +BASE_DIR + cfile + ' '
		config += '--drop-unused-functions ' if options[0]=='1' else ''
		config += '--full-slice ' if options[1]=='1' else ''
		config += '--no-assumptions ' if options[2]=='1' else ''
		config += '--partial-loops ' if options[3]=='1' else ''
		config += '--refine ' if options[4]=='1' else ''
		config += '--refine-strings ' if options[5]=='1' else ''
		config += '' if options[6]=='0' else options[6]+' '

metadata, dataset = readArff(testFile)

f = open(predFile,'wr')
f.write(metadata+'\n'+'\n'.join(dataset))
f.close()

featureMap, fileMap = createFileNames(fileNamesFile)

# runMekaTest(modelFile,trainFile,predFile,predSaveFile)
pred2Config(predSaveFile, featureMap, fileMap)
