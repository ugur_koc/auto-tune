#!/bin/bash

grep "seq-mthreaded/rekh_aso_true-unreach-call.3.M1.c" ../data-ml/csv-files/cbmc.csv
grep "bitvector/jain_7_false-no-overflow.i" ../data-ml/csv-files/cbmc.csv
grep "termination-memory-alloca/cstrncpy-alloca_true-termination.c.i" ../data-ml/csv-files/cbmc.csv
grep "seq-mthreaded/pals_STARTPALS_Triplicated_true-unreach-call.ufo.UNBOUNDED.pals.c" ../data-ml/csv-files/cbmc.csv
grep "recursive-simple/fibo_25_true-unreach-call.c" ../data-ml/csv-files/cbmc.csv
grep "termination-crafted/Binary_Search_false-no-overflow.c" ../data-ml/csv-files/cbmc.csv
grep "eca-rers2012/Problem15_label16_true-unreach-call.c" ../data-ml/csv-files/cbmc.csv
grep "eca-rers2012/Problem18_label24_true-unreach-call.c" ../data-ml/csv-files/cbmc.csv
grep "termination-15/cstrncmp_diffterm_alloca_true-termination.c.i" ../data-ml/csv-files/cbmc.csv
grep "termination-memory-linkedlists/cll_traverse-alloca_false-termination.c.i" ../data-ml/csv-files/cbmc.csv
grep "pthread-wmm/mix050_tso.oepc_false-unreach-call.i" ../data-ml/csv-files/cbmc.csv
grep "pthread-wmm/mix025_power.oepc_false-unreach-call.i" ../data-ml/csv-files/cbmc.csv
grep "pthread-wmm/mix051_power.opt_false-unreach-call.i" ../data-ml/csv-files/cbmc.csv
grep "seq-pthread/cs_time_var_mutex_true-unreach-call.i" ../data-ml/csv-files/cbmc.csv
grep "eca-rers2012/Problem19_label19_false-unreach-call.c" ../data-ml/csv-files/cbmc.csv
grep "termination-memory-alloca/easySum-alloca_true-termination.c.i" ../data-ml/csv-files/cbmc.csv
grep "array-examples/standard_two_index_01_true-unreach-call.i" ../data-ml/csv-files/cbmc.csv
grep "pthread-wmm/mix002_rmo.opt_false-unreach-call.i" ../data-ml/csv-files/cbmc.csv
grep "floats-cbmc-regression/float-div1_true-unreach-call.i" ../data-ml/csv-files/cbmc.csv
grep "pthread-wmm/mix003_rmo.oepc_false-unreach-call.i" ../data-ml/csv-files/cbmc.csv
grep "eca-rers2012/Problem15_label06_true-unreach-call.c" ../data-ml/csv-files/cbmc.csv
grep "pthread-wmm/rfi008_pso.oepc_false-unreach-call.i" ../data-ml/csv-files/cbmc.csv
grep "ssh/s3_srvr.blast.02_false-unreach-call.i.cil.c" ../data-ml/csv-files/cbmc.csv
grep "eca-rers2012/Problem19_label35_true-unreach-call.c" ../data-ml/csv-files/cbmc.csv
grep "termination-memory-alloca/HarrisLalNoriRajamani-2010SAS-Fig1-alloca_true-termination.c.i" ../data-ml/csv-files/cbmc.csv
grep "eca-rers2012/Problem16_label37_false-unreach-call.c" ../data-ml/csv-files/cbmc.csv
grep "array-memsafety/lis_unsafe_false-valid-deref.i" ../data-ml/csv-files/cbmc.csv
grep "pthread-wmm/mix032_rmo.oepc_false-unreach-call.i" ../data-ml/csv-files/cbmc.csv
grep "array-memsafety/reverse_array_unsafe_false-valid-deref.i" ../data-ml/csv-files/cbmc.csv
grep "eca-rers2012/Problem07_label37_false-unreach-call.c" ../data-ml/csv-files/cbmc.csv
grep "pthread-wmm/rfi007_rmo.oepc_false-unreach-call.i" ../data-ml/csv-files/cbmc.csv
grep "eca-rers2012/Problem10_label37_true-unreach-call.c" ../data-ml/csv-files/cbmc.csv
grep "eca-rers2012/Problem06_label53_true-unreach-call.c" ../data-ml/csv-files/cbmc.csv
grep "termination-15/cstrlen_malloc_true-termination.c.i" ../data-ml/csv-files/cbmc.csv
grep "termination-15/cstrpbrk_diffterm_alloca_true-termination.c.i" ../data-ml/csv-files/cbmc.csv
grep "recursive-simple/fibo_20_true-unreach-call.c" ../data-ml/csv-files/cbmc.csv
grep "pthread-wmm/safe005_tso.opt_true-unreach-call.i" ../data-ml/csv-files/cbmc.csv
grep "eca-rers2012/Problem04_label01_true-unreach-call.c" ../data-ml/csv-files/cbmc.csv
grep "termination-crafted/Pure3Phase_false-no-overflow.c" ../data-ml/csv-files/cbmc.csv
grep "eca-rers2012/Problem09_label16_true-unreach-call.c" ../data-ml/csv-files/cbmc.csv
grep "eca-rers2012/Problem13_label20_true-unreach-call.c" ../data-ml/csv-files/cbmc.csv
grep "eca-rers2012/Problem03_label31_false-unreach-call.c" ../data-ml/csv-files/cbmc.csv
grep "eca-rers2012/Problem10_label24_false-unreach-call.c" ../data-ml/csv-files/cbmc.csv
grep "eca-rers2012/Problem12_label16_true-unreach-call.c" ../data-ml/csv-files/cbmc.csv
grep "ldv-races/race-2_3-container_of_false-unreach-call.i" ../data-ml/csv-files/cbmc.csv
grep "termination-crafted-lit/ChawdharyCookGulwaniSagivYang-ESOP2008-aaron1_false-no-overflow.c" ../data-ml/csv-files/cbmc.csv
grep "pthread-wmm/safe008_pso.opt_false-unreach-call.i" ../data-ml/csv-files/cbmc.csv
grep "pthread-wmm/mix050_tso.opt_false-unreach-call.i" ../data-ml/csv-files/cbmc.csv
grep "eca-rers2012/Problem13_label46_true-unreach-call.c" ../data-ml/csv-files/cbmc.csv
grep "pthread-wmm/mix015_pso.opt_false-unreach-call.i" ../data-ml/csv-files/cbmc.csv
grep "pthread-wmm/mix025_rmo.opt_false-unreach-call.i" ../data-ml/csv-files/cbmc.csv
grep "pthread-wmm/safe003_tso.oepc_true-unreach-call.i" ../data-ml/csv-files/cbmc.csv
grep "eca-rers2012/Problem05_label49_true-unreach-call.c" ../data-ml/csv-files/cbmc.csv
grep "eca-rers2012/Problem13_label37_true-unreach-call.c" ../data-ml/csv-files/cbmc.csv
grep "array-examples/sorting_bubblesort_false-unreach-call_ground.i" ../data-ml/csv-files/cbmc.csv
grep "pthread-wmm/podwr000_pso.oepc_false-unreach-call.i" ../data-ml/csv-files/cbmc.csv
grep "eca-rers2012/Problem19_label11_false-unreach-call.c" ../data-ml/csv-files/cbmc.csv
grep "pthread-ext/30_Function_Pointer3_vs_true-unreach-call.i" ../data-ml/csv-files/cbmc.csv
grep "eca-rers2012/Problem07_label50_true-unreach-call.c" ../data-ml/csv-files/cbmc.csv
grep "termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex2.17_false-no-overflow.c" ../data-ml/csv-files/cbmc.csv
grep "pthread-wmm/mix011_pso.oepc_false-unreach-call.i" ../data-ml/csv-files/cbmc.csv
grep "pthread-wmm/rfi010_pso.opt_false-unreach-call.i" ../data-ml/csv-files/cbmc.csv
grep "pthread-wmm/safe021_power.oepc_false-unreach-call.i" ../data-ml/csv-files/cbmc.csv
grep "termination-crafted-lit/NoriSharma-FSE2013-Fig7_false-no-overflow.c" ../data-ml/csv-files/cbmc.csv
grep "pthread-wmm/safe024_power.opt_false-unreach-call.i" ../data-ml/csv-files/cbmc.csv
grep "floats-esbmc-regression/digits_bad_while_false-unreach-call.i" ../data-ml/csv-files/cbmc.csv
grep "loop-acceleration/array3_false-valid-deref.i" ../data-ml/csv-files/cbmc.csv
grep "termination-memory-alloca/openbsd_cstrlen-alloca_true-termination.c.i" ../data-ml/csv-files/cbmc.csv
grep "pthread-wmm/safe012_power.opt_false-unreach-call.i" ../data-ml/csv-files/cbmc.csv
grep "ldv-memsafety/ArraysOfVariableLength2_false-valid-deref-read.c" ../data-ml/csv-files/cbmc.csv
grep "eca-rers2012/Problem07_label14_true-unreach-call.c" ../data-ml/csv-files/cbmc.csv
grep "seq-mthreaded/rekcba_aso_false-unreach-call.4.M1.c" ../data-ml/csv-files/cbmc.csv
grep "pthread-wmm/mix004_power.opt_false-unreach-call.i" ../data-ml/csv-files/cbmc.csv
grep "pthread-wmm/mix018_pso.oepc_false-unreach-call.i" ../data-ml/csv-files/cbmc.csv
grep "eca-rers2012/Problem04_label51_true-unreach-call.c" ../data-ml/csv-files/cbmc.csv
grep "pthread-wmm/safe007_rmo.opt_false-unreach-call.i" ../data-ml/csv-files/cbmc.csv
grep "pthread-wmm/mix024_rmo.oepc_false-unreach-call.i" ../data-ml/csv-files/cbmc.csv
grep "eca-rers2012/Problem12_label00_false-unreach-call.c" ../data-ml/csv-files/cbmc.csv
grep "pthread-wmm/mix039_pso.oepc_false-unreach-call.i" ../data-ml/csv-files/cbmc.csv
grep "eca-rers2012/Problem07_label35_false-unreach-call.c" ../data-ml/csv-files/cbmc.csv
grep "eca-rers2012/Problem18_label01_false-unreach-call.c" ../data-ml/csv-files/cbmc.csv
grep "pthread-wmm/safe026_tso.oepc_true-unreach-call.i" ../data-ml/csv-files/cbmc.csv
grep "termination-recursive-malloc/chunk3_true-termination.c.i" ../data-ml/csv-files/cbmc.csv
grep "pthread-wmm/mix026_tso.oepc_false-unreach-call.i" ../data-ml/csv-files/cbmc.csv
grep "array-tiling/skipped_true-unreach-call.i" ../data-ml/csv-files/cbmc.csv
grep "eca-rers2012/Problem05_label34_true-unreach-call.c" ../data-ml/csv-files/cbmc.csv
grep "ldv-memsafety/memleaks_test15_false-valid-memtrack.i" ../data-ml/csv-files/cbmc.csv
grep "pthread-wmm/safe021_rmo.oepc_false-unreach-call.i" ../data-ml/csv-files/cbmc.csv
grep "pthread-wmm/safe027_tso.opt_true-unreach-call.i" ../data-ml/csv-files/cbmc.csv
grep "termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-counterex1a_false-no-overflow.c" ../data-ml/csv-files/cbmc.csv
grep "pthread-wmm/mix040_power.oepc_false-unreach-call.i" ../data-ml/csv-files/cbmc.csv
grep "loop-acceleration/diamond_true-unreach-call2.i" ../data-ml/csv-files/cbmc.csv
grep "pthread-wmm/thin001_power.opt_false-unreach-call.i" ../data-ml/csv-files/cbmc.csv
grep "pthread-wmm/mix044_pso.opt_false-unreach-call.i" ../data-ml/csv-files/cbmc.csv
grep "pthread-wmm/mix019_pso.opt_false-unreach-call.i" ../data-ml/csv-files/cbmc.csv
grep "eca-rers2012/Problem05_label31_true-unreach-call.c" ../data-ml/csv-files/cbmc.csv
grep "eca-rers2012/Problem05_label25_true-unreach-call.c" ../data-ml/csv-files/cbmc.csv
grep "recursive-simple/id_b3_o2_false-no-overflow.c" ../data-ml/csv-files/cbmc.csv
grep "termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-aaron3_false-no-overflow.c" ../data-ml/csv-files/cbmc.csv
grep "pthread-wmm/mix054_pso.oepc_false-unreach-call.i" ../data-ml/csv-files/cbmc.csv