#!/usr/bin/env python2.7

import re,os, json
from subprocess import call
from os.path import isfile, join

truePattern = re.compile("true-.*")

edgeTypes={}
edgeTypeIndx=0
directory='../pdgs'

def getTypeStr(edge):
	typeStr = edge['color'] + '::' + edge['label']

	# optional fields
	typeStr+='::' + ('NA' if not edge.has_key('rank') else edge['rank'])
	typeStr+='::' + ('NA' if not edge.has_key('style') else edge['style'])
	typeStr+='::' + ('NA' if not edge.has_key('penwidth') else edge['penwidth'])
	return typeStr

jsonFiles = [join(directory, f) for f in os.listdir(directory) if isfile(join(directory, f)) and '.json' in f]
for filePath in jsonFiles:
	pathArr=filePath.split('/')
	if '.DS_Store' in pathArr[-1]:
		continue
	datastore = json.load(open(filePath, 'r'))
	
	nodes = [node for node in datastore['objects'] if 'NODE' in node['name']]
	edges = []
	for edge in datastore['edges']:
		#print edge
		typeStr = getTypeStr(edge)
		if not edgeTypes.has_key(typeStr):
			edgeTypes[typeStr] = edgeTypeIndx
			edgeTypeIndx += 1

		edges.append((edge['head'], edgeTypes[typeStr], edge['tail']))

	print edges
	
	graph[nid]=[]
	if edges != '':
		edge_pairs =[ e.split(',') for e in edges.split(':')]
		for dep_type, target_id in edge_pairs:
			graph[nid].append([dep_types[dep_type], int(target_id)])

	datapoint=OrderedDict()
	datapoint['targets']=[[0,1] if truePattern.match(filePath) else [1,0]]
	datapoint['graph']=graph
	datapoint['node_features']=node_features
	
	dataset = []
	dataset.append(datapoint)