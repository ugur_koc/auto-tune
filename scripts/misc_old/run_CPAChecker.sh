#!/bin/bash

targetFile="../sv-benchmarks/c/array-examples/standard_vararg_true-unreach-call_ground_true-termination.c"

declare -a configs=("config/valueAnalysis-bam.properties" "config/valueAnalysis-bam-rec.properties" "config/valueAnalysis-BDD-bool-3600s.properties" "config/valueAnalysis-BDD-bool-intEQ-intADD.properties" "config/valueAnalysis-BDD-bool-intEQ.properties" "config/valueAnalysis-BDD-bool.properties" "config/valueAnalysis-Cegar-Optimized.properties" "config/valueAnalysis-Cegar.properties" "config/valueAnalysis-ConcreteCounterexampleCheck.properties" "config/valueAnalysis-concurrency.properties" "config/valueAnalysis-featureVars.properties" "config/valueAnalysis-GlobalRefiner.properties" "config/valueAnalysis-ItpRefiner-ABElf.properties" "config/valueAnalysis-ItpRefiner-ABEl.properties" "config/valueAnalysis-ItpRefiner.properties" "config/valueAnalysis-ItpRefiner-weightedTraversal.properties" "config/valueAnalysis-java.properties" "config/valueAnalysis-java-with-RTT.properties" "config/valueAnalysis-join.properties" "config/valueAnalysis-no-cex-check.properties" "config/valueAnalysis-NoRefiner.properties" "config/valueAnalysis-parallelBam-ItpRefiner.properties" "config/valueAnalysis-parallelBam.properties" "config/valueAnalysis-pcc-result-check-arg.properties" "config/valueAnalysis-pcc-result-check-partial-partitioned.properties" "config/valueAnalysis-pcc-trac-allARG.properties" "config/valueAnalysis-pcc-trac-all.properties" "config/valueAnalysis-Plain.properties" "config/valueAnalysis-predicateAnalysis--refsel.properties" "config/valueAnalysis-proofcheck-multiedges-defaultprop.properties" "config/valueAnalysis-proofcheck.properties" "config/valueAnalysis.properties" "config/valueAnalysis-resultcheck.properties" "config/valueAnalysis-thresholds.properties" "config/valueAnalysis-weightedTraversal.properties")

for conf in "${configs[@]}" 
do
	echo "$conf"
	./scripts/cpa.sh -config $conf -timelimit 900 $targetFile
done
