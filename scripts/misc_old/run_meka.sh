#!/bin/bash

for (( i = 1; i <= 1; i++ )); do
	java -cp "/Users/ukoc/meka/lib/*" meka.classifiers.multilabel.MCC -Is 0 -Iy 10 -P "Exact match" -S 0 -verbosity 3 -x 5 \
	-t ~/sv/data-ml/cbmc/cbmc-train-$i.arff -d ~/sv/data-ml/cbmc/cbmc-MCC-$i.dot -R \
	-W weka.classifiers.trees.J48 -- -C 0.25 -M 2
	# java -cp "/Users/ukoc/meka/lib/*" meka.classifiers.multilabel.BR -verbosity 3 -x 5 -t ~/sv/data-ml/cbmc/cbmc-train-$i.arff -d ~/sv/data-ml/cbmc/cbmc-BR-$i.dot -R -W weka.classifiers.trees.J48 -- -C 0.25 -M 2
	# java -cp "/Users/ukoc/meka/lib/*" meka.classifiers.multitarget.BCC -verbosity 3 -x 5 -t ~/sv/data-ml/cbmc/cbmc-train-$i.arff -d ~/sv/data-ml/cbmc/cbmc-BCC-$i.dot -X Ibf -S 0 -R -W weka.classifiers.trees.J48 -- -C 0.25 -M 2
	# java -cp "/Users/ukoc/meka/lib/*" meka.classifiers.multitarget.CC -verbosity 3 -x 5 -t ~/sv/data-ml/cbmc/cbmc-train-$i.arff -d ~/sv/data-ml/cbmc/cbmc-CC-$i.dot -S 0 -R -W weka.classifiers.trees.J48 -- -C 0.25 -M 2
	# java -cp "/Users/ukoc/meka/lib/*" meka.classifiers.multitarget.CR -verbosity 3 -x 5 -t ~/sv/data-ml/cbmc/cbmc-train-$i.arff -d ~/sv/data-ml/cbmc/cbmc-CR-$i.dot -s 0 -R -W weka.classifiers.trees.J48 -- -C 0.25 -M 2

	# test
	java -cp "/Users/ukoc/meka/lib/*" meka.classifiers.multilabel.MCC -verbosity 3 -l ~/sv/data-ml/cbmc/cbmc-MCC-$i.dot -t ~/sv/data-ml/cbmc/cbmc-singleline.arff -T ~/sv/data-ml/cbmc/cbmc-test-$i-pred.arff -predictions ~/sv/data-ml/cbmc/cbmc-MCC-$i.txt -W weka.classifiers.trees.J48
	# java -cp "/Users/ukoc/meka/lib/*" meka.classifiers.multitarget.CC -verbosity 3 -x 5 -l ~/sv/data-ml/cbmc/cbmc-CC-$i.dot -T ~/sv/data-ml/cbmc/cbmc-test-$i.arff -predictions ~/sv/data-ml/cbmc/cbmc-CC-$i.txt -S 0 -R -W weka.classifiers.trees.J48 -- -C 0.25 -M 2
	# java -cp "/Users/ukoc/meka/lib/*" meka.classifiers.multitarget.CR -verbosity 3 -x 5 -l ~/sv/data-ml/cbmc/cbmc-CR-$i.dot -T ~/sv/data-ml/cbmc/cbmc-test-$i.arff -predictions ~/sv/data-ml/cbmc/cbmc-CR-$i.txt -s 0 -R -W weka.classifiers.trees.J48 -- -C 0.25 -M 2
done

#-split-percentage 80 