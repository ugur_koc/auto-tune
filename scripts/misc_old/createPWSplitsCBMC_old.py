#!/usr/bin/env python2.7

import re,os,csv
import Utils_ML as mlu
import Utils_Config as conf
import subprocess as sp

#BASE_DIR = '../data-ml/cbmc/classification'
BASE_DIR = '../data-ml/cbmc/regression'
filePattern = '../data-ml/cbmc/cbmc-test-pw-fn-XYZ-1000.txt'
testProgs = []
for x in xrange(1,6): 
	with open(filePattern.replace('XYZ',str(x)),'r') as file: testProgs.append(file.read().splitlines())

def createPrecisionTestsPW(t):
	optionNames, optionSettings = conf.readDomFile('../doms/cbmc/CBMC-21opts-list.txt')
	configs  = conf.useCoveringArray(t, optionSettings = optionSettings)
	testFile = BASE_DIR + '/All-May14.arff'
	metadata, dataset = mlu.readArff(testFile, 0)
	
	testMaps = [{}, {}, {}, {}, {}]
	for dp in dataset:
		pName = dp[38+44]
		label = 'TN' if 'true-' in pName else 'TP'
		for x in xrange(0,5):
			if dp[-1:][0] in testProgs[x]:
				prog = ','.join(dp[22:-4]) + ',' + label + ',' + dp[35+44]
				if not testMaps[x].has_key(pName):
					testMaps[x][pName] = prog

	for x in xrange(0,5):
		for pName, prog in testMaps[x].iteritems():
			saveFile = BASE_DIR+'/test-progs/'+os.path.basename(pName)+'-t'+str(t)+'-split-' +str(x+1)+'.arff'
			datapoints=[]
			for config in configs:
				line = list(config)+ prog.split(',')
				datapoints.append(line)

			with open(saveFile, "wb") as sf:
				sf.write(metadata+'\n')
				writer = csv.writer(sf)
				writer.writerows(datapoints)
				sf.close()
			outs = sp.call(['java','-jar','predPrecision.jar', saveFile, pName])


def writeArrfFile(file,data,metadata):
	with open(file, "wb") as sf:
		sf.write(metadata+'\n')
		writer = csv.writer(sf)
		writer.writerows(data)
		sf.close()

def createPrecisionSplitsPW():
	metadata, dataset = mlu.readArff(BASE_DIR + '/All-Jul14-reg.arff', 0)
	testMaps = [[],[],[],[],[]]
	trainMaps = [[],[],[],[],[]]
	for dp in dataset:
		pName=dp[36+45] # adding llvm inst count 45
		for x in xrange(0,5):
			if pName in testProgs[x]:
				testMaps[x].append(dp[:-1])
			else:
				trainMaps[x].append(dp[:-1])

	saveFiles = [[BASE_DIR+'/test-1.arff',BASE_DIR+'/train-1.arff'],
				[BASE_DIR+'/test-2.arff',BASE_DIR+'/train-2.arff'],
				[BASE_DIR+'/test-3.arff',BASE_DIR+'/train-3.arff'],
				[BASE_DIR+'/test-4.arff',BASE_DIR+'/train-4.arff'],
				[BASE_DIR+'/test-5.arff',BASE_DIR+'/train-5.arff']]
	for x in xrange(0,5):
		writeArrfFile(saveFiles[x][0],testMaps[x],metadata)
		writeArrfFile(saveFiles[x][1],trainMaps[x],metadata)

createPrecisionSplitsPW()