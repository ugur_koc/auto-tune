#!/usr/bin/env python2.7

import re,os
import subprocess as sp

#FILES_DIR='screening-designs/'
FILES_DIR=''
EXE_DIR='/home/ukoc/sv/'

f1 = open(FILES_DIR+'exp_log.txt','r')
lines = f1.read().split('\n')
f1.close()
stats = []
statLine=''
status=''
targetFile=''
label=''
config = ''
time = ''
for line in	lines:
	#print(line)
	filePath=EXE_DIR+line
	if line.startswith("Parsing "):
		targetFile+=line.split(' ')[-1]
	elif line.startswith("VERIFICATION "):
		status = line.split(' ')[-1]
		if status == 'SUCCESSFUL':
			label = 'TN' if 'true-' in targetFile else 'FN'
		else:
			label = 'TP' if 'false-' in targetFile else 'FP'
		stats.append(targetFile+','+status+','+label+','+time)
		targetFile = ''
		status = 'INCOMP'
		label ='NA'


f = open(FILES_DIR+'exp_stat.txt','w')
f.write('\n'.join(stats))
f.close()