#!/bin/bash

trainFile="/home/ukoc/sv/data-ml/cbmc/precision-train-1.arff"
testFile="/home/ukoc/sv/data-ml/cbmc/precision-test-1.arff"
domFile="/home/ukoc/sv/doms/cbmc/CBMC-21opts-list.txt"

java -jar autotune.jar $trainFile $testFile pthread-wmm/mix050_tso.opt_false-unreach-call.i $domFile ReachSafety
java -jar autotune.jar $trainFile $testFile loop-acceleration/diamond_true-unreach-call2.i $domFile ReachSafety
java -jar autotune.jar $trainFile $testFile pthread-wmm/mix011_pso.oepc_false-unreach-call.i $domFile ReachSafety
java -jar autotune.jar $trainFile $testFile pthread-wmm/mix003_rmo.oepc_false-unreach-call.i $domFile ReachSafety
java -jar autotune.jar $trainFile $testFile eca-rers2012/Problem13_label46_true-unreach-call.c $domFile ReachSafety
java -jar autotune.jar $trainFile $testFile eca-rers2012/Problem06_label53_true-unreach-call.c $domFile ReachSafety
java -jar autotune.jar $trainFile $testFile seq-pthread/cs_time_var_mutex_true-unreach-call.i $domFile ReachSafety
java -jar autotune.jar $trainFile $testFile termination-memory-linkedlists/cll_traverse-alloca_false-termination.c.i $domFile Termination
java -jar autotune.jar $trainFile $testFile pthread-wmm/safe008_pso.opt_false-unreach-call.i $domFile ReachSafety
java -jar autotune.jar $trainFile $testFile recursive-simple/id_b3_o2_false-no-overflow.c $domFile NoOverflows
java -jar autotune.jar $trainFile $testFile pthread-wmm/mix024_rmo.oepc_false-unreach-call.i $domFile ReachSafety
java -jar autotune.jar $trainFile $testFile pthread-wmm/mix025_power.oepc_false-unreach-call.i $domFile ReachSafety
java -jar autotune.jar $trainFile $testFile pthread-wmm/safe027_tso.opt_true-unreach-call.i $domFile ReachSafety
java -jar autotune.jar $trainFile $testFile eca-rers2012/Problem18_label24_true-unreach-call.c $domFile ReachSafety
java -jar autotune.jar $trainFile $testFile termination-crafted-lit/NoriSharma-FSE2013-Fig7_false-no-overflow.c $domFile NoOverflows
java -jar autotune.jar $trainFile $testFile pthread-wmm/mix019_pso.opt_false-unreach-call.i $domFile ReachSafety
java -jar autotune.jar $trainFile $testFile eca-rers2012/Problem16_label37_false-unreach-call.c $domFile ReachSafety
java -jar autotune.jar $trainFile $testFile eca-rers2012/Problem12_label00_false-unreach-call.c $domFile ReachSafety
java -jar autotune.jar $trainFile $testFile pthread-wmm/mix050_tso.oepc_false-unreach-call.i $domFile ReachSafety
java -jar autotune.jar $trainFile $testFile termination-memory-alloca/openbsd_cstrlen-alloca_true-termination.c.i $domFile Termination