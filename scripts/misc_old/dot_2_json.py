#!/usr/bin/env python2.7
import sys, networkx, json, pygraphviz, os
from networkx.readwrite import json_graph
from sets import Set
from os import path
from collections import OrderedDict


zeros = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
minusOnes = [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1]
featuresMap = {}
cFiles = Set()
testSets = [[], [], [], [], []]
trainSets = [[], [], [], [], []]

filePattern = '../data-ml/cbmc/cbmc-test-pw-fn-XYZ-1000.txt'
testProgs = []
for x in xrange(1,6): 
    with open(filePattern.replace('XYZ',str(x)),'r') as file: testProgs.append(file.read().splitlines())

def load_features(features_file):
    domFile = open(features_file, 'r')
    featureLines = domFile.read().splitlines()
    domFile.close()
    for line in featureLines:
        fields=line.split(',')
        featuresMap[fields[0]]=list(map(int, fields[1:]))
        cFiles.add(fields[0].split('::')[0])

def dot_to_json(cFile, file_in, file_out):
    G = pygraphviz.AGraph()
    G.read(file_in)
    targets=[[0,1]] if 'false-' in file_in else [[1,0]]
    adj= OrderedDict()
    adj['targets'] = targets
    nodeMap={}
    counter = 0
    graph=[]
    node_features=[]
    nodes = G.nodes()
    for n in nodes:
        if not nodeMap.has_key(n):
            nodeMap[n] = counter
            counter += 1
    for n in nodes:
        for _,tn in G.edges(n):
            egdeType = int(2*n.attr.has_key('label')) + int(tn.attr.has_key('label')) # 0 lib to lib; 1 lib to prog; 2 prog to lib; 3 prog to prog
            graph.append([nodeMap[n], egdeType, nodeMap[tn]])
        funcDecription = cFile+'::'+n.attr['label'][1:-1]
        featureVec = featuresMap.get(funcDecription, zeros) if n.attr.has_key('label') else minusOnes
        node_features.append(featureVec)
    adj['graph'] = graph # sorted(graph, key=lambda x:x[0])
    adj['node_features'] = node_features
    return adj

if __name__ == "__main__":
    CG_DIR = '../graphs/callgraphs/'
    features_file='../data-ml/cbmc/cbmc-1000-samples-func.txt'
    testFileOut = '../ml/ggnn/data/cbmc-test-{}.json'
    trainFileOut = '../ml/ggnn/data/cbmc-train-{}.json'
    load_features(features_file)
    for cFile in cFiles:
        gFile=CG_DIR+cFile.split('/')[-1]
        dotFile=gFile+'.dot'
        jsonFile=gFile+'.json'
        if path.exists(dotFile) and os.stat(dotFile).st_size != 0:
            dp = dot_to_json(cFile, dotFile, jsonFile)
            for i in xrange(0,5):
                dataset=testSets[i] if cFile in testProgs[i] else trainSets[i]
                dataset.append(dp)
        else:
            print dotFile + ' empty or does not exist!'

    for i in xrange(0,5):
        with open(testFileOut.format(str(i+1)), 'w') as ftest: json.dump(testSets[i], ftest)
        with open(trainFileOut.format(str(i+1)), 'w') as ftrain: json.dump(trainSets[i], ftrain)