#!/usr/bin/env python2.7

import xml.etree.ElementTree as ET
import glob


trueStr='true'
falseStr='false'
xmlFiles = glob.glob("../data-sv/All-Raw-2018/*.xml")
files=set()
saveFile=open('all.txt','w')
saveFile.write('tool,name,prop,cpuenergy,cputime,host,memUsage,status,walltime,comp\n')

for f in xmlFiles:
	tree = ET.parse(f)
	root = tree.getroot()
	benchmarkname = root.get('benchmarkname')
	for run in root.iter('run'):
		name = run.get('name')
		prop = run.get('properties')
		comp='no'
		values=[]
		label='NA'
		for col in run.iter('column'):
			if not 'hidden' in col.attrib:
				value = col.get('value')
				values.append(value)
				if col.get('title') == 'status':
					if (value == trueStr or value.startswith(falseStr)):
						comp='yes'
						
						# Find Label  TODO
						# labelCategory = '_'+(trueStr if value == trueStr else falseStr)+'-'+prop
						# if value == trueStr and labelCategory in name:
		saveFile.write(benchmarkname+','+name+','+prop+','+",".join(values) + ',' + comp+'\n')
		if name not in files:
			files.add(name)

saveFile.close()

print '\n'.join(files)