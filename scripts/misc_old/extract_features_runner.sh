#!/bin/bash

wFlags="-Wno-logical-op-parentheses -Wno-incompatible-library-redeclaration -Wno-tautological-compare -Wno-return-type -Wno-parentheses-equality -Wno-unknown-attributes"
dir="../sv-comp/sv-benchmarks/c"

clang $wFlags -S -emit-llvm $dir/floats-cbmc-regression/float-div1_true-unreach-call.i -o $dir/floats-cbmc-regression/float-div1_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/floats-cbmc-regression/float-div1_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/floats-cbmc-regression/float-no-simp8_true-unreach-call.i -o $dir/floats-cbmc-regression/float-no-simp8_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/floats-cbmc-regression/float-no-simp8_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/floats-cbmc-regression/float14_true-unreach-call.i -o $dir/floats-cbmc-regression/float14_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/floats-cbmc-regression/float14_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/floats-cbmc-regression/float21_true-unreach-call.i -o $dir/floats-cbmc-regression/float21_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/floats-cbmc-regression/float21_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/floats-cbmc-regression/float8_true-unreach-call.i -o $dir/floats-cbmc-regression/float8_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/floats-cbmc-regression/float8_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/floats-cbmc-regression/float_lib1_true-unreach-call.i -o $dir/floats-cbmc-regression/float_lib1_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/floats-cbmc-regression/float_lib1_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/array-examples/data_structures_set_multi_proc_false-unreach-call_ground.c -o $dir/array-examples/data_structures_set_multi_proc_false-unreach-call_ground.i.ll
features_static_analysis/extractFeatures $dir/array-examples/data_structures_set_multi_proc_false-unreach-call_ground.i.ll
clang $wFlags -S -emit-llvm $dir/array-examples/sanfoundry_10_true-unreach-call_ground.i -o $dir/array-examples/sanfoundry_10_true-unreach-call_ground.i.ll
features_static_analysis/extractFeatures $dir/array-examples/sanfoundry_10_true-unreach-call_ground.i.ll
clang $wFlags -S -emit-llvm $dir/array-examples/sanfoundry_43_true-unreach-call_ground.i -o $dir/array-examples/sanfoundry_43_true-unreach-call_ground.i.ll
features_static_analysis/extractFeatures $dir/array-examples/sanfoundry_43_true-unreach-call_ground.i.ll
clang $wFlags -S -emit-llvm $dir/array-examples/sorting_bubblesort_false-unreach-call2_ground.i -o $dir/array-examples/sorting_bubblesort_false-unreach-call2_ground.i.ll
features_static_analysis/extractFeatures $dir/array-examples/sorting_bubblesort_false-unreach-call2_ground.i.ll
clang $wFlags -S -emit-llvm $dir/array-examples/sorting_bubblesort_false-unreach-call_ground.i -o $dir/array-examples/sorting_bubblesort_false-unreach-call_ground.i.ll
features_static_analysis/extractFeatures $dir/array-examples/sorting_bubblesort_false-unreach-call_ground.i.ll
clang $wFlags -S -emit-llvm $dir/array-examples/sorting_selectionsort_true-unreach-call_ground.i -o $dir/array-examples/sorting_selectionsort_true-unreach-call_ground.i.ll
features_static_analysis/extractFeatures $dir/array-examples/sorting_selectionsort_true-unreach-call_ground.i.ll
clang $wFlags -S -emit-llvm $dir/array-examples/standard_allDiff2_false-unreach-call_ground.i -o $dir/array-examples/standard_allDiff2_false-unreach-call_ground.i.ll
features_static_analysis/extractFeatures $dir/array-examples/standard_allDiff2_false-unreach-call_ground.i.ll
clang $wFlags -S -emit-llvm $dir/array-examples/standard_copy1_true-unreach-call_ground.i -o $dir/array-examples/standard_copy1_true-unreach-call_ground.i.ll
features_static_analysis/extractFeatures $dir/array-examples/standard_copy1_true-unreach-call_ground.i.ll
clang $wFlags -S -emit-llvm $dir/array-examples/standard_copy2_false-unreach-call_ground.i -o $dir/array-examples/standard_copy2_false-unreach-call_ground.i.ll
features_static_analysis/extractFeatures $dir/array-examples/standard_copy2_false-unreach-call_ground.i.ll
clang $wFlags -S -emit-llvm $dir/array-examples/standard_copy3_true-unreach-call_ground.i -o $dir/array-examples/standard_copy3_true-unreach-call_ground.i.ll
features_static_analysis/extractFeatures $dir/array-examples/standard_copy3_true-unreach-call_ground.i.ll
clang $wFlags -S -emit-llvm $dir/array-examples/standard_copy4_true-unreach-call_ground.i -o $dir/array-examples/standard_copy4_true-unreach-call_ground.i.ll
features_static_analysis/extractFeatures $dir/array-examples/standard_copy4_true-unreach-call_ground.i.ll
clang $wFlags -S -emit-llvm $dir/array-examples/standard_copy5_true-unreach-call_ground.i -o $dir/array-examples/standard_copy5_true-unreach-call_ground.i.ll
features_static_analysis/extractFeatures $dir/array-examples/standard_copy5_true-unreach-call_ground.i.ll
clang $wFlags -S -emit-llvm $dir/array-examples/standard_copy6_false-unreach-call_ground.i -o $dir/array-examples/standard_copy6_false-unreach-call_ground.i.ll
features_static_analysis/extractFeatures $dir/array-examples/standard_copy6_false-unreach-call_ground.i.ll
clang $wFlags -S -emit-llvm $dir/array-examples/standard_copyInitSum_true-unreach-call_ground.i -o $dir/array-examples/standard_copyInitSum_true-unreach-call_ground.i.ll
features_static_analysis/extractFeatures $dir/array-examples/standard_copyInitSum_true-unreach-call_ground.i.ll
clang $wFlags -S -emit-llvm $dir/array-examples/standard_init3_true-unreach-call_ground.i -o $dir/array-examples/standard_init3_true-unreach-call_ground.i.ll
features_static_analysis/extractFeatures $dir/array-examples/standard_init3_true-unreach-call_ground.i.ll
clang $wFlags -S -emit-llvm $dir/array-examples/standard_init5_false-unreach-call_ground.i -o $dir/array-examples/standard_init5_false-unreach-call_ground.i.ll
features_static_analysis/extractFeatures $dir/array-examples/standard_init5_false-unreach-call_ground.i.ll
clang $wFlags -S -emit-llvm $dir/array-examples/standard_init5_true-unreach-call_ground.i -o $dir/array-examples/standard_init5_true-unreach-call_ground.i.ll
features_static_analysis/extractFeatures $dir/array-examples/standard_init5_true-unreach-call_ground.i.ll
clang $wFlags -S -emit-llvm $dir/array-examples/standard_init6_false-unreach-call_ground.i -o $dir/array-examples/standard_init6_false-unreach-call_ground.i.ll
features_static_analysis/extractFeatures $dir/array-examples/standard_init6_false-unreach-call_ground.i.ll
clang $wFlags -S -emit-llvm $dir/array-examples/standard_init8_false-unreach-call_ground.i -o $dir/array-examples/standard_init8_false-unreach-call_ground.i.ll
features_static_analysis/extractFeatures $dir/array-examples/standard_init8_false-unreach-call_ground.i.ll
clang $wFlags -S -emit-llvm $dir/array-examples/standard_maxInArray_true-unreach-call_ground.i -o $dir/array-examples/standard_maxInArray_true-unreach-call_ground.i.ll
features_static_analysis/extractFeatures $dir/array-examples/standard_maxInArray_true-unreach-call_ground.i.ll
clang $wFlags -S -emit-llvm $dir/array-examples/standard_minInArray_true-unreach-call_ground.i -o $dir/array-examples/standard_minInArray_true-unreach-call_ground.i.ll
features_static_analysis/extractFeatures $dir/array-examples/standard_minInArray_true-unreach-call_ground.i.ll
clang $wFlags -S -emit-llvm $dir/array-examples/standard_partition_original_true-unreach-call_ground.i -o $dir/array-examples/standard_partition_original_true-unreach-call_ground.i.ll
features_static_analysis/extractFeatures $dir/array-examples/standard_partition_original_true-unreach-call_ground.i.ll
clang $wFlags -S -emit-llvm $dir/array-examples/standard_partition_true-unreach-call_ground.i -o $dir/array-examples/standard_partition_true-unreach-call_ground.i.ll
features_static_analysis/extractFeatures $dir/array-examples/standard_partition_true-unreach-call_ground.i.ll
clang $wFlags -S -emit-llvm $dir/array-examples/standard_password_true-unreach-call_ground.i -o $dir/array-examples/standard_password_true-unreach-call_ground.i.ll
features_static_analysis/extractFeatures $dir/array-examples/standard_password_true-unreach-call_ground.i.ll
clang $wFlags -S -emit-llvm $dir/array-examples/standard_strcmp_true-unreach-call_ground.i -o $dir/array-examples/standard_strcmp_true-unreach-call_ground.i.ll
features_static_analysis/extractFeatures $dir/array-examples/standard_strcmp_true-unreach-call_ground.i.ll
clang $wFlags -S -emit-llvm $dir/array-examples/standard_strcpy_false-valid-deref_ground.i -o $dir/array-examples/standard_strcpy_false-valid-deref_ground.i.ll
features_static_analysis/extractFeatures $dir/array-examples/standard_strcpy_false-valid-deref_ground.i.ll
clang $wFlags -S -emit-llvm $dir/array-examples/standard_two_index_01_true-unreach-call.i -o $dir/array-examples/standard_two_index_01_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/array-examples/standard_two_index_01_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/array-examples/standard_two_index_02_true-unreach-call.i -o $dir/array-examples/standard_two_index_02_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/array-examples/standard_two_index_02_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/array-examples/standard_two_index_03_true-unreach-call.i -o $dir/array-examples/standard_two_index_03_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/array-examples/standard_two_index_03_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/array-examples/standard_two_index_04_true-unreach-call.i -o $dir/array-examples/standard_two_index_04_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/array-examples/standard_two_index_04_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/array-examples/standard_two_index_05_true-unreach-call.i -o $dir/array-examples/standard_two_index_05_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/array-examples/standard_two_index_05_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/array-examples/standard_two_index_06_true-unreach-call.i -o $dir/array-examples/standard_two_index_06_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/array-examples/standard_two_index_06_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/array-examples/standard_two_index_08_true-unreach-call.i -o $dir/array-examples/standard_two_index_08_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/array-examples/standard_two_index_08_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/array-industry-pattern/array_of_struct_break_true-unreach-call.i -o $dir/array-industry-pattern/array_of_struct_break_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/array-industry-pattern/array_of_struct_break_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/array-industry-pattern/array_of_struct_ptr_cond_init_true-unreach-call.i -o $dir/array-industry-pattern/array_of_struct_ptr_cond_init_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/array-industry-pattern/array_of_struct_ptr_cond_init_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/array-industry-pattern/array_of_struct_ptr_mul_init_true-unreach-call.i -o $dir/array-industry-pattern/array_of_struct_ptr_mul_init_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/array-industry-pattern/array_of_struct_ptr_mul_init_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/array-industry-pattern/array_ptr_partial_init_true-unreach-call.i -o $dir/array-industry-pattern/array_ptr_partial_init_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/array-industry-pattern/array_ptr_partial_init_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/array-memsafety/count_down_unsafe_false-valid-deref.i -o $dir/array-memsafety/count_down_unsafe_false-valid-deref.i.ll
features_static_analysis/extractFeatures $dir/array-memsafety/count_down_unsafe_false-valid-deref.i.ll
clang $wFlags -S -emit-llvm $dir/array-memsafety/lis_unsafe_false-valid-deref.i -o $dir/array-memsafety/lis_unsafe_false-valid-deref.i.ll
features_static_analysis/extractFeatures $dir/array-memsafety/lis_unsafe_false-valid-deref.i.ll
clang $wFlags -S -emit-llvm $dir/array-memsafety/reverse_array_unsafe_false-valid-deref.i -o $dir/array-memsafety/reverse_array_unsafe_false-valid-deref.i.ll
features_static_analysis/extractFeatures $dir/array-memsafety/reverse_array_unsafe_false-valid-deref.i.ll
clang $wFlags -S -emit-llvm $dir/array-memsafety/selectionsort_unsafe_false-valid-deref.i -o $dir/array-memsafety/selectionsort_unsafe_false-valid-deref.i.ll
features_static_analysis/extractFeatures $dir/array-memsafety/selectionsort_unsafe_false-valid-deref.i.ll
clang $wFlags -S -emit-llvm $dir/array-programs/copysome1_false-unreach-call.i -o $dir/array-programs/copysome1_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/array-programs/copysome1_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/array-tiling/mbpr2_true-unreach-call.i -o $dir/array-tiling/mbpr2_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/array-tiling/mbpr2_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/array-tiling/nr4_true-unreach-call.i -o $dir/array-tiling/nr4_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/array-tiling/nr4_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/array-tiling/nr5_true-unreach-call.i -o $dir/array-tiling/nr5_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/array-tiling/nr5_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/array-tiling/pnr2_true-unreach-call.i -o $dir/array-tiling/pnr2_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/array-tiling/pnr2_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/array-tiling/rewnifrev_true-unreach-call.i -o $dir/array-tiling/rewnifrev_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/array-tiling/rewnifrev_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/array-tiling/tcpy_true-unreach-call.i -o $dir/array-tiling/tcpy_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/array-tiling/tcpy_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/bitvector/jain_7_false-no-overflow.i -o $dir/bitvector/jain_7_false-no-overflow.i.ll
features_static_analysis/extractFeatures $dir/bitvector/jain_7_false-no-overflow.i.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem03_label06_true-unreach-call.c -o $dir/eca-rers2012/Problem03_label06_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem03_label06_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem03_label07_true-unreach-call.c -o $dir/eca-rers2012/Problem03_label07_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem03_label07_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem03_label10_true-unreach-call.c -o $dir/eca-rers2012/Problem03_label10_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem03_label10_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem03_label12_true-unreach-call.c -o $dir/eca-rers2012/Problem03_label12_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem03_label12_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem03_label15_true-unreach-call.c -o $dir/eca-rers2012/Problem03_label15_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem03_label15_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem03_label21_true-unreach-call.c -o $dir/eca-rers2012/Problem03_label21_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem03_label21_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem03_label23_true-unreach-call.c -o $dir/eca-rers2012/Problem03_label23_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem03_label23_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem03_label24_true-unreach-call.c -o $dir/eca-rers2012/Problem03_label24_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem03_label24_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem03_label26_false-unreach-call.c -o $dir/eca-rers2012/Problem03_label26_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem03_label26_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem03_label28_false-unreach-call.c -o $dir/eca-rers2012/Problem03_label28_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem03_label28_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem03_label29_true-unreach-call.c -o $dir/eca-rers2012/Problem03_label29_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem03_label29_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem03_label30_true-unreach-call.c -o $dir/eca-rers2012/Problem03_label30_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem03_label30_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem03_label32_true-unreach-call.c -o $dir/eca-rers2012/Problem03_label32_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem03_label32_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem03_label33_true-unreach-call.c -o $dir/eca-rers2012/Problem03_label33_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem03_label33_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem03_label39_false-unreach-call.c -o $dir/eca-rers2012/Problem03_label39_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem03_label39_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem03_label40_true-unreach-call.c -o $dir/eca-rers2012/Problem03_label40_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem03_label40_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem03_label46_true-unreach-call.c -o $dir/eca-rers2012/Problem03_label46_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem03_label46_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem03_label48_true-unreach-call.c -o $dir/eca-rers2012/Problem03_label48_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem03_label48_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem03_label50_false-unreach-call.c -o $dir/eca-rers2012/Problem03_label50_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem03_label50_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem03_label53_true-unreach-call.c -o $dir/eca-rers2012/Problem03_label53_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem03_label53_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem03_label54_true-unreach-call.c -o $dir/eca-rers2012/Problem03_label54_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem03_label54_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem03_label55_true-unreach-call.c -o $dir/eca-rers2012/Problem03_label55_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem03_label55_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem04_label00_true-unreach-call.c -o $dir/eca-rers2012/Problem04_label00_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem04_label00_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem04_label02_true-unreach-call.c -o $dir/eca-rers2012/Problem04_label02_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem04_label02_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem04_label06_false-unreach-call.c -o $dir/eca-rers2012/Problem04_label06_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem04_label06_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem04_label07_true-unreach-call.c -o $dir/eca-rers2012/Problem04_label07_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem04_label07_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem04_label16_true-unreach-call.c -o $dir/eca-rers2012/Problem04_label16_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem04_label16_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem04_label22_true-unreach-call.c -o $dir/eca-rers2012/Problem04_label22_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem04_label22_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem04_label25_true-unreach-call.c -o $dir/eca-rers2012/Problem04_label25_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem04_label25_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem04_label35_false-unreach-call.c -o $dir/eca-rers2012/Problem04_label35_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem04_label35_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem04_label48_true-unreach-call.c -o $dir/eca-rers2012/Problem04_label48_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem04_label48_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem04_label51_true-unreach-call.c -o $dir/eca-rers2012/Problem04_label51_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem04_label51_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem04_label53_true-unreach-call.c -o $dir/eca-rers2012/Problem04_label53_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem04_label53_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem04_label54_true-unreach-call.c -o $dir/eca-rers2012/Problem04_label54_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem04_label54_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem05_label03_true-unreach-call.c -o $dir/eca-rers2012/Problem05_label03_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem05_label03_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem05_label04_true-unreach-call.c -o $dir/eca-rers2012/Problem05_label04_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem05_label04_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem05_label08_true-unreach-call.c -o $dir/eca-rers2012/Problem05_label08_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem05_label08_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem05_label09_true-unreach-call.c -o $dir/eca-rers2012/Problem05_label09_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem05_label09_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem05_label12_true-unreach-call.c -o $dir/eca-rers2012/Problem05_label12_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem05_label12_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem05_label16_true-unreach-call.c -o $dir/eca-rers2012/Problem05_label16_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem05_label16_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem05_label24_false-unreach-call.c -o $dir/eca-rers2012/Problem05_label24_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem05_label24_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem05_label26_false-unreach-call.c -o $dir/eca-rers2012/Problem05_label26_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem05_label26_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem05_label33_false-unreach-call.c -o $dir/eca-rers2012/Problem05_label33_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem05_label33_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem05_label34_true-unreach-call.c -o $dir/eca-rers2012/Problem05_label34_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem05_label34_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem05_label37_false-unreach-call.c -o $dir/eca-rers2012/Problem05_label37_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem05_label37_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem05_label39_false-unreach-call.c -o $dir/eca-rers2012/Problem05_label39_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem05_label39_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem05_label45_true-unreach-call.c -o $dir/eca-rers2012/Problem05_label45_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem05_label45_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem05_label47_false-unreach-call.c -o $dir/eca-rers2012/Problem05_label47_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem05_label47_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem05_label52_true-unreach-call.c -o $dir/eca-rers2012/Problem05_label52_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem05_label52_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem05_label55_false-unreach-call.c -o $dir/eca-rers2012/Problem05_label55_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem05_label55_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem05_label59_true-unreach-call.c -o $dir/eca-rers2012/Problem05_label59_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem05_label59_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem06_label03_true-unreach-call.c -o $dir/eca-rers2012/Problem06_label03_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem06_label03_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem06_label09_false-unreach-call.c -o $dir/eca-rers2012/Problem06_label09_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem06_label09_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem06_label14_true-unreach-call.c -o $dir/eca-rers2012/Problem06_label14_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem06_label14_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem06_label15_false-unreach-call.c -o $dir/eca-rers2012/Problem06_label15_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem06_label15_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem06_label17_true-unreach-call.c -o $dir/eca-rers2012/Problem06_label17_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem06_label17_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem06_label19_true-unreach-call.c -o $dir/eca-rers2012/Problem06_label19_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem06_label19_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem06_label21_false-unreach-call.c -o $dir/eca-rers2012/Problem06_label21_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem06_label21_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem06_label23_true-unreach-call.c -o $dir/eca-rers2012/Problem06_label23_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem06_label23_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem06_label24_false-unreach-call.c -o $dir/eca-rers2012/Problem06_label24_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem06_label24_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem06_label26_true-unreach-call.c -o $dir/eca-rers2012/Problem06_label26_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem06_label26_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem06_label27_false-unreach-call.c -o $dir/eca-rers2012/Problem06_label27_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem06_label27_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem06_label28_true-unreach-call.c -o $dir/eca-rers2012/Problem06_label28_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem06_label28_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem06_label29_false-unreach-call.c -o $dir/eca-rers2012/Problem06_label29_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem06_label29_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem06_label30_true-unreach-call.c -o $dir/eca-rers2012/Problem06_label30_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem06_label30_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem06_label31_true-unreach-call.c -o $dir/eca-rers2012/Problem06_label31_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem06_label31_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem06_label32_true-unreach-call.c -o $dir/eca-rers2012/Problem06_label32_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem06_label32_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem06_label33_false-unreach-call.c -o $dir/eca-rers2012/Problem06_label33_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem06_label33_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem06_label41_true-unreach-call.c -o $dir/eca-rers2012/Problem06_label41_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem06_label41_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem06_label43_true-unreach-call.c -o $dir/eca-rers2012/Problem06_label43_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem06_label43_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem06_label44_false-unreach-call.c -o $dir/eca-rers2012/Problem06_label44_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem06_label44_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem06_label47_false-unreach-call.c -o $dir/eca-rers2012/Problem06_label47_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem06_label47_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem06_label50_true-unreach-call.c -o $dir/eca-rers2012/Problem06_label50_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem06_label50_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem06_label51_true-unreach-call.c -o $dir/eca-rers2012/Problem06_label51_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem06_label51_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem06_label52_true-unreach-call.c -o $dir/eca-rers2012/Problem06_label52_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem06_label52_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem06_label53_true-unreach-call.c -o $dir/eca-rers2012/Problem06_label53_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem06_label53_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem06_label55_true-unreach-call.c -o $dir/eca-rers2012/Problem06_label55_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem06_label55_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem06_label59_false-unreach-call.c -o $dir/eca-rers2012/Problem06_label59_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem06_label59_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem07_label02_true-unreach-call.c -o $dir/eca-rers2012/Problem07_label02_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem07_label02_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem07_label07_false-unreach-call.c -o $dir/eca-rers2012/Problem07_label07_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem07_label07_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem07_label08_true-unreach-call.c -o $dir/eca-rers2012/Problem07_label08_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem07_label08_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem07_label09_false-unreach-call.c -o $dir/eca-rers2012/Problem07_label09_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem07_label09_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem07_label12_true-unreach-call.c -o $dir/eca-rers2012/Problem07_label12_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem07_label12_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem07_label14_true-unreach-call.c -o $dir/eca-rers2012/Problem07_label14_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem07_label14_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem07_label15_false-unreach-call.c -o $dir/eca-rers2012/Problem07_label15_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem07_label15_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem07_label20_false-unreach-call.c -o $dir/eca-rers2012/Problem07_label20_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem07_label20_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem07_label22_true-unreach-call.c -o $dir/eca-rers2012/Problem07_label22_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem07_label22_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem07_label23_false-unreach-call.c -o $dir/eca-rers2012/Problem07_label23_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem07_label23_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem07_label25_true-unreach-call.c -o $dir/eca-rers2012/Problem07_label25_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem07_label25_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem07_label27_true-unreach-call.c -o $dir/eca-rers2012/Problem07_label27_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem07_label27_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem07_label28_true-unreach-call.c -o $dir/eca-rers2012/Problem07_label28_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem07_label28_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem07_label29_true-unreach-call.c -o $dir/eca-rers2012/Problem07_label29_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem07_label29_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem07_label30_false-unreach-call.c -o $dir/eca-rers2012/Problem07_label30_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem07_label30_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem07_label32_true-unreach-call.c -o $dir/eca-rers2012/Problem07_label32_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem07_label32_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem07_label37_false-unreach-call.c -o $dir/eca-rers2012/Problem07_label37_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem07_label37_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem07_label41_true-unreach-call.c -o $dir/eca-rers2012/Problem07_label41_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem07_label41_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem07_label45_true-unreach-call.c -o $dir/eca-rers2012/Problem07_label45_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem07_label45_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem07_label52_true-unreach-call.c -o $dir/eca-rers2012/Problem07_label52_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem07_label52_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem07_label55_true-unreach-call.c -o $dir/eca-rers2012/Problem07_label55_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem07_label55_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem07_label56_true-unreach-call.c -o $dir/eca-rers2012/Problem07_label56_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem07_label56_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem07_label57_true-unreach-call.c -o $dir/eca-rers2012/Problem07_label57_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem07_label57_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem07_label58_false-unreach-call.c -o $dir/eca-rers2012/Problem07_label58_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem07_label58_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem08_label03_true-unreach-call.c -o $dir/eca-rers2012/Problem08_label03_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem08_label03_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem08_label04_false-unreach-call.c -o $dir/eca-rers2012/Problem08_label04_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem08_label04_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem08_label12_true-unreach-call.c -o $dir/eca-rers2012/Problem08_label12_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem08_label12_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem08_label16_true-unreach-call.c -o $dir/eca-rers2012/Problem08_label16_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem08_label16_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem08_label19_true-unreach-call.c -o $dir/eca-rers2012/Problem08_label19_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem08_label19_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem08_label26_false-unreach-call.c -o $dir/eca-rers2012/Problem08_label26_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem08_label26_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem08_label27_true-unreach-call.c -o $dir/eca-rers2012/Problem08_label27_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem08_label27_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem08_label29_false-unreach-call.c -o $dir/eca-rers2012/Problem08_label29_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem08_label29_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem08_label37_false-unreach-call.c -o $dir/eca-rers2012/Problem08_label37_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem08_label37_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem08_label39_true-unreach-call.c -o $dir/eca-rers2012/Problem08_label39_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem08_label39_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem08_label47_true-unreach-call.c -o $dir/eca-rers2012/Problem08_label47_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem08_label47_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem08_label52_true-unreach-call.c -o $dir/eca-rers2012/Problem08_label52_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem08_label52_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem08_label58_true-unreach-call.c -o $dir/eca-rers2012/Problem08_label58_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem08_label58_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem08_label59_false-unreach-call.c -o $dir/eca-rers2012/Problem08_label59_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem08_label59_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem09_label00_true-unreach-call.c -o $dir/eca-rers2012/Problem09_label00_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem09_label00_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem09_label01_true-unreach-call.c -o $dir/eca-rers2012/Problem09_label01_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem09_label01_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem09_label12_true-unreach-call.c -o $dir/eca-rers2012/Problem09_label12_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem09_label12_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem09_label13_true-unreach-call.c -o $dir/eca-rers2012/Problem09_label13_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem09_label13_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem09_label17_true-unreach-call.c -o $dir/eca-rers2012/Problem09_label17_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem09_label17_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem09_label18_true-unreach-call.c -o $dir/eca-rers2012/Problem09_label18_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem09_label18_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem09_label20_false-unreach-call.c -o $dir/eca-rers2012/Problem09_label20_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem09_label20_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem09_label23_true-unreach-call.c -o $dir/eca-rers2012/Problem09_label23_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem09_label23_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem09_label27_true-unreach-call.c -o $dir/eca-rers2012/Problem09_label27_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem09_label27_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem09_label30_true-unreach-call.c -o $dir/eca-rers2012/Problem09_label30_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem09_label30_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem09_label34_false-unreach-call.c -o $dir/eca-rers2012/Problem09_label34_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem09_label34_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem09_label37_true-unreach-call.c -o $dir/eca-rers2012/Problem09_label37_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem09_label37_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem09_label40_true-unreach-call.c -o $dir/eca-rers2012/Problem09_label40_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem09_label40_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem09_label44_false-unreach-call.c -o $dir/eca-rers2012/Problem09_label44_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem09_label44_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem09_label45_true-unreach-call.c -o $dir/eca-rers2012/Problem09_label45_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem09_label45_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem09_label55_true-unreach-call.c -o $dir/eca-rers2012/Problem09_label55_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem09_label55_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem09_label58_true-unreach-call.c -o $dir/eca-rers2012/Problem09_label58_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem09_label58_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem10_label00_true-unreach-call.c -o $dir/eca-rers2012/Problem10_label00_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem10_label00_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem10_label05_true-unreach-call.c -o $dir/eca-rers2012/Problem10_label05_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem10_label05_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem10_label07_true-unreach-call.c -o $dir/eca-rers2012/Problem10_label07_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem10_label07_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem10_label12_false-unreach-call.c -o $dir/eca-rers2012/Problem10_label12_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem10_label12_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem10_label15_false-unreach-call.c -o $dir/eca-rers2012/Problem10_label15_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem10_label15_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem10_label16_true-unreach-call.c -o $dir/eca-rers2012/Problem10_label16_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem10_label16_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem10_label17_true-unreach-call.c -o $dir/eca-rers2012/Problem10_label17_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem10_label17_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem10_label19_true-unreach-call.c -o $dir/eca-rers2012/Problem10_label19_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem10_label19_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem10_label24_false-unreach-call.c -o $dir/eca-rers2012/Problem10_label24_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem10_label24_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem10_label25_true-unreach-call.c -o $dir/eca-rers2012/Problem10_label25_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem10_label25_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem10_label27_true-unreach-call.c -o $dir/eca-rers2012/Problem10_label27_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem10_label27_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem10_label29_false-unreach-call.c -o $dir/eca-rers2012/Problem10_label29_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem10_label29_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem10_label30_true-unreach-call.c -o $dir/eca-rers2012/Problem10_label30_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem10_label30_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem10_label32_true-unreach-call.c -o $dir/eca-rers2012/Problem10_label32_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem10_label32_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem10_label33_true-unreach-call.c -o $dir/eca-rers2012/Problem10_label33_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem10_label33_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem10_label35_true-unreach-call.c -o $dir/eca-rers2012/Problem10_label35_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem10_label35_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem10_label36_true-unreach-call.c -o $dir/eca-rers2012/Problem10_label36_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem10_label36_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem10_label40_true-unreach-call.c -o $dir/eca-rers2012/Problem10_label40_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem10_label40_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem10_label43_true-unreach-call.c -o $dir/eca-rers2012/Problem10_label43_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem10_label43_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem10_label46_false-unreach-call.c -o $dir/eca-rers2012/Problem10_label46_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem10_label46_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem10_label50_false-unreach-call.c -o $dir/eca-rers2012/Problem10_label50_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem10_label50_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem10_label53_true-unreach-call.c -o $dir/eca-rers2012/Problem10_label53_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem10_label53_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem11_label03_true-unreach-call.c -o $dir/eca-rers2012/Problem11_label03_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem11_label03_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem11_label13_true-unreach-call.c -o $dir/eca-rers2012/Problem11_label13_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem11_label13_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem11_label16_true-unreach-call.c -o $dir/eca-rers2012/Problem11_label16_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem11_label16_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem11_label17_true-unreach-call.c -o $dir/eca-rers2012/Problem11_label17_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem11_label17_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem11_label21_true-unreach-call.c -o $dir/eca-rers2012/Problem11_label21_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem11_label21_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem11_label22_true-unreach-call.c -o $dir/eca-rers2012/Problem11_label22_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem11_label22_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem11_label23_true-unreach-call.c -o $dir/eca-rers2012/Problem11_label23_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem11_label23_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem11_label37_true-unreach-call.c -o $dir/eca-rers2012/Problem11_label37_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem11_label37_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem11_label40_true-unreach-call.c -o $dir/eca-rers2012/Problem11_label40_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem11_label40_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem11_label43_false-unreach-call.c -o $dir/eca-rers2012/Problem11_label43_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem11_label43_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem11_label44_true-unreach-call.c -o $dir/eca-rers2012/Problem11_label44_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem11_label44_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem11_label49_false-unreach-call.c -o $dir/eca-rers2012/Problem11_label49_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem11_label49_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem11_label50_true-unreach-call.c -o $dir/eca-rers2012/Problem11_label50_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem11_label50_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem11_label58_false-unreach-call.c -o $dir/eca-rers2012/Problem11_label58_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem11_label58_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem11_label59_true-unreach-call.c -o $dir/eca-rers2012/Problem11_label59_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem11_label59_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem12_label04_true-unreach-call.c -o $dir/eca-rers2012/Problem12_label04_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem12_label04_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem12_label06_false-unreach-call.c -o $dir/eca-rers2012/Problem12_label06_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem12_label06_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem12_label22_true-unreach-call.c -o $dir/eca-rers2012/Problem12_label22_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem12_label22_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem12_label23_true-unreach-call.c -o $dir/eca-rers2012/Problem12_label23_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem12_label23_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem12_label25_false-unreach-call.c -o $dir/eca-rers2012/Problem12_label25_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem12_label25_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem12_label26_true-unreach-call.c -o $dir/eca-rers2012/Problem12_label26_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem12_label26_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem12_label27_true-unreach-call.c -o $dir/eca-rers2012/Problem12_label27_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem12_label27_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem12_label28_false-unreach-call.c -o $dir/eca-rers2012/Problem12_label28_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem12_label28_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem12_label32_true-unreach-call.c -o $dir/eca-rers2012/Problem12_label32_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem12_label32_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem12_label33_true-unreach-call.c -o $dir/eca-rers2012/Problem12_label33_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem12_label33_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem12_label35_false-unreach-call.c -o $dir/eca-rers2012/Problem12_label35_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem12_label35_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem12_label36_true-unreach-call.c -o $dir/eca-rers2012/Problem12_label36_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem12_label36_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem12_label38_false-unreach-call.c -o $dir/eca-rers2012/Problem12_label38_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem12_label38_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem12_label44_true-unreach-call.c -o $dir/eca-rers2012/Problem12_label44_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem12_label44_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem12_label47_true-unreach-call.c -o $dir/eca-rers2012/Problem12_label47_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem12_label47_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem12_label48_false-unreach-call.c -o $dir/eca-rers2012/Problem12_label48_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem12_label48_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem12_label49_true-unreach-call.c -o $dir/eca-rers2012/Problem12_label49_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem12_label49_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem12_label56_true-unreach-call.c -o $dir/eca-rers2012/Problem12_label56_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem12_label56_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem12_label57_true-unreach-call.c -o $dir/eca-rers2012/Problem12_label57_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem12_label57_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem12_label58_true-unreach-call.c -o $dir/eca-rers2012/Problem12_label58_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem12_label58_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem13_label03_true-unreach-call.c -o $dir/eca-rers2012/Problem13_label03_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem13_label03_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem13_label04_false-unreach-call.c -o $dir/eca-rers2012/Problem13_label04_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem13_label04_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem13_label12_false-unreach-call.c -o $dir/eca-rers2012/Problem13_label12_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem13_label12_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem13_label13_true-unreach-call.c -o $dir/eca-rers2012/Problem13_label13_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem13_label13_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem13_label16_false-unreach-call.c -o $dir/eca-rers2012/Problem13_label16_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem13_label16_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem13_label19_false-unreach-call.c -o $dir/eca-rers2012/Problem13_label19_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem13_label19_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem13_label23_false-unreach-call.c -o $dir/eca-rers2012/Problem13_label23_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem13_label23_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem13_label26_true-unreach-call.c -o $dir/eca-rers2012/Problem13_label26_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem13_label26_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem13_label33_true-unreach-call.c -o $dir/eca-rers2012/Problem13_label33_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem13_label33_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem13_label34_true-unreach-call.c -o $dir/eca-rers2012/Problem13_label34_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem13_label34_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem13_label38_true-unreach-call.c -o $dir/eca-rers2012/Problem13_label38_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem13_label38_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem13_label40_false-unreach-call.c -o $dir/eca-rers2012/Problem13_label40_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem13_label40_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem13_label42_true-unreach-call.c -o $dir/eca-rers2012/Problem13_label42_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem13_label42_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem13_label43_false-unreach-call.c -o $dir/eca-rers2012/Problem13_label43_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem13_label43_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem13_label49_true-unreach-call.c -o $dir/eca-rers2012/Problem13_label49_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem13_label49_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem13_label52_true-unreach-call.c -o $dir/eca-rers2012/Problem13_label52_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem13_label52_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem13_label55_true-unreach-call.c -o $dir/eca-rers2012/Problem13_label55_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem13_label55_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem13_label56_true-unreach-call.c -o $dir/eca-rers2012/Problem13_label56_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem13_label56_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem13_label58_false-unreach-call.c -o $dir/eca-rers2012/Problem13_label58_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem13_label58_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem13_label59_true-unreach-call.c -o $dir/eca-rers2012/Problem13_label59_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem13_label59_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem15_label02_false-unreach-call.c -o $dir/eca-rers2012/Problem15_label02_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem15_label02_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem15_label05_true-unreach-call.c -o $dir/eca-rers2012/Problem15_label05_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem15_label05_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem15_label08_true-unreach-call.c -o $dir/eca-rers2012/Problem15_label08_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem15_label08_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem15_label09_false-unreach-call.c -o $dir/eca-rers2012/Problem15_label09_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem15_label09_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem15_label10_true-unreach-call.c -o $dir/eca-rers2012/Problem15_label10_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem15_label10_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem15_label24_true-unreach-call.c -o $dir/eca-rers2012/Problem15_label24_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem15_label24_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem15_label25_false-unreach-call.c -o $dir/eca-rers2012/Problem15_label25_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem15_label25_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem15_label26_true-unreach-call.c -o $dir/eca-rers2012/Problem15_label26_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem15_label26_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem15_label27_true-unreach-call.c -o $dir/eca-rers2012/Problem15_label27_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem15_label27_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem15_label28_true-unreach-call.c -o $dir/eca-rers2012/Problem15_label28_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem15_label28_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem15_label31_true-unreach-call.c -o $dir/eca-rers2012/Problem15_label31_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem15_label31_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem15_label32_true-unreach-call.c -o $dir/eca-rers2012/Problem15_label32_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem15_label32_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem15_label34_false-unreach-call.c -o $dir/eca-rers2012/Problem15_label34_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem15_label34_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem15_label35_true-unreach-call.c -o $dir/eca-rers2012/Problem15_label35_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem15_label35_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem15_label37_false-unreach-call.c -o $dir/eca-rers2012/Problem15_label37_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem15_label37_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem15_label38_false-unreach-call.c -o $dir/eca-rers2012/Problem15_label38_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem15_label38_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem15_label40_false-unreach-call.c -o $dir/eca-rers2012/Problem15_label40_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem15_label40_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem15_label42_true-unreach-call.c -o $dir/eca-rers2012/Problem15_label42_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem15_label42_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem15_label44_true-unreach-call.c -o $dir/eca-rers2012/Problem15_label44_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem15_label44_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem15_label46_true-unreach-call.c -o $dir/eca-rers2012/Problem15_label46_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem15_label46_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem15_label57_true-unreach-call.c -o $dir/eca-rers2012/Problem15_label57_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem15_label57_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem15_label59_true-unreach-call.c -o $dir/eca-rers2012/Problem15_label59_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem15_label59_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem16_label00_false-unreach-call.c -o $dir/eca-rers2012/Problem16_label00_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem16_label00_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem16_label05_false-unreach-call.c -o $dir/eca-rers2012/Problem16_label05_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem16_label05_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem16_label07_true-unreach-call.c -o $dir/eca-rers2012/Problem16_label07_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem16_label07_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem16_label09_true-unreach-call.c -o $dir/eca-rers2012/Problem16_label09_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem16_label09_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem16_label10_true-unreach-call.c -o $dir/eca-rers2012/Problem16_label10_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem16_label10_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem16_label11_true-unreach-call.c -o $dir/eca-rers2012/Problem16_label11_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem16_label11_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem16_label12_true-unreach-call.c -o $dir/eca-rers2012/Problem16_label12_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem16_label12_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem16_label14_false-unreach-call.c -o $dir/eca-rers2012/Problem16_label14_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem16_label14_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem16_label16_true-unreach-call.c -o $dir/eca-rers2012/Problem16_label16_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem16_label16_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem16_label18_false-unreach-call.c -o $dir/eca-rers2012/Problem16_label18_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem16_label18_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem16_label20_false-unreach-call.c -o $dir/eca-rers2012/Problem16_label20_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem16_label20_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem16_label21_true-unreach-call.c -o $dir/eca-rers2012/Problem16_label21_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem16_label21_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem16_label25_true-unreach-call.c -o $dir/eca-rers2012/Problem16_label25_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem16_label25_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem16_label39_true-unreach-call.c -o $dir/eca-rers2012/Problem16_label39_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem16_label39_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem16_label41_false-unreach-call.c -o $dir/eca-rers2012/Problem16_label41_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem16_label41_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem16_label43_false-unreach-call.c -o $dir/eca-rers2012/Problem16_label43_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem16_label43_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem16_label47_true-unreach-call.c -o $dir/eca-rers2012/Problem16_label47_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem16_label47_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem16_label48_true-unreach-call.c -o $dir/eca-rers2012/Problem16_label48_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem16_label48_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem16_label49_true-unreach-call.c -o $dir/eca-rers2012/Problem16_label49_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem16_label49_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem16_label52_false-unreach-call.c -o $dir/eca-rers2012/Problem16_label52_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem16_label52_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem16_label54_false-unreach-call.c -o $dir/eca-rers2012/Problem16_label54_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem16_label54_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem16_label57_true-unreach-call.c -o $dir/eca-rers2012/Problem16_label57_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem16_label57_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem16_label58_true-unreach-call.c -o $dir/eca-rers2012/Problem16_label58_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem16_label58_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem16_label59_true-unreach-call.c -o $dir/eca-rers2012/Problem16_label59_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem16_label59_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem17_label00_true-unreach-call.c -o $dir/eca-rers2012/Problem17_label00_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem17_label00_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem17_label01_true-unreach-call.c -o $dir/eca-rers2012/Problem17_label01_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem17_label01_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem17_label06_true-unreach-call.c -o $dir/eca-rers2012/Problem17_label06_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem17_label06_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem17_label10_true-unreach-call.c -o $dir/eca-rers2012/Problem17_label10_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem17_label10_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem17_label13_false-unreach-call.c -o $dir/eca-rers2012/Problem17_label13_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem17_label13_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem17_label14_true-unreach-call.c -o $dir/eca-rers2012/Problem17_label14_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem17_label14_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem17_label17_true-unreach-call.c -o $dir/eca-rers2012/Problem17_label17_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem17_label17_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem17_label21_true-unreach-call.c -o $dir/eca-rers2012/Problem17_label21_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem17_label21_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem17_label28_true-unreach-call.c -o $dir/eca-rers2012/Problem17_label28_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem17_label28_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem17_label37_false-unreach-call.c -o $dir/eca-rers2012/Problem17_label37_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem17_label37_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem17_label39_true-unreach-call.c -o $dir/eca-rers2012/Problem17_label39_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem17_label39_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem17_label40_false-unreach-call.c -o $dir/eca-rers2012/Problem17_label40_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem17_label40_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem17_label43_true-unreach-call.c -o $dir/eca-rers2012/Problem17_label43_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem17_label43_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem17_label45_true-unreach-call.c -o $dir/eca-rers2012/Problem17_label45_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem17_label45_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem17_label52_false-unreach-call.c -o $dir/eca-rers2012/Problem17_label52_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem17_label52_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem17_label56_true-unreach-call.c -o $dir/eca-rers2012/Problem17_label56_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem17_label56_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem17_label57_false-unreach-call.c -o $dir/eca-rers2012/Problem17_label57_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem17_label57_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem17_label58_true-unreach-call.c -o $dir/eca-rers2012/Problem17_label58_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem17_label58_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem17_label59_true-unreach-call.c -o $dir/eca-rers2012/Problem17_label59_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem17_label59_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem18_label00_false-unreach-call.c -o $dir/eca-rers2012/Problem18_label00_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem18_label00_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem18_label05_true-unreach-call.c -o $dir/eca-rers2012/Problem18_label05_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem18_label05_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem18_label14_true-unreach-call.c -o $dir/eca-rers2012/Problem18_label14_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem18_label14_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem18_label15_true-unreach-call.c -o $dir/eca-rers2012/Problem18_label15_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem18_label15_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem18_label16_true-unreach-call.c -o $dir/eca-rers2012/Problem18_label16_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem18_label16_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem18_label26_true-unreach-call.c -o $dir/eca-rers2012/Problem18_label26_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem18_label26_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem18_label28_true-unreach-call.c -o $dir/eca-rers2012/Problem18_label28_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem18_label28_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem18_label32_false-unreach-call.c -o $dir/eca-rers2012/Problem18_label32_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem18_label32_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem18_label33_false-unreach-call.c -o $dir/eca-rers2012/Problem18_label33_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem18_label33_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem18_label37_true-unreach-call.c -o $dir/eca-rers2012/Problem18_label37_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem18_label37_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem18_label39_false-unreach-call.c -o $dir/eca-rers2012/Problem18_label39_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem18_label39_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem18_label40_true-unreach-call.c -o $dir/eca-rers2012/Problem18_label40_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem18_label40_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem18_label41_true-unreach-call.c -o $dir/eca-rers2012/Problem18_label41_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem18_label41_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem18_label42_true-unreach-call.c -o $dir/eca-rers2012/Problem18_label42_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem18_label42_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem18_label44_true-unreach-call.c -o $dir/eca-rers2012/Problem18_label44_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem18_label44_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem18_label48_true-unreach-call.c -o $dir/eca-rers2012/Problem18_label48_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem18_label48_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem18_label50_true-unreach-call.c -o $dir/eca-rers2012/Problem18_label50_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem18_label50_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem18_label54_true-unreach-call.c -o $dir/eca-rers2012/Problem18_label54_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem18_label54_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem19_label03_true-unreach-call.c -o $dir/eca-rers2012/Problem19_label03_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem19_label03_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem19_label04_true-unreach-call.c -o $dir/eca-rers2012/Problem19_label04_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem19_label04_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem19_label08_true-unreach-call.c -o $dir/eca-rers2012/Problem19_label08_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem19_label08_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem19_label14_false-unreach-call.c -o $dir/eca-rers2012/Problem19_label14_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem19_label14_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem19_label18_false-unreach-call.c -o $dir/eca-rers2012/Problem19_label18_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem19_label18_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem19_label19_false-unreach-call.c -o $dir/eca-rers2012/Problem19_label19_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem19_label19_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem19_label23_true-unreach-call.c -o $dir/eca-rers2012/Problem19_label23_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem19_label23_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem19_label25_true-unreach-call.c -o $dir/eca-rers2012/Problem19_label25_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem19_label25_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem19_label26_false-unreach-call.c -o $dir/eca-rers2012/Problem19_label26_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem19_label26_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem19_label28_false-unreach-call.c -o $dir/eca-rers2012/Problem19_label28_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem19_label28_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem19_label29_false-unreach-call.c -o $dir/eca-rers2012/Problem19_label29_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem19_label29_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem19_label34_true-unreach-call.c -o $dir/eca-rers2012/Problem19_label34_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem19_label34_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem19_label35_true-unreach-call.c -o $dir/eca-rers2012/Problem19_label35_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem19_label35_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem19_label37_true-unreach-call.c -o $dir/eca-rers2012/Problem19_label37_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem19_label37_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem19_label38_true-unreach-call.c -o $dir/eca-rers2012/Problem19_label38_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem19_label38_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem19_label41_false-unreach-call.c -o $dir/eca-rers2012/Problem19_label41_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem19_label41_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem19_label44_true-unreach-call.c -o $dir/eca-rers2012/Problem19_label44_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem19_label44_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem19_label48_true-unreach-call.c -o $dir/eca-rers2012/Problem19_label48_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem19_label48_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem19_label49_true-unreach-call.c -o $dir/eca-rers2012/Problem19_label49_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem19_label49_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/eca-rers2012/Problem19_label53_false-unreach-call.c -o $dir/eca-rers2012/Problem19_label53_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/eca-rers2012/Problem19_label53_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/float-benchs/exp_loop_true-unreach-call.c -o $dir/float-benchs/exp_loop_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/float-benchs/exp_loop_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/float-benchs/filter2_iterated_true-unreach-call.c -o $dir/float-benchs/filter2_iterated_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/float-benchs/filter2_iterated_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/float-benchs/filter_iir_true-unreach-call.c -o $dir/float-benchs/filter_iir_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/float-benchs/filter_iir_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/float-benchs/mea8000_true-unreach-call.c -o $dir/float-benchs/mea8000_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/float-benchs/mea8000_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/float-benchs/sin_interpolated_negation_true-unreach-call.c -o $dir/float-benchs/sin_interpolated_negation_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/float-benchs/sin_interpolated_negation_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/float-benchs/sqrt_biNewton_pseudoconstant_true-unreach-call.c -o $dir/float-benchs/sqrt_biNewton_pseudoconstant_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/float-benchs/sqrt_biNewton_pseudoconstant_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/float-benchs/sqrt_Householder_interval_true-unreach-call.c -o $dir/float-benchs/sqrt_Householder_interval_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/float-benchs/sqrt_Householder_interval_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/float-benchs/sqrt_poly2_false-unreach-call.c -o $dir/float-benchs/sqrt_poly2_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/float-benchs/sqrt_poly2_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/floats-esbmc-regression/ceil_nondet_true-unreach-call.i -o $dir/floats-esbmc-regression/ceil_nondet_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/floats-esbmc-regression/ceil_nondet_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/floats-esbmc-regression/digits_bad_for_false-unreach-call.i -o $dir/floats-esbmc-regression/digits_bad_for_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/floats-esbmc-regression/digits_bad_for_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/floats-esbmc-regression/Double_div_true-unreach-call.i -o $dir/floats-esbmc-regression/Double_div_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/floats-esbmc-regression/Double_div_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/floats-esbmc-regression/Float_div_true-unreach-call.i -o $dir/floats-esbmc-regression/Float_div_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/floats-esbmc-regression/Float_div_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/floats-esbmc-regression/fmin_true-unreach-call.i -o $dir/floats-esbmc-regression/fmin_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/floats-esbmc-regression/fmin_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/floats-esbmc-regression/isless_true-unreach-call.i -o $dir/floats-esbmc-regression/isless_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/floats-esbmc-regression/isless_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/floats-esbmc-regression/isunordered_true-unreach-call.i -o $dir/floats-esbmc-regression/isunordered_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/floats-esbmc-regression/isunordered_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/floats-esbmc-regression/nearbyint2_true-unreach-call.i -o $dir/floats-esbmc-regression/nearbyint2_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/floats-esbmc-regression/nearbyint2_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/floats-esbmc-regression/remainder_true-unreach-call.i -o $dir/floats-esbmc-regression/remainder_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/floats-esbmc-regression/remainder_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/floats-esbmc-regression/round_true-unreach-call.i -o $dir/floats-esbmc-regression/round_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/floats-esbmc-regression/round_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/floats-esbmc-regression/rounding_functions_true-unreach-call.i -o $dir/floats-esbmc-regression/rounding_functions_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/floats-esbmc-regression/rounding_functions_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/floats-esbmc-regression/trunc_nondet_2_true-unreach-call.i -o $dir/floats-esbmc-regression/trunc_nondet_2_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/floats-esbmc-regression/trunc_nondet_2_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/heap-manipulation/tree_false-valid-deref.i -o $dir/heap-manipulation/tree_false-valid-deref.i.ll
features_static_analysis/extractFeatures $dir/heap-manipulation/tree_false-valid-deref.i.ll
clang $wFlags -S -emit-llvm $dir/ldv-memsafety-bitfields/test-bitfields-1_false-valid-deref.i -o $dir/ldv-memsafety-bitfields/test-bitfields-1_false-valid-deref.i.ll
features_static_analysis/extractFeatures $dir/ldv-memsafety-bitfields/test-bitfields-1_false-valid-deref.i.ll
clang $wFlags -S -emit-llvm $dir/ldv-memsafety-bitfields/test-bitfields-2.1_false-valid-free.i -o $dir/ldv-memsafety-bitfields/test-bitfields-2.1_false-valid-free.i.ll
features_static_analysis/extractFeatures $dir/ldv-memsafety-bitfields/test-bitfields-2.1_false-valid-free.i.ll
clang $wFlags -S -emit-llvm $dir/ldv-memsafety-bitfields/test-bitfields-2_false-valid-deref.i -o $dir/ldv-memsafety-bitfields/test-bitfields-2_false-valid-deref.i.ll
features_static_analysis/extractFeatures $dir/ldv-memsafety-bitfields/test-bitfields-2_false-valid-deref.i.ll
clang $wFlags -S -emit-llvm $dir/ldv-memsafety/ArraysOfVariableLength2_false-valid-deref-write.c -o $dir/ldv-memsafety/ArraysOfVariableLength2_false-valid-deref-write.c.ll
features_static_analysis/extractFeatures $dir/ldv-memsafety/ArraysOfVariableLength2_false-valid-deref-write.c.ll
clang $wFlags -S -emit-llvm $dir/ldv-memsafety/ArraysOfVariableLength2_true-valid-memsafety.c -o $dir/ldv-memsafety/ArraysOfVariableLength2_true-valid-memsafety.c.ll
features_static_analysis/extractFeatures $dir/ldv-memsafety/ArraysOfVariableLength2_true-valid-memsafety.c.ll
clang $wFlags -S -emit-llvm $dir/ldv-memsafety/ArraysWithLenghtAtDeclaration_false-valid-deref-write.c -o $dir/ldv-memsafety/ArraysWithLenghtAtDeclaration_false-valid-deref-write.c.ll
features_static_analysis/extractFeatures $dir/ldv-memsafety/ArraysWithLenghtAtDeclaration_false-valid-deref-write.c.ll
clang $wFlags -S -emit-llvm $dir/ldv-memsafety/memleaks_test11_1_false-valid-free.i -o $dir/ldv-memsafety/memleaks_test11_1_false-valid-free.i.ll
features_static_analysis/extractFeatures $dir/ldv-memsafety/memleaks_test11_1_false-valid-free.i.ll
clang $wFlags -S -emit-llvm $dir/ldv-memsafety/memleaks_test12_false-valid-free.i -o $dir/ldv-memsafety/memleaks_test12_false-valid-free.i.ll
features_static_analysis/extractFeatures $dir/ldv-memsafety/memleaks_test12_false-valid-free.i.ll
clang $wFlags -S -emit-llvm $dir/ldv-memsafety/memleaks_test12_true-valid-memsafety.i -o $dir/ldv-memsafety/memleaks_test12_true-valid-memsafety.i.ll
features_static_analysis/extractFeatures $dir/ldv-memsafety/memleaks_test12_true-valid-memsafety.i.ll
clang $wFlags -S -emit-llvm $dir/ldv-memsafety/memleaks_test14_2_false-valid-memtrack.i -o $dir/ldv-memsafety/memleaks_test14_2_false-valid-memtrack.i.ll
features_static_analysis/extractFeatures $dir/ldv-memsafety/memleaks_test14_2_false-valid-memtrack.i.ll
clang $wFlags -S -emit-llvm $dir/ldv-memsafety/memleaks_test14_3_false-valid-deref.i -o $dir/ldv-memsafety/memleaks_test14_3_false-valid-deref.i.ll
features_static_analysis/extractFeatures $dir/ldv-memsafety/memleaks_test14_3_false-valid-deref.i.ll
clang $wFlags -S -emit-llvm $dir/ldv-memsafety/memleaks_test14_true-valid-memsafety.i -o $dir/ldv-memsafety/memleaks_test14_true-valid-memsafety.i.ll
features_static_analysis/extractFeatures $dir/ldv-memsafety/memleaks_test14_true-valid-memsafety.i.ll
clang $wFlags -S -emit-llvm $dir/ldv-memsafety/memleaks_test1_false-valid-free.i -o $dir/ldv-memsafety/memleaks_test1_false-valid-free.i.ll
features_static_analysis/extractFeatures $dir/ldv-memsafety/memleaks_test1_false-valid-free.i.ll
clang $wFlags -S -emit-llvm $dir/ldv-memsafety/memsetNonZero2_false-valid-deref-write.c -o $dir/ldv-memsafety/memsetNonZero2_false-valid-deref-write.c.ll
features_static_analysis/extractFeatures $dir/ldv-memsafety/memsetNonZero2_false-valid-deref-write.c.ll
clang $wFlags -S -emit-llvm $dir/ldv-memsafety/StructInitialization1_true-valid-memsafety.c -o $dir/ldv-memsafety/StructInitialization1_true-valid-memsafety.c.ll
features_static_analysis/extractFeatures $dir/ldv-memsafety/StructInitialization1_true-valid-memsafety.c.ll
clang $wFlags -S -emit-llvm $dir/ldv-races/race-1_3-join_false-unreach-call.i -o $dir/ldv-races/race-1_3-join_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/ldv-races/race-1_3-join_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/ldv-races/race-2_4-container_of_false-unreach-call.i -o $dir/ldv-races/race-2_4-container_of_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/ldv-races/race-2_4-container_of_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/ldv-races/race-4_1-thread_local_vars_true-unreach-call.i -o $dir/ldv-races/race-4_1-thread_local_vars_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/ldv-races/race-4_1-thread_local_vars_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/ldv-races/race-4_2-thread_local_vars_false-unreach-call.i -o $dir/ldv-races/race-4_2-thread_local_vars_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/ldv-races/race-4_2-thread_local_vars_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/ldv-regression/test22_false-unreach-call.c -o $dir/ldv-regression/test22_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/ldv-regression/test22_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/ldv-regression/test23_false-unreach-call.c -o $dir/ldv-regression/test23_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/ldv-regression/test23_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/ldv-regression/test24_false-unreach-call.c -o $dir/ldv-regression/test24_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/ldv-regression/test24_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/ldv-sets/test_mutex_double_unlock_false-unreach-call.i -o $dir/ldv-sets/test_mutex_double_unlock_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/ldv-sets/test_mutex_double_unlock_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/ldv-sets/test_mutex_unbounded_false-unreach-call.i -o $dir/ldv-sets/test_mutex_unbounded_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/ldv-sets/test_mutex_unbounded_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/list-ext-properties/test-0214_1_true-valid-memsafety.i -o $dir/list-ext-properties/test-0214_1_true-valid-memsafety.i.ll
features_static_analysis/extractFeatures $dir/list-ext-properties/test-0214_1_true-valid-memsafety.i.ll
clang $wFlags -S -emit-llvm $dir/list-ext-properties/test-0217_1_true-valid-memsafety.i -o $dir/list-ext-properties/test-0217_1_true-valid-memsafety.i.ll
features_static_analysis/extractFeatures $dir/list-ext-properties/test-0217_1_true-valid-memsafety.i.ll
clang $wFlags -S -emit-llvm $dir/list-ext-properties/test-0504_1_true-valid-memsafety.i -o $dir/list-ext-properties/test-0504_1_true-valid-memsafety.i.ll
features_static_analysis/extractFeatures $dir/list-ext-properties/test-0504_1_true-valid-memsafety.i.ll
clang $wFlags -S -emit-llvm $dir/list-ext-properties/test-0513_1_true-valid-memsafety.i -o $dir/list-ext-properties/test-0513_1_true-valid-memsafety.i.ll
features_static_analysis/extractFeatures $dir/list-ext-properties/test-0513_1_true-valid-memsafety.i.ll
clang $wFlags -S -emit-llvm $dir/list-ext2-properties/simple_and_skiplist_2lvl_false-unreach-call.i -o $dir/list-ext2-properties/simple_and_skiplist_2lvl_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/list-ext2-properties/simple_and_skiplist_2lvl_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/loop-acceleration/phases_false-unreach-call1.i -o $dir/loop-acceleration/phases_false-unreach-call1.i.ll
features_static_analysis/extractFeatures $dir/loop-acceleration/phases_false-unreach-call1.i.ll
clang $wFlags -S -emit-llvm $dir/loop-acceleration/simple_true-unreach-call1.i -o $dir/loop-acceleration/simple_true-unreach-call1.i.ll
features_static_analysis/extractFeatures $dir/loop-acceleration/simple_true-unreach-call1.i.ll
clang $wFlags -S -emit-llvm $dir/loop-acceleration/simple_true-unreach-call4.i -o $dir/loop-acceleration/simple_true-unreach-call4.i.ll
features_static_analysis/extractFeatures $dir/loop-acceleration/simple_true-unreach-call4.i.ll
clang $wFlags -S -emit-llvm $dir/loop-industry-pattern/aiob_2_true-unreach-call.c -o $dir/loop-industry-pattern/aiob_2_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/loop-industry-pattern/aiob_2_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/loop-industry-pattern/nested_true-unreach-call.c -o $dir/loop-industry-pattern/nested_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/loop-industry-pattern/nested_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/loop-industry-pattern/ofuf_1_true-unreach-call.c -o $dir/loop-industry-pattern/ofuf_1_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/loop-industry-pattern/ofuf_1_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/loop-industry-pattern/ofuf_3_true-unreach-call.c -o $dir/loop-industry-pattern/ofuf_3_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/loop-industry-pattern/ofuf_3_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/loop-industry-pattern/ofuf_4_true-unreach-call.c -o $dir/loop-industry-pattern/ofuf_4_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/loop-industry-pattern/ofuf_4_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/loop-industry-pattern/ofuf_5_true-unreach-call.c -o $dir/loop-industry-pattern/ofuf_5_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/loop-industry-pattern/ofuf_5_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/loops/eureka_01_true-unreach-call.i -o $dir/loops/eureka_01_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/loops/eureka_01_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/loops/linear_sea.ch_true-unreach-call.i -o $dir/loops/linear_sea.ch_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/loops/linear_sea.ch_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/loops/lu.cmp_true-unreach-call.i -o $dir/loops/lu.cmp_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/loops/lu.cmp_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/loops/sum_array_false-unreach-call.i -o $dir/loops/sum_array_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/loops/sum_array_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/loops/vogal_false-unreach-call.i -o $dir/loops/vogal_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/loops/vogal_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/loops/vogal_true-unreach-call.i -o $dir/loops/vogal_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/loops/vogal_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/memsafety-ext2/complex_data_creation_test02_true-valid-memsafety.i -o $dir/memsafety-ext2/complex_data_creation_test02_true-valid-memsafety.i.ll
features_static_analysis/extractFeatures $dir/memsafety-ext2/complex_data_creation_test02_true-valid-memsafety.i.ll
clang $wFlags -S -emit-llvm $dir/memsafety-ext2/optional_data_creation_test04_false-valid-memtrack.i -o $dir/memsafety-ext2/optional_data_creation_test04_false-valid-memtrack.i.ll
features_static_analysis/extractFeatures $dir/memsafety-ext2/optional_data_creation_test04_false-valid-memtrack.i.ll
clang $wFlags -S -emit-llvm $dir/memsafety-ext2/split_list_test05_false-valid-deref.i -o $dir/memsafety-ext2/split_list_test05_false-valid-deref.i.ll
features_static_analysis/extractFeatures $dir/memsafety-ext2/split_list_test05_false-valid-deref.i.ll
clang $wFlags -S -emit-llvm $dir/memsafety/20020406-1_false-valid-memtrack.i -o $dir/memsafety/20020406-1_false-valid-memtrack.i.ll
features_static_analysis/extractFeatures $dir/memsafety/20020406-1_false-valid-memtrack.i.ll
clang $wFlags -S -emit-llvm $dir/memsafety/20051113-1.c_false-valid-memtrack.i -o $dir/memsafety/20051113-1.c_false-valid-memtrack.i.ll
features_static_analysis/extractFeatures $dir/memsafety/20051113-1.c_false-valid-memtrack.i.ll
clang $wFlags -S -emit-llvm $dir/memsafety/960521-1_false-valid-free.i -o $dir/memsafety/960521-1_false-valid-free.i.ll
features_static_analysis/extractFeatures $dir/memsafety/960521-1_false-valid-free.i.ll
clang $wFlags -S -emit-llvm $dir/memsafety/lockfree-3.2_false-valid-memtrack.i -o $dir/memsafety/lockfree-3.2_false-valid-memtrack.i.ll
features_static_analysis/extractFeatures $dir/memsafety/lockfree-3.2_false-valid-memtrack.i.ll
clang $wFlags -S -emit-llvm $dir/memsafety/test-0102_true-valid-memsafety.i -o $dir/memsafety/test-0102_true-valid-memsafety.i.ll
features_static_analysis/extractFeatures $dir/memsafety/test-0102_true-valid-memsafety.i.ll
clang $wFlags -S -emit-llvm $dir/memsafety/test-0134_true-valid-memsafety.i -o $dir/memsafety/test-0134_true-valid-memsafety.i.ll
features_static_analysis/extractFeatures $dir/memsafety/test-0134_true-valid-memsafety.i.ll
clang $wFlags -S -emit-llvm $dir/memsafety/test-0137_false-valid-deref.i -o $dir/memsafety/test-0137_false-valid-deref.i.ll
features_static_analysis/extractFeatures $dir/memsafety/test-0137_false-valid-deref.i.ll
clang $wFlags -S -emit-llvm $dir/memsafety/test-0232_false-valid-memtrack.i -o $dir/memsafety/test-0232_false-valid-memtrack.i.ll
features_static_analysis/extractFeatures $dir/memsafety/test-0232_false-valid-memtrack.i.ll
clang $wFlags -S -emit-llvm $dir/memsafety/test-0235_false-valid-deref.i -o $dir/memsafety/test-0235_false-valid-deref.i.ll
features_static_analysis/extractFeatures $dir/memsafety/test-0235_false-valid-deref.i.ll
clang $wFlags -S -emit-llvm $dir/memsafety/test-0235_false-valid-memtrack.i -o $dir/memsafety/test-0235_false-valid-memtrack.i.ll
features_static_analysis/extractFeatures $dir/memsafety/test-0235_false-valid-memtrack.i.ll
clang $wFlags -S -emit-llvm $dir/memsafety/test-0236_true-valid-memsafety.i -o $dir/memsafety/test-0236_true-valid-memsafety.i.ll
features_static_analysis/extractFeatures $dir/memsafety/test-0236_true-valid-memsafety.i.ll
clang $wFlags -S -emit-llvm $dir/memsafety/test-0237_true-valid-memsafety.i -o $dir/memsafety/test-0237_true-valid-memsafety.i.ll
features_static_analysis/extractFeatures $dir/memsafety/test-0237_true-valid-memsafety.i.ll
clang $wFlags -S -emit-llvm $dir/memsafety/test-0521_true-valid-memsafety.i -o $dir/memsafety/test-0521_true-valid-memsafety.i.ll
features_static_analysis/extractFeatures $dir/memsafety/test-0521_true-valid-memsafety.i.ll
clang $wFlags -S -emit-llvm $dir/ntdrivers/cdaudio_true-unreach-call.i.cil.c -o $dir/ntdrivers/cdaudio_true-unreach-call.i.cil.c.ll
features_static_analysis/extractFeatures $dir/ntdrivers/cdaudio_true-unreach-call.i.cil.c.ll
clang $wFlags -S -emit-llvm $dir/ntdrivers/diskperf_true-unreach-call.i.cil.c -o $dir/ntdrivers/diskperf_true-unreach-call.i.cil.c.ll
features_static_analysis/extractFeatures $dir/ntdrivers/diskperf_true-unreach-call.i.cil.c.ll
clang $wFlags -S -emit-llvm $dir/ntdrivers/floppy_false-unreach-call.i.cil.c -o $dir/ntdrivers/floppy_false-unreach-call.i.cil.c.ll
features_static_analysis/extractFeatures $dir/ntdrivers/floppy_false-unreach-call.i.cil.c.ll
clang $wFlags -S -emit-llvm $dir/ntdrivers/kbfiltr_false-unreach-call.i.cil.c -o $dir/ntdrivers/kbfiltr_false-unreach-call.i.cil.c.ll
features_static_analysis/extractFeatures $dir/ntdrivers/kbfiltr_false-unreach-call.i.cil.c.ll
clang $wFlags -S -emit-llvm $dir/ntdrivers/parport_true-unreach-call.i.cil.c -o $dir/ntdrivers/parport_true-unreach-call.i.cil.c.ll
features_static_analysis/extractFeatures $dir/ntdrivers/parport_true-unreach-call.i.cil.c.ll
clang $wFlags -S -emit-llvm $dir/pthread-atomic/dekker_true-unreach-call.i -o $dir/pthread-atomic/dekker_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-atomic/dekker_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-atomic/qrcu_false-unreach-call.i -o $dir/pthread-atomic/qrcu_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-atomic/qrcu_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-atomic/read_write_lock_false-unreach-call.i -o $dir/pthread-atomic/read_write_lock_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-atomic/read_write_lock_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-atomic/szymanski_true-unreach-call.i -o $dir/pthread-atomic/szymanski_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-atomic/szymanski_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-complex/bounded_buffer_false-unreach-call.i -o $dir/pthread-complex/bounded_buffer_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-complex/bounded_buffer_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-complex/safestack_relacy_false-unreach-call.i -o $dir/pthread-complex/safestack_relacy_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-complex/safestack_relacy_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-driver-races/char_generic_nvram_nvram_llseek_nvram_unlocked_ioctl_true-unreach-call.i -o $dir/pthread-driver-races/char_generic_nvram_nvram_llseek_nvram_unlocked_ioctl_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-driver-races/char_generic_nvram_nvram_llseek_nvram_unlocked_ioctl_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-driver-races/char_generic_nvram_nvram_llseek_write_nvram_true-unreach-call.i -o $dir/pthread-driver-races/char_generic_nvram_nvram_llseek_write_nvram_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-driver-races/char_generic_nvram_nvram_llseek_write_nvram_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-driver-races/char_generic_nvram_read_nvram_nvram_unlocked_ioctl_true-unreach-call.i -o $dir/pthread-driver-races/char_generic_nvram_read_nvram_nvram_unlocked_ioctl_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-driver-races/char_generic_nvram_read_nvram_nvram_unlocked_ioctl_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-ext/08_rand_cas_true-unreach-call.i -o $dir/pthread-ext/08_rand_cas_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-ext/08_rand_cas_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-ext/13_unverif_true-unreach-call.i -o $dir/pthread-ext/13_unverif_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-ext/13_unverif_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-ext/15_dekker_true-unreach-call.i -o $dir/pthread-ext/15_dekker_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-ext/15_dekker_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-ext/17_szymanski_true-unreach-call.i -o $dir/pthread-ext/17_szymanski_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-ext/17_szymanski_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-ext/19_time_var_mutex_true-unreach-call.i -o $dir/pthread-ext/19_time_var_mutex_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-ext/19_time_var_mutex_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-ext/20_lamport_true-unreach-call.i -o $dir/pthread-ext/20_lamport_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-ext/20_lamport_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-ext/23_lu-fig2.fixed_true-unreach-call.i -o $dir/pthread-ext/23_lu-fig2.fixed_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-ext/23_lu-fig2.fixed_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-ext/25_stack_longest_false-unreach-call.i -o $dir/pthread-ext/25_stack_longest_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-ext/25_stack_longest_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-ext/29_conditionals_vs_true-unreach-call.i -o $dir/pthread-ext/29_conditionals_vs_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-ext/29_conditionals_vs_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-ext/30_Function_Pointer3_vs_true-unreach-call.i -o $dir/pthread-ext/30_Function_Pointer3_vs_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-ext/30_Function_Pointer3_vs_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-ext/32_pthread5_vs_false-unreach-call.i -o $dir/pthread-ext/32_pthread5_vs_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-ext/32_pthread5_vs_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-ext/34_double_lock_p2_vs_true-unreach-call.i -o $dir/pthread-ext/34_double_lock_p2_vs_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-ext/34_double_lock_p2_vs_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-ext/35_double_lock_p3_vs_true-unreach-call.i -o $dir/pthread-ext/35_double_lock_p3_vs_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-ext/35_double_lock_p3_vs_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-ext/36_stack_cas_p0_vs_concur_true-unreach-call.i -o $dir/pthread-ext/36_stack_cas_p0_vs_concur_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-ext/36_stack_cas_p0_vs_concur_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-ext/39_rand_lock_p0_vs_true-unreach-call.i -o $dir/pthread-ext/39_rand_lock_p0_vs_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-ext/39_rand_lock_p0_vs_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-ext/44_Solaris__space_map__sliced_true-unreach-call.i -o $dir/pthread-ext/44_Solaris__space_map__sliced_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-ext/44_Solaris__space_map__sliced_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-ext/45_monabsex1_vs_true-unreach-call.i -o $dir/pthread-ext/45_monabsex1_vs_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-ext/45_monabsex1_vs_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-ext/46_monabsex2_vs_true-unreach-call.i -o $dir/pthread-ext/46_monabsex2_vs_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-ext/46_monabsex2_vs_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-ext/48_ticket_lock_low_contention_vs_true-unreach-call.i -o $dir/pthread-ext/48_ticket_lock_low_contention_vs_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-ext/48_ticket_lock_low_contention_vs_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-lit/fkp2013_variant_true-unreach-call.i -o $dir/pthread-lit/fkp2013_variant_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-lit/fkp2013_variant_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-lit/fkp2014_true-unreach-call.i -o $dir/pthread-lit/fkp2014_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-lit/fkp2014_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix000_power.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix000_power.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix000_power.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix000_pso.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix000_pso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix000_pso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix000_rmo.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix000_rmo.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix000_rmo.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix000_rmo.opt_false-unreach-call.i -o $dir/pthread-wmm/mix000_rmo.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix000_rmo.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix001_power.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix001_power.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix001_power.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix001_power.opt_false-unreach-call.i -o $dir/pthread-wmm/mix001_power.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix001_power.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix001_tso.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix001_tso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix001_tso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix001_tso.opt_false-unreach-call.i -o $dir/pthread-wmm/mix001_tso.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix001_tso.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix002_pso.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix002_pso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix002_pso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix002_pso.opt_false-unreach-call.i -o $dir/pthread-wmm/mix002_pso.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix002_pso.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix003_power.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix003_power.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix003_power.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix003_tso.opt_false-unreach-call.i -o $dir/pthread-wmm/mix003_tso.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix003_tso.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix004_power.opt_false-unreach-call.i -o $dir/pthread-wmm/mix004_power.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix004_power.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix004_pso.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix004_pso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix004_pso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix004_rmo.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix004_rmo.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix004_rmo.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix005_rmo.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix005_rmo.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix005_rmo.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix006_power.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix006_power.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix006_power.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix006_rmo.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix006_rmo.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix006_rmo.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix007_rmo.opt_false-unreach-call.i -o $dir/pthread-wmm/mix007_rmo.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix007_rmo.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix008_rmo.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix008_rmo.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix008_rmo.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix009_pso.opt_false-unreach-call.i -o $dir/pthread-wmm/mix009_pso.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix009_pso.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix009_rmo.opt_false-unreach-call.i -o $dir/pthread-wmm/mix009_rmo.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix009_rmo.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix009_tso.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix009_tso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix009_tso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix009_tso.opt_false-unreach-call.i -o $dir/pthread-wmm/mix009_tso.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix009_tso.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix010_power.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix010_power.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix010_power.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix011_power.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix011_power.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix011_power.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix011_pso.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix011_pso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix011_pso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix011_pso.opt_false-unreach-call.i -o $dir/pthread-wmm/mix011_pso.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix011_pso.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix011_rmo.opt_false-unreach-call.i -o $dir/pthread-wmm/mix011_rmo.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix011_rmo.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix011_tso.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix011_tso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix011_tso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix012_rmo.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix012_rmo.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix012_rmo.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix012_tso.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix012_tso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix012_tso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix013_tso.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix013_tso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix013_tso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix014_power.opt_false-unreach-call.i -o $dir/pthread-wmm/mix014_power.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix014_power.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix015_tso.opt_false-unreach-call.i -o $dir/pthread-wmm/mix015_tso.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix015_tso.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix017_power.opt_false-unreach-call.i -o $dir/pthread-wmm/mix017_power.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix017_power.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix017_rmo.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix017_rmo.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix017_rmo.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix018_power.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix018_power.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix018_power.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix018_pso.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix018_pso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix018_pso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix018_rmo.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix018_rmo.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix018_rmo.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix018_tso.opt_false-unreach-call.i -o $dir/pthread-wmm/mix018_tso.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix018_tso.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix019_pso.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix019_pso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix019_pso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix019_pso.opt_false-unreach-call.i -o $dir/pthread-wmm/mix019_pso.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix019_pso.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix019_rmo.opt_false-unreach-call.i -o $dir/pthread-wmm/mix019_rmo.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix019_rmo.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix020_pso.opt_false-unreach-call.i -o $dir/pthread-wmm/mix020_pso.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix020_pso.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix021_power.opt_false-unreach-call.i -o $dir/pthread-wmm/mix021_power.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix021_power.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix021_pso.opt_false-unreach-call.i -o $dir/pthread-wmm/mix021_pso.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix021_pso.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix021_rmo.opt_false-unreach-call.i -o $dir/pthread-wmm/mix021_rmo.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix021_rmo.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix022_power.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix022_power.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix022_power.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix022_power.opt_false-unreach-call.i -o $dir/pthread-wmm/mix022_power.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix022_power.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix022_rmo.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix022_rmo.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix022_rmo.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix023_power.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix023_power.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix023_power.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix023_rmo.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix023_rmo.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix023_rmo.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix023_tso.opt_false-unreach-call.i -o $dir/pthread-wmm/mix023_tso.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix023_tso.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix024_power.opt_false-unreach-call.i -o $dir/pthread-wmm/mix024_power.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix024_power.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix024_pso.opt_false-unreach-call.i -o $dir/pthread-wmm/mix024_pso.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix024_pso.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix024_rmo.opt_false-unreach-call.i -o $dir/pthread-wmm/mix024_rmo.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix024_rmo.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix024_tso.opt_false-unreach-call.i -o $dir/pthread-wmm/mix024_tso.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix024_tso.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix025_rmo.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix025_rmo.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix025_rmo.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix025_tso.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix025_tso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix025_tso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix026_rmo.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix026_rmo.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix026_rmo.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix026_rmo.opt_false-unreach-call.i -o $dir/pthread-wmm/mix026_rmo.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix026_rmo.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix027_power.opt_false-unreach-call.i -o $dir/pthread-wmm/mix027_power.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix027_power.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix027_pso.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix027_pso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix027_pso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix027_tso.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix027_tso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix027_tso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix027_tso.opt_false-unreach-call.i -o $dir/pthread-wmm/mix027_tso.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix027_tso.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix029_power.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix029_power.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix029_power.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix029_power.opt_false-unreach-call.i -o $dir/pthread-wmm/mix029_power.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix029_power.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix029_pso.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix029_pso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix029_pso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix029_rmo.opt_false-unreach-call.i -o $dir/pthread-wmm/mix029_rmo.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix029_rmo.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix030_pso.opt_false-unreach-call.i -o $dir/pthread-wmm/mix030_pso.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix030_pso.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix030_tso.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix030_tso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix030_tso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix031_pso.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix031_pso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix031_pso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix031_rmo.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix031_rmo.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix031_rmo.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix031_tso.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix031_tso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix031_tso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix032_power.opt_false-unreach-call.i -o $dir/pthread-wmm/mix032_power.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix032_power.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix032_pso.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix032_pso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix032_pso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix032_pso.opt_false-unreach-call.i -o $dir/pthread-wmm/mix032_pso.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix032_pso.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix032_rmo.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix032_rmo.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix032_rmo.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix032_tso.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix032_tso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix032_tso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix033_power.opt_false-unreach-call.i -o $dir/pthread-wmm/mix033_power.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix033_power.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix033_pso.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix033_pso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix033_pso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix033_pso.opt_false-unreach-call.i -o $dir/pthread-wmm/mix033_pso.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix033_pso.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix034_pso.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix034_pso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix034_pso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix034_pso.opt_false-unreach-call.i -o $dir/pthread-wmm/mix034_pso.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix034_pso.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix034_tso.opt_false-unreach-call.i -o $dir/pthread-wmm/mix034_tso.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix034_tso.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix035_power.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix035_power.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix035_power.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix035_rmo.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix035_rmo.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix035_rmo.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix036_power.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix036_power.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix036_power.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix036_pso.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix036_pso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix036_pso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix036_rmo.opt_false-unreach-call.i -o $dir/pthread-wmm/mix036_rmo.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix036_rmo.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix037_power.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix037_power.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix037_power.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix037_pso.opt_false-unreach-call.i -o $dir/pthread-wmm/mix037_pso.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix037_pso.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix037_rmo.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix037_rmo.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix037_rmo.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix038_power.opt_false-unreach-call.i -o $dir/pthread-wmm/mix038_power.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix038_power.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix038_rmo.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix038_rmo.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix038_rmo.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix039_pso.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix039_pso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix039_pso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix039_rmo.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix039_rmo.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix039_rmo.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix039_tso.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix039_tso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix039_tso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix040_rmo.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix040_rmo.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix040_rmo.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix040_tso.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix040_tso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix040_tso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix040_tso.opt_false-unreach-call.i -o $dir/pthread-wmm/mix040_tso.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix040_tso.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix041_rmo.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix041_rmo.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix041_rmo.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix042_rmo.opt_false-unreach-call.i -o $dir/pthread-wmm/mix042_rmo.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix042_rmo.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix043_power.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix043_power.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix043_power.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix043_pso.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix043_pso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix043_pso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix043_pso.opt_false-unreach-call.i -o $dir/pthread-wmm/mix043_pso.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix043_pso.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix043_rmo.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix043_rmo.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix043_rmo.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix043_rmo.opt_false-unreach-call.i -o $dir/pthread-wmm/mix043_rmo.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix043_rmo.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix043_tso.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix043_tso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix043_tso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix044_power.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix044_power.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix044_power.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix044_pso.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix044_pso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix044_pso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix044_pso.opt_false-unreach-call.i -o $dir/pthread-wmm/mix044_pso.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix044_pso.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix044_tso.opt_false-unreach-call.i -o $dir/pthread-wmm/mix044_tso.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix044_tso.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix045_power.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix045_power.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix045_power.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix045_pso.opt_false-unreach-call.i -o $dir/pthread-wmm/mix045_pso.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix045_pso.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix047_pso.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix047_pso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix047_pso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix047_rmo.opt_false-unreach-call.i -o $dir/pthread-wmm/mix047_rmo.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix047_rmo.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix047_tso.opt_false-unreach-call.i -o $dir/pthread-wmm/mix047_tso.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix047_tso.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix048_pso.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix048_pso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix048_pso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix049_power.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix049_power.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix049_power.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix049_pso.opt_false-unreach-call.i -o $dir/pthread-wmm/mix049_pso.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix049_pso.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix049_rmo.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix049_rmo.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix049_rmo.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix050_power.opt_false-unreach-call.i -o $dir/pthread-wmm/mix050_power.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix050_power.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix050_pso.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix050_pso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix050_pso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix050_rmo.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix050_rmo.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix050_rmo.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix051_pso.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix051_pso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix051_pso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix051_rmo.opt_false-unreach-call.i -o $dir/pthread-wmm/mix051_rmo.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix051_rmo.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix052_power.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix052_power.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix052_power.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix052_power.opt_false-unreach-call.i -o $dir/pthread-wmm/mix052_power.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix052_power.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix052_pso.opt_false-unreach-call.i -o $dir/pthread-wmm/mix052_pso.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix052_pso.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix052_tso.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix052_tso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix052_tso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix053_power.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix053_power.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix053_power.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix053_rmo.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix053_rmo.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix053_rmo.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix054_power.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix054_power.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix054_power.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix054_power.opt_false-unreach-call.i -o $dir/pthread-wmm/mix054_power.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix054_power.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix054_rmo.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix054_rmo.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix054_rmo.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix054_rmo.opt_false-unreach-call.i -o $dir/pthread-wmm/mix054_rmo.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix054_rmo.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix054_tso.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix054_tso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix054_tso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix054_tso.opt_false-unreach-call.i -o $dir/pthread-wmm/mix054_tso.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix054_tso.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix055_power.opt_false-unreach-call.i -o $dir/pthread-wmm/mix055_power.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix055_power.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix055_pso.opt_false-unreach-call.i -o $dir/pthread-wmm/mix055_pso.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix055_pso.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix056_power.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix056_power.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix056_power.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix056_power.opt_false-unreach-call.i -o $dir/pthread-wmm/mix056_power.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix056_power.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix056_pso.opt_false-unreach-call.i -o $dir/pthread-wmm/mix056_pso.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix056_pso.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix056_rmo.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix056_rmo.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix056_rmo.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix056_rmo.opt_false-unreach-call.i -o $dir/pthread-wmm/mix056_rmo.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix056_rmo.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix057_power.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix057_power.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix057_power.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix057_pso.oepc_false-unreach-call.i -o $dir/pthread-wmm/mix057_pso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix057_pso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix057_pso.opt_false-unreach-call.i -o $dir/pthread-wmm/mix057_pso.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix057_pso.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/mix057_tso.opt_false-unreach-call.i -o $dir/pthread-wmm/mix057_tso.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/mix057_tso.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/podwr001_power.oepc_false-unreach-call.i -o $dir/pthread-wmm/podwr001_power.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/podwr001_power.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/podwr001_pso.opt_false-unreach-call.i -o $dir/pthread-wmm/podwr001_pso.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/podwr001_pso.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/podwr001_tso.oepc_false-unreach-call.i -o $dir/pthread-wmm/podwr001_tso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/podwr001_tso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/podwr001_tso.opt_false-unreach-call.i -o $dir/pthread-wmm/podwr001_tso.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/podwr001_tso.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/rfi000_rmo.oepc_false-unreach-call.i -o $dir/pthread-wmm/rfi000_rmo.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/rfi000_rmo.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/rfi000_tso.opt_true-unreach-call.i -o $dir/pthread-wmm/rfi000_tso.opt_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/rfi000_tso.opt_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/rfi001_power.oepc_false-unreach-call.i -o $dir/pthread-wmm/rfi001_power.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/rfi001_power.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/rfi001_tso.oepc_false-unreach-call.i -o $dir/pthread-wmm/rfi001_tso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/rfi001_tso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/rfi001_tso.opt_false-unreach-call.i -o $dir/pthread-wmm/rfi001_tso.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/rfi001_tso.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/rfi002_power.oepc_true-unreach-call.i -o $dir/pthread-wmm/rfi002_power.oepc_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/rfi002_power.oepc_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/rfi002_power.opt_true-unreach-call.i -o $dir/pthread-wmm/rfi002_power.opt_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/rfi002_power.opt_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/rfi002_pso.oepc_true-unreach-call.i -o $dir/pthread-wmm/rfi002_pso.oepc_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/rfi002_pso.oepc_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/rfi002_rmo.opt_true-unreach-call.i -o $dir/pthread-wmm/rfi002_rmo.opt_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/rfi002_rmo.opt_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/rfi003_power.oepc_false-unreach-call.i -o $dir/pthread-wmm/rfi003_power.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/rfi003_power.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/rfi003_power.opt_false-unreach-call.i -o $dir/pthread-wmm/rfi003_power.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/rfi003_power.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/rfi003_pso.oepc_false-unreach-call.i -o $dir/pthread-wmm/rfi003_pso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/rfi003_pso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/rfi003_pso.opt_false-unreach-call.i -o $dir/pthread-wmm/rfi003_pso.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/rfi003_pso.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/rfi004_power.opt_false-unreach-call.i -o $dir/pthread-wmm/rfi004_power.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/rfi004_power.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/rfi004_rmo.oepc_false-unreach-call.i -o $dir/pthread-wmm/rfi004_rmo.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/rfi004_rmo.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/rfi005_power.opt_false-unreach-call.i -o $dir/pthread-wmm/rfi005_power.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/rfi005_power.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/rfi005_tso.oepc_false-unreach-call.i -o $dir/pthread-wmm/rfi005_tso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/rfi005_tso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/rfi006_tso.oepc_false-unreach-call.i -o $dir/pthread-wmm/rfi006_tso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/rfi006_tso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/rfi007_power.oepc_false-unreach-call.i -o $dir/pthread-wmm/rfi007_power.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/rfi007_power.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/rfi008_power.opt_false-unreach-call.i -o $dir/pthread-wmm/rfi008_power.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/rfi008_power.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/rfi008_pso.oepc_false-unreach-call.i -o $dir/pthread-wmm/rfi008_pso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/rfi008_pso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/rfi008_rmo.oepc_false-unreach-call.i -o $dir/pthread-wmm/rfi008_rmo.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/rfi008_rmo.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/rfi009_power.oepc_false-unreach-call.i -o $dir/pthread-wmm/rfi009_power.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/rfi009_power.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/rfi009_power.opt_false-unreach-call.i -o $dir/pthread-wmm/rfi009_power.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/rfi009_power.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/rfi009_pso.oepc_false-unreach-call.i -o $dir/pthread-wmm/rfi009_pso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/rfi009_pso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/rfi009_pso.opt_false-unreach-call.i -o $dir/pthread-wmm/rfi009_pso.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/rfi009_pso.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/rfi009_rmo.oepc_false-unreach-call.i -o $dir/pthread-wmm/rfi009_rmo.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/rfi009_rmo.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/rfi010_power.opt_false-unreach-call.i -o $dir/pthread-wmm/rfi010_power.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/rfi010_power.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/rfi010_tso.oepc_false-unreach-call.i -o $dir/pthread-wmm/rfi010_tso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/rfi010_tso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe000_power.opt_false-unreach-call.i -o $dir/pthread-wmm/safe000_power.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe000_power.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe000_pso.oepc_true-unreach-call.i -o $dir/pthread-wmm/safe000_pso.oepc_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe000_pso.oepc_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe001_power.opt_false-unreach-call.i -o $dir/pthread-wmm/safe001_power.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe001_power.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe001_pso.oepc_false-unreach-call.i -o $dir/pthread-wmm/safe001_pso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe001_pso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe001_pso.opt_false-unreach-call.i -o $dir/pthread-wmm/safe001_pso.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe001_pso.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe001_tso.oepc_true-unreach-call.i -o $dir/pthread-wmm/safe001_tso.oepc_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe001_tso.oepc_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe001_tso.opt_true-unreach-call.i -o $dir/pthread-wmm/safe001_tso.opt_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe001_tso.opt_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe002_power.opt_false-unreach-call.i -o $dir/pthread-wmm/safe002_power.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe002_power.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe002_pso.oepc_false-unreach-call.i -o $dir/pthread-wmm/safe002_pso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe002_pso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe002_rmo.opt_false-unreach-call.i -o $dir/pthread-wmm/safe002_rmo.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe002_rmo.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe003_pso.oepc_false-unreach-call.i -o $dir/pthread-wmm/safe003_pso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe003_pso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe003_rmo.oepc_false-unreach-call.i -o $dir/pthread-wmm/safe003_rmo.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe003_rmo.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe003_rmo.opt_false-unreach-call.i -o $dir/pthread-wmm/safe003_rmo.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe003_rmo.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe003_tso.oepc_true-unreach-call.i -o $dir/pthread-wmm/safe003_tso.oepc_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe003_tso.oepc_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe003_tso.opt_true-unreach-call.i -o $dir/pthread-wmm/safe003_tso.opt_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe003_tso.opt_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe004_power.oepc_false-unreach-call.i -o $dir/pthread-wmm/safe004_power.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe004_power.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe004_rmo.opt_false-unreach-call.i -o $dir/pthread-wmm/safe004_rmo.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe004_rmo.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe005_pso.opt_false-unreach-call.i -o $dir/pthread-wmm/safe005_pso.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe005_pso.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe005_rmo.oepc_false-unreach-call.i -o $dir/pthread-wmm/safe005_rmo.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe005_rmo.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe006_pso.opt_true-unreach-call.i -o $dir/pthread-wmm/safe006_pso.opt_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe006_pso.opt_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe007_rmo.oepc_false-unreach-call.i -o $dir/pthread-wmm/safe007_rmo.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe007_rmo.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe007_tso.opt_true-unreach-call.i -o $dir/pthread-wmm/safe007_tso.opt_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe007_tso.opt_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe008_power.oepc_false-unreach-call.i -o $dir/pthread-wmm/safe008_power.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe008_power.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe008_pso.opt_false-unreach-call.i -o $dir/pthread-wmm/safe008_pso.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe008_pso.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe008_rmo.opt_false-unreach-call.i -o $dir/pthread-wmm/safe008_rmo.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe008_rmo.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe008_tso.oepc_true-unreach-call.i -o $dir/pthread-wmm/safe008_tso.oepc_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe008_tso.oepc_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe009_power.opt_false-unreach-call.i -o $dir/pthread-wmm/safe009_power.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe009_power.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe009_rmo.oepc_false-unreach-call.i -o $dir/pthread-wmm/safe009_rmo.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe009_rmo.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe009_tso.opt_true-unreach-call.i -o $dir/pthread-wmm/safe009_tso.opt_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe009_tso.opt_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe010_power.opt_false-unreach-call.i -o $dir/pthread-wmm/safe010_power.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe010_power.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe010_tso.opt_true-unreach-call.i -o $dir/pthread-wmm/safe010_tso.opt_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe010_tso.opt_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe011_pso.opt_false-unreach-call.i -o $dir/pthread-wmm/safe011_pso.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe011_pso.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe011_rmo.oepc_false-unreach-call.i -o $dir/pthread-wmm/safe011_rmo.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe011_rmo.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe012_rmo.opt_false-unreach-call.i -o $dir/pthread-wmm/safe012_rmo.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe012_rmo.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe012_tso.opt_true-unreach-call.i -o $dir/pthread-wmm/safe012_tso.opt_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe012_tso.opt_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe013_power.opt_false-unreach-call.i -o $dir/pthread-wmm/safe013_power.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe013_power.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe013_tso.opt_true-unreach-call.i -o $dir/pthread-wmm/safe013_tso.opt_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe013_tso.opt_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe014_power.opt_false-unreach-call.i -o $dir/pthread-wmm/safe014_power.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe014_power.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe014_rmo.oepc_false-unreach-call.i -o $dir/pthread-wmm/safe014_rmo.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe014_rmo.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe014_rmo.opt_false-unreach-call.i -o $dir/pthread-wmm/safe014_rmo.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe014_rmo.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe015_pso.opt_true-unreach-call.i -o $dir/pthread-wmm/safe015_pso.opt_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe015_pso.opt_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe015_tso.oepc_true-unreach-call.i -o $dir/pthread-wmm/safe015_tso.oepc_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe015_tso.oepc_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe016_power.oepc_false-unreach-call.i -o $dir/pthread-wmm/safe016_power.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe016_power.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe016_power.opt_false-unreach-call.i -o $dir/pthread-wmm/safe016_power.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe016_power.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe016_pso.opt_true-unreach-call.i -o $dir/pthread-wmm/safe016_pso.opt_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe016_pso.opt_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe016_tso.oepc_true-unreach-call.i -o $dir/pthread-wmm/safe016_tso.oepc_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe016_tso.oepc_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe017_pso.oepc_false-unreach-call.i -o $dir/pthread-wmm/safe017_pso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe017_pso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe017_rmo.oepc_false-unreach-call.i -o $dir/pthread-wmm/safe017_rmo.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe017_rmo.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe017_rmo.opt_false-unreach-call.i -o $dir/pthread-wmm/safe017_rmo.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe017_rmo.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe018_rmo.opt_false-unreach-call.i -o $dir/pthread-wmm/safe018_rmo.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe018_rmo.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe018_tso.oepc_true-unreach-call.i -o $dir/pthread-wmm/safe018_tso.oepc_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe018_tso.oepc_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe019_power.oepc_false-unreach-call.i -o $dir/pthread-wmm/safe019_power.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe019_power.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe019_power.opt_false-unreach-call.i -o $dir/pthread-wmm/safe019_power.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe019_power.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe019_rmo.opt_false-unreach-call.i -o $dir/pthread-wmm/safe019_rmo.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe019_rmo.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe021_power.oepc_false-unreach-call.i -o $dir/pthread-wmm/safe021_power.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe021_power.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe021_power.opt_false-unreach-call.i -o $dir/pthread-wmm/safe021_power.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe021_power.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe021_pso.opt_false-unreach-call.i -o $dir/pthread-wmm/safe021_pso.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe021_pso.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe021_rmo.oepc_false-unreach-call.i -o $dir/pthread-wmm/safe021_rmo.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe021_rmo.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe021_rmo.opt_false-unreach-call.i -o $dir/pthread-wmm/safe021_rmo.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe021_rmo.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe022_power.oepc_false-unreach-call.i -o $dir/pthread-wmm/safe022_power.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe022_power.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe022_power.opt_false-unreach-call.i -o $dir/pthread-wmm/safe022_power.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe022_power.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe022_pso.opt_false-unreach-call.i -o $dir/pthread-wmm/safe022_pso.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe022_pso.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe023_rmo.opt_false-unreach-call.i -o $dir/pthread-wmm/safe023_rmo.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe023_rmo.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe024_power.oepc_false-unreach-call.i -o $dir/pthread-wmm/safe024_power.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe024_power.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe024_pso.oepc_false-unreach-call.i -o $dir/pthread-wmm/safe024_pso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe024_pso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe025_pso.opt_true-unreach-call.i -o $dir/pthread-wmm/safe025_pso.opt_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe025_pso.opt_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe025_rmo.oepc_false-unreach-call.i -o $dir/pthread-wmm/safe025_rmo.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe025_rmo.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe026_tso.opt_true-unreach-call.i -o $dir/pthread-wmm/safe026_tso.opt_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe026_tso.opt_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe027_rmo.oepc_false-unreach-call.i -o $dir/pthread-wmm/safe027_rmo.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe027_rmo.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe027_tso.opt_true-unreach-call.i -o $dir/pthread-wmm/safe027_tso.opt_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe027_tso.opt_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe028_power.opt_false-unreach-call.i -o $dir/pthread-wmm/safe028_power.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe028_power.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe028_rmo.oepc_false-unreach-call.i -o $dir/pthread-wmm/safe028_rmo.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe028_rmo.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe028_rmo.opt_false-unreach-call.i -o $dir/pthread-wmm/safe028_rmo.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe028_rmo.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe029_power.opt_false-unreach-call.i -o $dir/pthread-wmm/safe029_power.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe029_power.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe029_pso.oepc_false-unreach-call.i -o $dir/pthread-wmm/safe029_pso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe029_pso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe030_power.opt_false-unreach-call.i -o $dir/pthread-wmm/safe030_power.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe030_power.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe030_rmo.opt_false-unreach-call.i -o $dir/pthread-wmm/safe030_rmo.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe030_rmo.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe030_tso.opt_true-unreach-call.i -o $dir/pthread-wmm/safe030_tso.opt_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe030_tso.opt_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe031_power.oepc_false-unreach-call.i -o $dir/pthread-wmm/safe031_power.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe031_power.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe031_pso.oepc_false-unreach-call.i -o $dir/pthread-wmm/safe031_pso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe031_pso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe031_rmo.oepc_false-unreach-call.i -o $dir/pthread-wmm/safe031_rmo.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe031_rmo.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe031_rmo.opt_false-unreach-call.i -o $dir/pthread-wmm/safe031_rmo.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe031_rmo.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe032_power.oepc_false-unreach-call.i -o $dir/pthread-wmm/safe032_power.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe032_power.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe032_power.opt_false-unreach-call.i -o $dir/pthread-wmm/safe032_power.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe032_power.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe032_pso.oepc_false-unreach-call.i -o $dir/pthread-wmm/safe032_pso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe032_pso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe032_rmo.oepc_false-unreach-call.i -o $dir/pthread-wmm/safe032_rmo.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe032_rmo.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe032_rmo.opt_false-unreach-call.i -o $dir/pthread-wmm/safe032_rmo.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe032_rmo.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe033_pso.oepc_false-unreach-call.i -o $dir/pthread-wmm/safe033_pso.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe033_pso.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe033_tso.opt_true-unreach-call.i -o $dir/pthread-wmm/safe033_tso.opt_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe033_tso.opt_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe034_power.opt_false-unreach-call.i -o $dir/pthread-wmm/safe034_power.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe034_power.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe035_pso.opt_true-unreach-call.i -o $dir/pthread-wmm/safe035_pso.opt_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe035_pso.opt_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe036_pso.oepc_true-unreach-call.i -o $dir/pthread-wmm/safe036_pso.oepc_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe036_pso.oepc_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe036_rmo.opt_true-unreach-call.i -o $dir/pthread-wmm/safe036_rmo.opt_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe036_rmo.opt_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe036_tso.opt_true-unreach-call.i -o $dir/pthread-wmm/safe036_tso.opt_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe036_tso.opt_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe037_power.oepc_true-unreach-call.i -o $dir/pthread-wmm/safe037_power.oepc_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe037_power.oepc_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe037_rmo.opt_true-unreach-call.i -o $dir/pthread-wmm/safe037_rmo.opt_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe037_rmo.opt_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/safe037_tso.opt_true-unreach-call.i -o $dir/pthread-wmm/safe037_tso.opt_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/safe037_tso.opt_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/thin000_power.opt_false-unreach-call.i -o $dir/pthread-wmm/thin000_power.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/thin000_power.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/thin000_pso.oepc_true-unreach-call.i -o $dir/pthread-wmm/thin000_pso.oepc_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/thin000_pso.oepc_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/thin000_rmo.oepc_false-unreach-call.i -o $dir/pthread-wmm/thin000_rmo.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/thin000_rmo.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/thin000_tso.opt_true-unreach-call.i -o $dir/pthread-wmm/thin000_tso.opt_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/thin000_tso.opt_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/thin001_power.opt_false-unreach-call.i -o $dir/pthread-wmm/thin001_power.opt_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/thin001_power.opt_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/thin001_pso.oepc_true-unreach-call.i -o $dir/pthread-wmm/thin001_pso.oepc_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/thin001_pso.oepc_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/thin001_tso.oepc_true-unreach-call.i -o $dir/pthread-wmm/thin001_tso.oepc_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/thin001_tso.oepc_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/thin002_power.oepc_false-unreach-call.i -o $dir/pthread-wmm/thin002_power.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/thin002_power.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/thin002_rmo.oepc_false-unreach-call.i -o $dir/pthread-wmm/thin002_rmo.oepc_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/thin002_rmo.oepc_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread-wmm/thin002_tso.opt_true-unreach-call.i -o $dir/pthread-wmm/thin002_tso.opt_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread-wmm/thin002_tso.opt_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread/bigshot_p_false-unreach-call.i -o $dir/pthread/bigshot_p_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread/bigshot_p_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread/bigshot_s2_true-unreach-call.i -o $dir/pthread/bigshot_s2_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread/bigshot_s2_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread/fib_bench_longest_true-unreach-call.i -o $dir/pthread/fib_bench_longest_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread/fib_bench_longest_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread/indexer_true-unreach-call.i -o $dir/pthread/indexer_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread/indexer_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread/queue_ok_true-unreach-call.i -o $dir/pthread/queue_ok_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread/queue_ok_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread/reorder_2_false-unreach-call.i -o $dir/pthread/reorder_2_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread/reorder_2_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread/reorder_5_false-unreach-call.i -o $dir/pthread/reorder_5_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread/reorder_5_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread/sync01_true-unreach-call.i -o $dir/pthread/sync01_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread/sync01_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/pthread/twostage_3_false-unreach-call.i -o $dir/pthread/twostage_3_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/pthread/twostage_3_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/recursive-simple/fibo_15_false-unreach-call.c -o $dir/recursive-simple/fibo_15_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/recursive-simple/fibo_15_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/recursive-simple/fibo_15_true-unreach-call.c -o $dir/recursive-simple/fibo_15_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/recursive-simple/fibo_15_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/recursive-simple/fibo_20_true-unreach-call.c -o $dir/recursive-simple/fibo_20_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/recursive-simple/fibo_20_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/recursive-simple/fibo_25_false-unreach-call.c -o $dir/recursive-simple/fibo_25_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/recursive-simple/fibo_25_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/recursive-simple/fibo_25_true-unreach-call.c -o $dir/recursive-simple/fibo_25_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/recursive-simple/fibo_25_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/recursive-simple/fibo_2calls_10_true-unreach-call.c -o $dir/recursive-simple/fibo_2calls_10_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/recursive-simple/fibo_2calls_10_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/recursive-simple/fibo_2calls_15_true-unreach-call.c -o $dir/recursive-simple/fibo_2calls_15_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/recursive-simple/fibo_2calls_15_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/recursive-simple/fibo_2calls_20_false-unreach-call.c -o $dir/recursive-simple/fibo_2calls_20_false-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/recursive-simple/fibo_2calls_20_false-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/recursive-simple/fibo_2calls_20_true-unreach-call.c -o $dir/recursive-simple/fibo_2calls_20_true-unreach-call.c.ll
features_static_analysis/extractFeatures $dir/recursive-simple/fibo_2calls_20_true-unreach-call.c.ll
clang $wFlags -S -emit-llvm $dir/recursive-simple/id_b3_o5_false-no-overflow.c -o $dir/recursive-simple/id_b3_o5_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/recursive-simple/id_b3_o5_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/recursive-simple/sum_non_eq_false-no-overflow.c -o $dir/recursive-simple/sum_non_eq_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/recursive-simple/sum_non_eq_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/recursive-simple/sum_non_false-no-overflow.c -o $dir/recursive-simple/sum_non_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/recursive-simple/sum_non_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/recursive/Addition03_false-no-overflow.c -o $dir/recursive/Addition03_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/recursive/Addition03_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/reducercommutativity/avg60_true-unreach-call.i -o $dir/reducercommutativity/avg60_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/reducercommutativity/avg60_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/reducercommutativity/rangesum20_false-unreach-call.i -o $dir/reducercommutativity/rangesum20_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/reducercommutativity/rangesum20_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/reducercommutativity/rangesum40_false-unreach-call.i -o $dir/reducercommutativity/rangesum40_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/reducercommutativity/rangesum40_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/reducercommutativity/sep10_true-unreach-call.i -o $dir/reducercommutativity/sep10_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/reducercommutativity/sep10_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/reducercommutativity/sep40_true-unreach-call.i -o $dir/reducercommutativity/sep40_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/reducercommutativity/sep40_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/reducercommutativity/sep60_true-unreach-call.i -o $dir/reducercommutativity/sep60_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/reducercommutativity/sep60_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/reducercommutativity/sum60_true-unreach-call.i -o $dir/reducercommutativity/sum60_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/reducercommutativity/sum60_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/seq-mthreaded/pals_floodmax.3_overflow_false-unreach-call.ufo.UNBOUNDED.pals.c -o $dir/seq-mthreaded/pals_floodmax.3_overflow_false-unreach-call.ufo.UNBOUNDED.pals.c.ll
features_static_analysis/extractFeatures $dir/seq-mthreaded/pals_floodmax.3_overflow_false-unreach-call.ufo.UNBOUNDED.pals.c.ll
clang $wFlags -S -emit-llvm $dir/seq-mthreaded/pals_floodmax.4_false-unreach-call.1.ufo.UNBOUNDED.pals.c -o $dir/seq-mthreaded/pals_floodmax.4_false-unreach-call.1.ufo.UNBOUNDED.pals.c.ll
features_static_analysis/extractFeatures $dir/seq-mthreaded/pals_floodmax.4_false-unreach-call.1.ufo.UNBOUNDED.pals.c.ll
clang $wFlags -S -emit-llvm $dir/seq-mthreaded/pals_floodmax.4_false-unreach-call.2.ufo.UNBOUNDED.pals.c -o $dir/seq-mthreaded/pals_floodmax.4_false-unreach-call.2.ufo.UNBOUNDED.pals.c.ll
features_static_analysis/extractFeatures $dir/seq-mthreaded/pals_floodmax.4_false-unreach-call.2.ufo.UNBOUNDED.pals.c.ll
clang $wFlags -S -emit-llvm $dir/seq-mthreaded/pals_floodmax.5_false-unreach-call.2.ufo.UNBOUNDED.pals.c -o $dir/seq-mthreaded/pals_floodmax.5_false-unreach-call.2.ufo.UNBOUNDED.pals.c.ll
features_static_analysis/extractFeatures $dir/seq-mthreaded/pals_floodmax.5_false-unreach-call.2.ufo.UNBOUNDED.pals.c.ll
clang $wFlags -S -emit-llvm $dir/seq-mthreaded/pals_floodmax.5_false-unreach-call.3.ufo.UNBOUNDED.pals.c -o $dir/seq-mthreaded/pals_floodmax.5_false-unreach-call.3.ufo.UNBOUNDED.pals.c.ll
features_static_analysis/extractFeatures $dir/seq-mthreaded/pals_floodmax.5_false-unreach-call.3.ufo.UNBOUNDED.pals.c.ll
clang $wFlags -S -emit-llvm $dir/seq-mthreaded/pals_floodmax.5_overflow_false-unreach-call.ufo.UNBOUNDED.pals.c -o $dir/seq-mthreaded/pals_floodmax.5_overflow_false-unreach-call.ufo.UNBOUNDED.pals.c.ll
features_static_analysis/extractFeatures $dir/seq-mthreaded/pals_floodmax.5_overflow_false-unreach-call.ufo.UNBOUNDED.pals.c.ll
clang $wFlags -S -emit-llvm $dir/seq-mthreaded/pals_lcr-var-start-time.3_false-unreach-call.2.ufo.UNBOUNDED.pals.c -o $dir/seq-mthreaded/pals_lcr-var-start-time.3_false-unreach-call.2.ufo.UNBOUNDED.pals.c.ll
features_static_analysis/extractFeatures $dir/seq-mthreaded/pals_lcr-var-start-time.3_false-unreach-call.2.ufo.UNBOUNDED.pals.c.ll
clang $wFlags -S -emit-llvm $dir/seq-mthreaded/pals_lcr-var-start-time.4_false-unreach-call.1.ufo.UNBOUNDED.pals.c -o $dir/seq-mthreaded/pals_lcr-var-start-time.4_false-unreach-call.1.ufo.UNBOUNDED.pals.c.ll
features_static_analysis/extractFeatures $dir/seq-mthreaded/pals_lcr-var-start-time.4_false-unreach-call.1.ufo.UNBOUNDED.pals.c.ll
clang $wFlags -S -emit-llvm $dir/seq-mthreaded/pals_lcr-var-start-time.4_false-unreach-call.2.ufo.UNBOUNDED.pals.c -o $dir/seq-mthreaded/pals_lcr-var-start-time.4_false-unreach-call.2.ufo.UNBOUNDED.pals.c.ll
features_static_analysis/extractFeatures $dir/seq-mthreaded/pals_lcr-var-start-time.4_false-unreach-call.2.ufo.UNBOUNDED.pals.c.ll
clang $wFlags -S -emit-llvm $dir/seq-mthreaded/pals_lcr-var-start-time.6_false-unreach-call.2.ufo.UNBOUNDED.pals.c -o $dir/seq-mthreaded/pals_lcr-var-start-time.6_false-unreach-call.2.ufo.UNBOUNDED.pals.c.ll
features_static_analysis/extractFeatures $dir/seq-mthreaded/pals_lcr-var-start-time.6_false-unreach-call.2.ufo.UNBOUNDED.pals.c.ll
clang $wFlags -S -emit-llvm $dir/seq-mthreaded/pals_lcr-var-start-time.6_true-unreach-call.ufo.UNBOUNDED.pals.c -o $dir/seq-mthreaded/pals_lcr-var-start-time.6_true-unreach-call.ufo.UNBOUNDED.pals.c.ll
features_static_analysis/extractFeatures $dir/seq-mthreaded/pals_lcr-var-start-time.6_true-unreach-call.ufo.UNBOUNDED.pals.c.ll
clang $wFlags -S -emit-llvm $dir/seq-mthreaded/pals_lcr.4_overflow_false-unreach-call.ufo.UNBOUNDED.pals.c -o $dir/seq-mthreaded/pals_lcr.4_overflow_false-unreach-call.ufo.UNBOUNDED.pals.c.ll
features_static_analysis/extractFeatures $dir/seq-mthreaded/pals_lcr.4_overflow_false-unreach-call.ufo.UNBOUNDED.pals.c.ll
clang $wFlags -S -emit-llvm $dir/seq-mthreaded/pals_lcr.4_true-unreach-call.ufo.UNBOUNDED.pals.c -o $dir/seq-mthreaded/pals_lcr.4_true-unreach-call.ufo.UNBOUNDED.pals.c.ll
features_static_analysis/extractFeatures $dir/seq-mthreaded/pals_lcr.4_true-unreach-call.ufo.UNBOUNDED.pals.c.ll
clang $wFlags -S -emit-llvm $dir/seq-mthreaded/pals_lcr.5_overflow_false-unreach-call.ufo.UNBOUNDED.pals.c -o $dir/seq-mthreaded/pals_lcr.5_overflow_false-unreach-call.ufo.UNBOUNDED.pals.c.ll
features_static_analysis/extractFeatures $dir/seq-mthreaded/pals_lcr.5_overflow_false-unreach-call.ufo.UNBOUNDED.pals.c.ll
clang $wFlags -S -emit-llvm $dir/seq-mthreaded/pals_lcr.5_true-unreach-call.ufo.UNBOUNDED.pals.c -o $dir/seq-mthreaded/pals_lcr.5_true-unreach-call.ufo.UNBOUNDED.pals.c.ll
features_static_analysis/extractFeatures $dir/seq-mthreaded/pals_lcr.5_true-unreach-call.ufo.UNBOUNDED.pals.c.ll
clang $wFlags -S -emit-llvm $dir/seq-mthreaded/pals_lcr.6_true-unreach-call.ufo.UNBOUNDED.pals.c -o $dir/seq-mthreaded/pals_lcr.6_true-unreach-call.ufo.UNBOUNDED.pals.c.ll
features_static_analysis/extractFeatures $dir/seq-mthreaded/pals_lcr.6_true-unreach-call.ufo.UNBOUNDED.pals.c.ll
clang $wFlags -S -emit-llvm $dir/seq-mthreaded/pals_lcr.8_true-unreach-call.ufo.UNBOUNDED.pals.c -o $dir/seq-mthreaded/pals_lcr.8_true-unreach-call.ufo.UNBOUNDED.pals.c.ll
features_static_analysis/extractFeatures $dir/seq-mthreaded/pals_lcr.8_true-unreach-call.ufo.UNBOUNDED.pals.c.ll
clang $wFlags -S -emit-llvm $dir/seq-mthreaded/pals_opt-floodmax.3_false-unreach-call.2.ufo.UNBOUNDED.pals.c -o $dir/seq-mthreaded/pals_opt-floodmax.3_false-unreach-call.2.ufo.UNBOUNDED.pals.c.ll
features_static_analysis/extractFeatures $dir/seq-mthreaded/pals_opt-floodmax.3_false-unreach-call.2.ufo.UNBOUNDED.pals.c.ll
clang $wFlags -S -emit-llvm $dir/seq-mthreaded/pals_opt-floodmax.4_false-unreach-call.2.ufo.UNBOUNDED.pals.c -o $dir/seq-mthreaded/pals_opt-floodmax.4_false-unreach-call.2.ufo.UNBOUNDED.pals.c.ll
features_static_analysis/extractFeatures $dir/seq-mthreaded/pals_opt-floodmax.4_false-unreach-call.2.ufo.UNBOUNDED.pals.c.ll
clang $wFlags -S -emit-llvm $dir/seq-mthreaded/pals_opt-floodmax.4_true-unreach-call.ufo.UNBOUNDED.pals.c -o $dir/seq-mthreaded/pals_opt-floodmax.4_true-unreach-call.ufo.UNBOUNDED.pals.c.ll
features_static_analysis/extractFeatures $dir/seq-mthreaded/pals_opt-floodmax.4_true-unreach-call.ufo.UNBOUNDED.pals.c.ll
clang $wFlags -S -emit-llvm $dir/seq-mthreaded/pals_opt-floodmax.5_false-unreach-call.2.ufo.UNBOUNDED.pals.c -o $dir/seq-mthreaded/pals_opt-floodmax.5_false-unreach-call.2.ufo.UNBOUNDED.pals.c.ll
features_static_analysis/extractFeatures $dir/seq-mthreaded/pals_opt-floodmax.5_false-unreach-call.2.ufo.UNBOUNDED.pals.c.ll
clang $wFlags -S -emit-llvm $dir/seq-mthreaded/pals_opt-floodmax.5_true-unreach-call.ufo.UNBOUNDED.pals.c -o $dir/seq-mthreaded/pals_opt-floodmax.5_true-unreach-call.ufo.UNBOUNDED.pals.c.ll
features_static_analysis/extractFeatures $dir/seq-mthreaded/pals_opt-floodmax.5_true-unreach-call.ufo.UNBOUNDED.pals.c.ll
clang $wFlags -S -emit-llvm $dir/seq-mthreaded/pals_STARTPALS_ActiveStandby_false-unreach-call.4_2.ufo.BOUNDED-10.pals.c -o $dir/seq-mthreaded/pals_STARTPALS_ActiveStandby_false-unreach-call.4_2.ufo.BOUNDED-10.pals.c.ll
features_static_analysis/extractFeatures $dir/seq-mthreaded/pals_STARTPALS_ActiveStandby_false-unreach-call.4_2.ufo.BOUNDED-10.pals.c.ll
clang $wFlags -S -emit-llvm $dir/seq-mthreaded/pals_STARTPALS_ActiveStandby_false-unreach-call.4_2.ufo.UNBOUNDED.pals.c -o $dir/seq-mthreaded/pals_STARTPALS_ActiveStandby_false-unreach-call.4_2.ufo.UNBOUNDED.pals.c.ll
features_static_analysis/extractFeatures $dir/seq-mthreaded/pals_STARTPALS_ActiveStandby_false-unreach-call.4_2.ufo.UNBOUNDED.pals.c.ll
clang $wFlags -S -emit-llvm $dir/seq-mthreaded/pals_STARTPALS_ActiveStandby_false-unreach-call.5.ufo.BOUNDED-10.pals.c -o $dir/seq-mthreaded/pals_STARTPALS_ActiveStandby_false-unreach-call.5.ufo.BOUNDED-10.pals.c.ll
features_static_analysis/extractFeatures $dir/seq-mthreaded/pals_STARTPALS_ActiveStandby_false-unreach-call.5.ufo.BOUNDED-10.pals.c.ll
clang $wFlags -S -emit-llvm $dir/seq-mthreaded/pals_STARTPALS_ActiveStandby_true-unreach-call.ufo.BOUNDED-10.pals.c -o $dir/seq-mthreaded/pals_STARTPALS_ActiveStandby_true-unreach-call.ufo.BOUNDED-10.pals.c.ll
features_static_analysis/extractFeatures $dir/seq-mthreaded/pals_STARTPALS_ActiveStandby_true-unreach-call.ufo.BOUNDED-10.pals.c.ll
clang $wFlags -S -emit-llvm $dir/seq-mthreaded/pals_STARTPALS_ActiveStandby_true-unreach-call.ufo.UNBOUNDED.pals.c -o $dir/seq-mthreaded/pals_STARTPALS_ActiveStandby_true-unreach-call.ufo.UNBOUNDED.pals.c.ll
features_static_analysis/extractFeatures $dir/seq-mthreaded/pals_STARTPALS_ActiveStandby_true-unreach-call.ufo.UNBOUNDED.pals.c.ll
clang $wFlags -S -emit-llvm $dir/seq-mthreaded/pals_STARTPALS_Triplicated_true-unreach-call.ufo.UNBOUNDED.pals.c -o $dir/seq-mthreaded/pals_STARTPALS_Triplicated_true-unreach-call.ufo.UNBOUNDED.pals.c.ll
features_static_analysis/extractFeatures $dir/seq-mthreaded/pals_STARTPALS_Triplicated_true-unreach-call.ufo.UNBOUNDED.pals.c.ll
clang $wFlags -S -emit-llvm $dir/seq-mthreaded/rekcba_aso_false-unreach-call.1.M4.c -o $dir/seq-mthreaded/rekcba_aso_false-unreach-call.1.M4.c.ll
features_static_analysis/extractFeatures $dir/seq-mthreaded/rekcba_aso_false-unreach-call.1.M4.c.ll
clang $wFlags -S -emit-llvm $dir/seq-mthreaded/rekcba_aso_false-unreach-call.3.M4.c -o $dir/seq-mthreaded/rekcba_aso_false-unreach-call.3.M4.c.ll
features_static_analysis/extractFeatures $dir/seq-mthreaded/rekcba_aso_false-unreach-call.3.M4.c.ll
clang $wFlags -S -emit-llvm $dir/seq-mthreaded/rekcba_aso_true-unreach-call.2.M1.c -o $dir/seq-mthreaded/rekcba_aso_true-unreach-call.2.M1.c.ll
features_static_analysis/extractFeatures $dir/seq-mthreaded/rekcba_aso_true-unreach-call.2.M1.c.ll
clang $wFlags -S -emit-llvm $dir/seq-mthreaded/rekcba_aso_true-unreach-call.2.M4.c -o $dir/seq-mthreaded/rekcba_aso_true-unreach-call.2.M4.c.ll
features_static_analysis/extractFeatures $dir/seq-mthreaded/rekcba_aso_true-unreach-call.2.M4.c.ll
clang $wFlags -S -emit-llvm $dir/seq-mthreaded/rekcba_nxt_false-unreach-call.1.M4.c -o $dir/seq-mthreaded/rekcba_nxt_false-unreach-call.1.M4.c.ll
features_static_analysis/extractFeatures $dir/seq-mthreaded/rekcba_nxt_false-unreach-call.1.M4.c.ll
clang $wFlags -S -emit-llvm $dir/seq-mthreaded/rekcba_nxt_true-unreach-call.1.M1.c -o $dir/seq-mthreaded/rekcba_nxt_true-unreach-call.1.M1.c.ll
features_static_analysis/extractFeatures $dir/seq-mthreaded/rekcba_nxt_true-unreach-call.1.M1.c.ll
clang $wFlags -S -emit-llvm $dir/seq-mthreaded/rekh_aso_false-unreach-call.2.M1.c -o $dir/seq-mthreaded/rekh_aso_false-unreach-call.2.M1.c.ll
features_static_analysis/extractFeatures $dir/seq-mthreaded/rekh_aso_false-unreach-call.2.M1.c.ll
clang $wFlags -S -emit-llvm $dir/seq-mthreaded/rekh_aso_true-unreach-call.1.M1.c -o $dir/seq-mthreaded/rekh_aso_true-unreach-call.1.M1.c.ll
features_static_analysis/extractFeatures $dir/seq-mthreaded/rekh_aso_true-unreach-call.1.M1.c.ll
clang $wFlags -S -emit-llvm $dir/seq-mthreaded/rekh_aso_true-unreach-call.1.M4.c -o $dir/seq-mthreaded/rekh_aso_true-unreach-call.1.M4.c.ll
features_static_analysis/extractFeatures $dir/seq-mthreaded/rekh_aso_true-unreach-call.1.M4.c.ll
clang $wFlags -S -emit-llvm $dir/seq-mthreaded/rekh_aso_true-unreach-call.3.M1.c -o $dir/seq-mthreaded/rekh_aso_true-unreach-call.3.M1.c.ll
features_static_analysis/extractFeatures $dir/seq-mthreaded/rekh_aso_true-unreach-call.3.M1.c.ll
clang $wFlags -S -emit-llvm $dir/seq-mthreaded/rekh_nxt_false-unreach-call.2.M1.c -o $dir/seq-mthreaded/rekh_nxt_false-unreach-call.2.M1.c.ll
features_static_analysis/extractFeatures $dir/seq-mthreaded/rekh_nxt_false-unreach-call.2.M1.c.ll
clang $wFlags -S -emit-llvm $dir/seq-mthreaded/rekh_nxt_true-unreach-call.1.M4.c -o $dir/seq-mthreaded/rekh_nxt_true-unreach-call.1.M4.c.ll
features_static_analysis/extractFeatures $dir/seq-mthreaded/rekh_nxt_true-unreach-call.1.M4.c.ll
clang $wFlags -S -emit-llvm $dir/seq-pthread/cs_fib_false-unreach-call.i -o $dir/seq-pthread/cs_fib_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/seq-pthread/cs_fib_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/seq-pthread/cs_fib_longer_false-unreach-call.i -o $dir/seq-pthread/cs_fib_longer_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/seq-pthread/cs_fib_longer_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/seq-pthread/cs_fib_true-unreach-call.i -o $dir/seq-pthread/cs_fib_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/seq-pthread/cs_fib_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/seq-pthread/cs_lamport_true-unreach-call.i -o $dir/seq-pthread/cs_lamport_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/seq-pthread/cs_lamport_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/seq-pthread/cs_queue_false-unreach-call.i -o $dir/seq-pthread/cs_queue_false-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/seq-pthread/cs_queue_false-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/seq-pthread/cs_queue_true-unreach-call.i -o $dir/seq-pthread/cs_queue_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/seq-pthread/cs_queue_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/seq-pthread/cs_read_write_lock_true-unreach-call.i -o $dir/seq-pthread/cs_read_write_lock_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/seq-pthread/cs_read_write_lock_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/seq-pthread/cs_stateful_true-unreach-call.i -o $dir/seq-pthread/cs_stateful_true-unreach-call.i.ll
features_static_analysis/extractFeatures $dir/seq-pthread/cs_stateful_true-unreach-call.i.ll
clang $wFlags -S -emit-llvm $dir/signedintegeroverflow-regression/AdditionIntMin_false-no-overflow.c.i -o $dir/signedintegeroverflow-regression/AdditionIntMin_false-no-overflow.c.i.ll
features_static_analysis/extractFeatures $dir/signedintegeroverflow-regression/AdditionIntMin_false-no-overflow.c.i.ll
clang $wFlags -S -emit-llvm $dir/signedintegeroverflow-regression/PostfixDecrement_false-no-overflow.c.i -o $dir/signedintegeroverflow-regression/PostfixDecrement_false-no-overflow.c.i.ll
features_static_analysis/extractFeatures $dir/signedintegeroverflow-regression/PostfixDecrement_false-no-overflow.c.i.ll
clang $wFlags -S -emit-llvm $dir/signedintegeroverflow-regression/PrefixDecrement_false-no-overflow.c.i -o $dir/signedintegeroverflow-regression/PrefixDecrement_false-no-overflow.c.i.ll
features_static_analysis/extractFeatures $dir/signedintegeroverflow-regression/PrefixDecrement_false-no-overflow.c.i.ll
clang $wFlags -S -emit-llvm $dir/signedintegeroverflow-regression/UnaryMinus_false-no-overflow.c.i -o $dir/signedintegeroverflow-regression/UnaryMinus_false-no-overflow.c.i.ll
features_static_analysis/extractFeatures $dir/signedintegeroverflow-regression/UnaryMinus_false-no-overflow.c.i.ll
clang $wFlags -S -emit-llvm $dir/ssh/s3_clnt.blast.01_true-unreach-call.i.cil.c -o $dir/ssh/s3_clnt.blast.01_true-unreach-call.i.cil.c.ll
features_static_analysis/extractFeatures $dir/ssh/s3_clnt.blast.01_true-unreach-call.i.cil.c.ll
clang $wFlags -S -emit-llvm $dir/ssh/s3_clnt.blast.03_false-unreach-call.i.cil.c -o $dir/ssh/s3_clnt.blast.03_false-unreach-call.i.cil.c.ll
features_static_analysis/extractFeatures $dir/ssh/s3_clnt.blast.03_false-unreach-call.i.cil.c.ll
clang $wFlags -S -emit-llvm $dir/ssh/s3_clnt.blast.03_true-unreach-call.i.cil.c -o $dir/ssh/s3_clnt.blast.03_true-unreach-call.i.cil.c.ll
features_static_analysis/extractFeatures $dir/ssh/s3_clnt.blast.03_true-unreach-call.i.cil.c.ll
clang $wFlags -S -emit-llvm $dir/ssh/s3_srvr.blast.02_false-unreach-call.i.cil.c -o $dir/ssh/s3_srvr.blast.02_false-unreach-call.i.cil.c.ll
features_static_analysis/extractFeatures $dir/ssh/s3_srvr.blast.02_false-unreach-call.i.cil.c.ll
clang $wFlags -S -emit-llvm $dir/ssh/s3_srvr.blast.04_false-unreach-call.i.cil.c -o $dir/ssh/s3_srvr.blast.04_false-unreach-call.i.cil.c.ll
features_static_analysis/extractFeatures $dir/ssh/s3_srvr.blast.04_false-unreach-call.i.cil.c.ll
clang $wFlags -S -emit-llvm $dir/ssh/s3_srvr.blast.07_false-unreach-call.i.cil.c -o $dir/ssh/s3_srvr.blast.07_false-unreach-call.i.cil.c.ll
features_static_analysis/extractFeatures $dir/ssh/s3_srvr.blast.07_false-unreach-call.i.cil.c.ll
clang $wFlags -S -emit-llvm $dir/ssh/s3_srvr.blast.08_true-unreach-call.i.cil.c -o $dir/ssh/s3_srvr.blast.08_true-unreach-call.i.cil.c.ll
features_static_analysis/extractFeatures $dir/ssh/s3_srvr.blast.08_true-unreach-call.i.cil.c.ll
clang $wFlags -S -emit-llvm $dir/ssh/s3_srvr.blast.09_false-unreach-call.i.cil.c -o $dir/ssh/s3_srvr.blast.09_false-unreach-call.i.cil.c.ll
features_static_analysis/extractFeatures $dir/ssh/s3_srvr.blast.09_false-unreach-call.i.cil.c.ll
clang $wFlags -S -emit-llvm $dir/ssh/s3_srvr.blast.09_true-unreach-call.i.cil.c -o $dir/ssh/s3_srvr.blast.09_true-unreach-call.i.cil.c.ll
features_static_analysis/extractFeatures $dir/ssh/s3_srvr.blast.09_true-unreach-call.i.cil.c.ll
clang $wFlags -S -emit-llvm $dir/ssh/s3_srvr.blast.11_true-unreach-call.i.cil.c -o $dir/ssh/s3_srvr.blast.11_true-unreach-call.i.cil.c.ll
features_static_analysis/extractFeatures $dir/ssh/s3_srvr.blast.11_true-unreach-call.i.cil.c.ll
clang $wFlags -S -emit-llvm $dir/ssh/s3_srvr.blast.12_false-unreach-call.i.cil.c -o $dir/ssh/s3_srvr.blast.12_false-unreach-call.i.cil.c.ll
features_static_analysis/extractFeatures $dir/ssh/s3_srvr.blast.12_false-unreach-call.i.cil.c.ll
clang $wFlags -S -emit-llvm $dir/ssh/s3_srvr.blast.12_true-unreach-call.i.cil.c -o $dir/ssh/s3_srvr.blast.12_true-unreach-call.i.cil.c.ll
features_static_analysis/extractFeatures $dir/ssh/s3_srvr.blast.12_true-unreach-call.i.cil.c.ll
clang $wFlags -S -emit-llvm $dir/ssh/s3_srvr.blast.13_false-unreach-call.i.cil.c -o $dir/ssh/s3_srvr.blast.13_false-unreach-call.i.cil.c.ll
features_static_analysis/extractFeatures $dir/ssh/s3_srvr.blast.13_false-unreach-call.i.cil.c.ll
clang $wFlags -S -emit-llvm $dir/ssh/s3_srvr.blast.15_true-unreach-call.i.cil.c -o $dir/ssh/s3_srvr.blast.15_true-unreach-call.i.cil.c.ll
features_static_analysis/extractFeatures $dir/ssh/s3_srvr.blast.15_true-unreach-call.i.cil.c.ll
clang $wFlags -S -emit-llvm $dir/termination-15/array04_alloca_false-termination.c.i -o $dir/termination-15/array04_alloca_false-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-15/array04_alloca_false-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-15/array07_alloca_true-termination.c.i -o $dir/termination-15/array07_alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-15/array07_alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-15/array13_alloca_true-termination.c.i -o $dir/termination-15/array13_alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-15/array13_alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-15/array16_alloca_fixed_true-termination.c.i -o $dir/termination-15/array16_alloca_fixed_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-15/array16_alloca_fixed_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-15/array16_alloca_original_false-termination.c.i -o $dir/termination-15/array16_alloca_original_false-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-15/array16_alloca_original_false-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-15/array20_alloca_false-termination.c.i -o $dir/termination-15/array20_alloca_false-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-15/array20_alloca_false-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-15/count_up_alloca_true-termination.c.i -o $dir/termination-15/count_up_alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-15/count_up_alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-15/cstrcat_mixed_alloca_true-termination.c.i -o $dir/termination-15/cstrcat_mixed_alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-15/cstrcat_mixed_alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-15/cstrchr_reverse_alloca_true-termination.c.i -o $dir/termination-15/cstrchr_reverse_alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-15/cstrchr_reverse_alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-15/cstrcmp_malloc_true-termination.c.i -o $dir/termination-15/cstrcmp_malloc_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-15/cstrcmp_malloc_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-15/cstrcmp_mixed_alloca_true-termination.c.i -o $dir/termination-15/cstrcmp_mixed_alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-15/cstrcmp_mixed_alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-15/cstrcpy_mixed_alloca_true-termination.c.i -o $dir/termination-15/cstrcpy_mixed_alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-15/cstrcpy_mixed_alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-15/cstrcspn_diffterm_alloca_true-termination.c.i -o $dir/termination-15/cstrcspn_diffterm_alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-15/cstrcspn_diffterm_alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-15/cstrcspn_reverse_alloca_true-termination.c.i -o $dir/termination-15/cstrcspn_reverse_alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-15/cstrcspn_reverse_alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-15/cstrlen_malloc_true-termination.c.i -o $dir/termination-15/cstrlen_malloc_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-15/cstrlen_malloc_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-15/cstrlen_reverse_alloca_true-termination.c.i -o $dir/termination-15/cstrlen_reverse_alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-15/cstrlen_reverse_alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-15/cstrncat_mixed_alloca_true-termination.c.i -o $dir/termination-15/cstrncat_mixed_alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-15/cstrncat_mixed_alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-15/cstrncmp_diffterm_alloca_true-termination.c.i -o $dir/termination-15/cstrncmp_diffterm_alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-15/cstrncmp_diffterm_alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-15/cstrncpy_diffterm_alloca_true-termination.c.i -o $dir/termination-15/cstrncpy_diffterm_alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-15/cstrncpy_diffterm_alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-15/cstrpbrk_diffterm_alloca_true-termination.c.i -o $dir/termination-15/cstrpbrk_diffterm_alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-15/cstrpbrk_diffterm_alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-15/cstrpbrk_reverse_alloca_true-termination.c.i -o $dir/termination-15/cstrpbrk_reverse_alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-15/cstrpbrk_reverse_alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-15/cstrspn_malloc_true-termination.c.i -o $dir/termination-15/cstrspn_malloc_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-15/cstrspn_malloc_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-15/cstrspn_reverse_alloca_true-termination.c.i -o $dir/termination-15/cstrspn_reverse_alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-15/cstrspn_reverse_alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-counterex1b_false-no-overflow.c -o $dir/termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-counterex1b_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-counterex1b_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-easy2_false-no-overflow.c -o $dir/termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-easy2_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-easy2_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-exmini_false-no-overflow.c -o $dir/termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-exmini_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-exmini_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-rsd_false-no-overflow.c -o $dir/termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-rsd_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-rsd_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted-lit/BradleyMannaSipma-ICALP2005-Fig1_false-no-overflow.c -o $dir/termination-crafted-lit/BradleyMannaSipma-ICALP2005-Fig1_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted-lit/BradleyMannaSipma-ICALP2005-Fig1_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted-lit/ChawdharyCookGulwaniSagivYang-ESOP2008-aaron12_false-no-overflow.c -o $dir/termination-crafted-lit/ChawdharyCookGulwaniSagivYang-ESOP2008-aaron12_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted-lit/ChawdharyCookGulwaniSagivYang-ESOP2008-aaron12_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted-lit/ChawdharyCookGulwaniSagivYang-ESOP2008-aaron1_false-no-overflow.c -o $dir/termination-crafted-lit/ChawdharyCookGulwaniSagivYang-ESOP2008-aaron1_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted-lit/ChawdharyCookGulwaniSagivYang-ESOP2008-aaron1_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex1.02_false-no-overflow.c -o $dir/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex1.02_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex1.02_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex1.05_false-no-overflow.c -o $dir/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex1.05_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex1.05_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex2.09_false-no-overflow.c -o $dir/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex2.09_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex2.09_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex2.10_false-no-overflow.c -o $dir/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex2.10_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex2.10_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex2.11_false-no-overflow.c -o $dir/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex2.11_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex2.11_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex2.13_false-no-overflow.c -o $dir/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex2.13_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex2.13_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex2.14_false-no-overflow.c -o $dir/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex2.14_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex2.14_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex3.02_false-no-overflow.c -o $dir/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex3.02_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex3.02_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex3.03_false-no-overflow.c -o $dir/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex3.03_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex3.03_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex3.04_false-no-overflow.c -o $dir/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex3.04_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex3.04_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex3.06_false-no-overflow.c -o $dir/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex3.06_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex3.06_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex3.10_false-no-overflow.c -o $dir/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex3.10_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex3.10_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted-lit/ColonSipma-TACAS2001-Fig1_false-no-overflow.c -o $dir/termination-crafted-lit/ColonSipma-TACAS2001-Fig1_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted-lit/ColonSipma-TACAS2001-Fig1_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted-lit/cstrncmp_false-no-overflow.c -o $dir/termination-crafted-lit/cstrncmp_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted-lit/cstrncmp_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted-lit/GulavaniGulwani-CAV2008-Fig1a_false-no-overflow.c -o $dir/termination-crafted-lit/GulavaniGulwani-CAV2008-Fig1a_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted-lit/GulavaniGulwani-CAV2008-Fig1a_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted-lit/GulwaniJainKoskinen-PLDI2009-Fig1_false-no-overflow.c -o $dir/termination-crafted-lit/GulwaniJainKoskinen-PLDI2009-Fig1_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted-lit/GulwaniJainKoskinen-PLDI2009-Fig1_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted-lit/HarrisLalNoriRajamani-SAS2010-Fig2_false-no-overflow.c -o $dir/termination-crafted-lit/HarrisLalNoriRajamani-SAS2010-Fig2_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted-lit/HarrisLalNoriRajamani-SAS2010-Fig2_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted-lit/HeizmannHoenickeLeikePodelski-ATVA2013-Fig2_false-no-overflow.c -o $dir/termination-crafted-lit/HeizmannHoenickeLeikePodelski-ATVA2013-Fig2_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted-lit/HeizmannHoenickeLeikePodelski-ATVA2013-Fig2_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted-lit/HeizmannHoenickeLeikePodelski-ATVA2013-Fig8_false-no-overflow.c -o $dir/termination-crafted-lit/HeizmannHoenickeLeikePodelski-ATVA2013-Fig8_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted-lit/HeizmannHoenickeLeikePodelski-ATVA2013-Fig8_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted-lit/LeeJonesBen-Amram-POPL2001-Ex6_false-no-overflow.c -o $dir/termination-crafted-lit/LeeJonesBen-Amram-POPL2001-Ex6_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted-lit/LeeJonesBen-Amram-POPL2001-Ex6_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted-lit/LeikeHeizmann-TACAS2014-Ex7_false-no-overflow.c -o $dir/termination-crafted-lit/LeikeHeizmann-TACAS2014-Ex7_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted-lit/LeikeHeizmann-TACAS2014-Ex7_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted-lit/Masse-VMCAI2014-Ex6_false-no-overflow.c -o $dir/termination-crafted-lit/Masse-VMCAI2014-Ex6_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted-lit/Masse-VMCAI2014-Ex6_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted-lit/Masse-VMCAI2014-Fig1b_false-no-overflow.c -o $dir/termination-crafted-lit/Masse-VMCAI2014-Fig1b_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted-lit/Masse-VMCAI2014-Fig1b_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted-lit/min_rf_false-no-overflow.c -o $dir/termination-crafted-lit/min_rf_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted-lit/min_rf_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted-lit/NoriSharma-FSE2013-Fig7_false-no-overflow.c -o $dir/termination-crafted-lit/NoriSharma-FSE2013-Fig7_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted-lit/NoriSharma-FSE2013-Fig7_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted-lit/NoriSharma-FSE2013-Fig8_false-no-overflow.c -o $dir/termination-crafted-lit/NoriSharma-FSE2013-Fig8_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted-lit/NoriSharma-FSE2013-Fig8_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted-lit/PodelskiRybalchenko-LICS2004-Fig2_false-no-overflow.c -o $dir/termination-crafted-lit/PodelskiRybalchenko-LICS2004-Fig2_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted-lit/PodelskiRybalchenko-LICS2004-Fig2_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted-lit/PodelskiRybalchenko-TACAS2011-Fig3_false-no-overflow.c -o $dir/termination-crafted-lit/PodelskiRybalchenko-TACAS2011-Fig3_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted-lit/PodelskiRybalchenko-TACAS2011-Fig3_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted-lit/PodelskiRybalchenko-VMCAI2004-Ex1_false-no-overflow.c -o $dir/termination-crafted-lit/PodelskiRybalchenko-VMCAI2004-Ex1_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted-lit/PodelskiRybalchenko-VMCAI2004-Ex1_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted/4NestedWith3Variables_false-no-overflow.c -o $dir/termination-crafted/4NestedWith3Variables_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted/4NestedWith3Variables_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted/aaron2_false-no-overflow.c -o $dir/termination-crafted/aaron2_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted/aaron2_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted/Bangalore_false-no-overflow.c -o $dir/termination-crafted/Bangalore_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted/Bangalore_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted/Cairo_step2_false-no-overflow.c -o $dir/termination-crafted/Cairo_step2_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted/Cairo_step2_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted/Collatz_unknown-termination_false-no-overflow.c -o $dir/termination-crafted/Collatz_unknown-termination_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted/Collatz_unknown-termination_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted/Hanoi_plus_false-no-overflow.c -o $dir/termination-crafted/Hanoi_plus_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted/Hanoi_plus_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted/LexIndexValue-Pointer_true-no-overflow.c -o $dir/termination-crafted/LexIndexValue-Pointer_true-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted/LexIndexValue-Pointer_true-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted/NestedRecursion_1a_false-no-overflow.c -o $dir/termination-crafted/NestedRecursion_1a_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted/NestedRecursion_1a_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted/NonTermination2_false-no-overflow.c -o $dir/termination-crafted/NonTermination2_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted/NonTermination2_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted/NonTermination4_false-no-overflow.c -o $dir/termination-crafted/NonTermination4_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted/NonTermination4_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted/NonTerminationSimple2_false-no-overflow.c -o $dir/termination-crafted/NonTerminationSimple2_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted/NonTerminationSimple2_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted/NonTerminationSimple6_false-no-overflow.c -o $dir/termination-crafted/NonTerminationSimple6_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted/NonTerminationSimple6_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted/NonTerminationSimple9_false-no-overflow.c -o $dir/termination-crafted/NonTerminationSimple9_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted/NonTerminationSimple9_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted/Pure2Phase_false-no-overflow.c -o $dir/termination-crafted/Pure2Phase_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted/Pure2Phase_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted/Rotation180_false-no-overflow.c -o $dir/termination-crafted/Rotation180_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted/Rotation180_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted/Singapore_false-no-overflow.c -o $dir/termination-crafted/Singapore_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted/Singapore_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted/Singapore_v1_false-no-overflow.c -o $dir/termination-crafted/Singapore_v1_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted/Singapore_v1_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-crafted/Toulouse-MultiBranchesToLoop_false-no-overflow.c -o $dir/termination-crafted/Toulouse-MultiBranchesToLoop_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-crafted/Toulouse-MultiBranchesToLoop_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-libowfat/basename_true-termination.c.i -o $dir/termination-libowfat/basename_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-libowfat/basename_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-libowfat/build_fullname_true-termination.c.i -o $dir/termination-libowfat/build_fullname_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-libowfat/build_fullname_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-libowfat/strcat_true-termination.c.i -o $dir/termination-libowfat/strcat_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-libowfat/strcat_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-libowfat/strcpy_small_true-termination.c.i -o $dir/termination-libowfat/strcpy_small_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-libowfat/strcpy_small_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-libowfat/strcspn_true-termination.c.i -o $dir/termination-libowfat/strcspn_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-libowfat/strcspn_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-libowfat/strrchr_short_true-termination.c.i -o $dir/termination-libowfat/strrchr_short_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-libowfat/strrchr_short_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-libowfat/strrchr_true-termination.c.i -o $dir/termination-libowfat/strrchr_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-libowfat/strrchr_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-libowfat/strtok_r_true-termination.c.i -o $dir/termination-libowfat/strtok_r_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-libowfat/strtok_r_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-libowfat/strtol_true-termination.c.i -o $dir/termination-libowfat/strtol_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-libowfat/strtol_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-libowfat/strtoul_true-termination.c.i -o $dir/termination-libowfat/strtoul_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-libowfat/strtoul_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-libowfat/strtoull_true-termination.c.i -o $dir/termination-libowfat/strtoull_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-libowfat/strtoull_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-memory-alloca/add_last-alloca_true-termination.c.i -o $dir/termination-memory-alloca/add_last-alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-memory-alloca/add_last-alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-memory-alloca/array01-alloca_true-termination.c.i -o $dir/termination-memory-alloca/array01-alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-memory-alloca/array01-alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-memory-alloca/array03-alloca_true-termination.c.i -o $dir/termination-memory-alloca/array03-alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-memory-alloca/array03-alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-memory-alloca/BrockschmidtCookFuhs-2013CAV-Fig1-alloca_true-termination.c.i -o $dir/termination-memory-alloca/BrockschmidtCookFuhs-2013CAV-Fig1-alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-memory-alloca/BrockschmidtCookFuhs-2013CAV-Fig1-alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-memory-alloca/BrockschmidtCookFuhs-2013CAV-Introduction-alloca_true-termination.c.i -o $dir/termination-memory-alloca/BrockschmidtCookFuhs-2013CAV-Introduction-alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-memory-alloca/BrockschmidtCookFuhs-2013CAV-Introduction-alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-memory-alloca/CookSeeZuleger-2013TACAS-Fig3-alloca_true-termination.c.i -o $dir/termination-memory-alloca/CookSeeZuleger-2013TACAS-Fig3-alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-memory-alloca/CookSeeZuleger-2013TACAS-Fig3-alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-memory-alloca/CookSeeZuleger-2013TACAS-Fig7b-alloca_true-termination.c.i -o $dir/termination-memory-alloca/CookSeeZuleger-2013TACAS-Fig7b-alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-memory-alloca/CookSeeZuleger-2013TACAS-Fig7b-alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-memory-alloca/cstrcat-alloca_true-termination.c.i -o $dir/termination-memory-alloca/cstrcat-alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-memory-alloca/cstrcat-alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-memory-alloca/cstrchr-alloca_true-termination.c.i -o $dir/termination-memory-alloca/cstrchr-alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-memory-alloca/cstrchr-alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-memory-alloca/cstrcpy-alloca_true-termination.c.i -o $dir/termination-memory-alloca/cstrcpy-alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-memory-alloca/cstrcpy-alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-memory-alloca/cstrcspn-alloca_true-termination.c.i -o $dir/termination-memory-alloca/cstrcspn-alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-memory-alloca/cstrcspn-alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-memory-alloca/cstrpbrk-alloca_true-termination.c.i -o $dir/termination-memory-alloca/cstrpbrk-alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-memory-alloca/cstrpbrk-alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-memory-alloca/cstrspn-alloca_true-termination.c.i -o $dir/termination-memory-alloca/cstrspn-alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-memory-alloca/cstrspn-alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-memory-alloca/easySum-alloca_true-termination.c.i -o $dir/termination-memory-alloca/easySum-alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-memory-alloca/easySum-alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-memory-alloca/ex2-alloca_true-termination.c.i -o $dir/termination-memory-alloca/ex2-alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-memory-alloca/ex2-alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-memory-alloca/genady-alloca_true-termination.c.i -o $dir/termination-memory-alloca/genady-alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-memory-alloca/genady-alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-memory-alloca/java_LogBuiltIn-alloca_true-termination.c.i -o $dir/termination-memory-alloca/java_LogBuiltIn-alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-memory-alloca/java_LogBuiltIn-alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-memory-alloca/KroeningSharyginaTsitovichWintersteiger-2010CAV-Fig1-alloca_true-termination.c.i -o $dir/termination-memory-alloca/KroeningSharyginaTsitovichWintersteiger-2010CAV-Fig1-alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-memory-alloca/KroeningSharyginaTsitovichWintersteiger-2010CAV-Fig1-alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-memory-alloca/min_rf-alloca_true-termination.c.i -o $dir/termination-memory-alloca/min_rf-alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-memory-alloca/min_rf-alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-memory-alloca/openbsd_cmemchr-alloca_true-termination.c.i -o $dir/termination-memory-alloca/openbsd_cmemchr-alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-memory-alloca/openbsd_cmemchr-alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-memory-alloca/openbsd_cmemrchr-alloca_true-termination.c.i -o $dir/termination-memory-alloca/openbsd_cmemrchr-alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-memory-alloca/openbsd_cmemrchr-alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-memory-alloca/openbsd_cstpcpy-alloca_true-termination.c.i -o $dir/termination-memory-alloca/openbsd_cstpcpy-alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-memory-alloca/openbsd_cstpcpy-alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-memory-alloca/openbsd_cstrcat-alloca_true-termination.c.i -o $dir/termination-memory-alloca/openbsd_cstrcat-alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-memory-alloca/openbsd_cstrcat-alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-memory-alloca/openbsd_cstrlcpy-alloca_true-termination.c.i -o $dir/termination-memory-alloca/openbsd_cstrlcpy-alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-memory-alloca/openbsd_cstrlcpy-alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-memory-alloca/openbsd_cstrncat-alloca_true-termination.c.i -o $dir/termination-memory-alloca/openbsd_cstrncat-alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-memory-alloca/openbsd_cstrncat-alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-memory-alloca/openbsd_cstrncpy-alloca_true-termination.c.i -o $dir/termination-memory-alloca/openbsd_cstrncpy-alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-memory-alloca/openbsd_cstrncpy-alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-memory-alloca/openbsd_cstrspn-alloca_true-termination.c.i -o $dir/termination-memory-alloca/openbsd_cstrspn-alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-memory-alloca/openbsd_cstrspn-alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-memory-alloca/rec_strlen-alloca_true-termination.c.i -o $dir/termination-memory-alloca/rec_strlen-alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-memory-alloca/rec_strlen-alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-memory-alloca/Urban-2013WST-Fig2-alloca_true-termination.c.i -o $dir/termination-memory-alloca/Urban-2013WST-Fig2-alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-memory-alloca/Urban-2013WST-Fig2-alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-memory-alloca/Urban-alloca_true-termination.c.i -o $dir/termination-memory-alloca/Urban-alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-memory-alloca/Urban-alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-memory-linkedlists/cll_by_lseg-alloca_false-termination.c.i -o $dir/termination-memory-linkedlists/cll_by_lseg-alloca_false-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-memory-linkedlists/cll_by_lseg-alloca_false-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-memory-linkedlists/cll_by_lseg_traverse-alloca_false-termination.c.i -o $dir/termination-memory-linkedlists/cll_by_lseg_traverse-alloca_false-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-memory-linkedlists/cll_by_lseg_traverse-alloca_false-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-memory-linkedlists/ll_append-alloca_true-termination.c.i -o $dir/termination-memory-linkedlists/ll_append-alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-memory-linkedlists/ll_append-alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-memory-linkedlists/ll_append_rec-alloca_true-termination.c.i -o $dir/termination-memory-linkedlists/ll_append_rec-alloca_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-memory-linkedlists/ll_append_rec-alloca_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-numeric/Avg_true_false-no-overflow.c -o $dir/termination-numeric/Avg_true_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-numeric/Avg_true_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-numeric/MultCommutative_false-no-overflow.c -o $dir/termination-numeric/MultCommutative_false-no-overflow.c.ll
features_static_analysis/extractFeatures $dir/termination-numeric/MultCommutative_false-no-overflow.c.ll
clang $wFlags -S -emit-llvm $dir/termination-recursive-malloc/chunk2_true-termination.c.i -o $dir/termination-recursive-malloc/chunk2_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-recursive-malloc/chunk2_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-recursive-malloc/rec_malloc_ex10_true-termination.c.i -o $dir/termination-recursive-malloc/rec_malloc_ex10_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-recursive-malloc/rec_malloc_ex10_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-recursive-malloc/rec_malloc_ex11B_true-termination.c.i -o $dir/termination-recursive-malloc/rec_malloc_ex11B_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-recursive-malloc/rec_malloc_ex11B_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-recursive-malloc/rec_malloc_ex11D_true-termination.c.i -o $dir/termination-recursive-malloc/rec_malloc_ex11D_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-recursive-malloc/rec_malloc_ex11D_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-recursive-malloc/rec_malloc_ex3_true-termination.c.i -o $dir/termination-recursive-malloc/rec_malloc_ex3_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-recursive-malloc/rec_malloc_ex3_true-termination.c.i.ll
clang $wFlags -S -emit-llvm $dir/termination-recursive-malloc/rec_malloc_ex7B_true-termination.c.i -o $dir/termination-recursive-malloc/rec_malloc_ex7B_true-termination.c.i.ll
features_static_analysis/extractFeatures $dir/termination-recursive-malloc/rec_malloc_ex7B_true-termination.c.i.ll