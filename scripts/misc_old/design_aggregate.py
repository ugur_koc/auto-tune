#!/usr/bin/env python2.7

import re,os
import Utils_Config as cu

domFile = '../doms/cbmc/CBMC-21opts.txt'
designFile='../designs/CBMC-21opt-SD.txt'#CBMC-33opts-SD-v1/
statFile = '../stats/stats-cbmc-21opts-Apr2.txt'
saveFile = '../designs/CBMC-32opt-Responses-Apr2.txt'

f = open(statFile,'r')
stats = f.read().splitlines()
f.close()
configMap={}
for stat in stats:
	config, cFile, propFile, result, time = stat.split(',')
	time=float(time)
	TP = FP = TN = FN = 0
	CMPLT = 1
	if result == '0':
		TN, FN = [1, 0] if 'true-' in cFile else [0, 1]
	elif result == '10':
		TP, FP = [1, 0] if 'false-' in cFile else [0, 1]
	else:
		CMPLT = 0

	if configMap.has_key(config):
		configMap[config] = [CMPLT+configMap[config][0], TP+configMap[config][1], TN+configMap[config][2], FP+configMap[config][3], FN+configMap[config][4], time+configMap[config][5]]
	else:
		configMap[config] = [CMPLT, TP, TN, FP, FN, time]

#print configMap
f = open(designFile,'r')
lines = f.read().splitlines()[1:]
f.close()

domFile = open(domFile, 'r')
options = [opt.split(' ')[0] for opt in domFile.readlines()]
domFile.close()

resultsFile = open(saveFile,'w')
for line in lines:
	runConfig = cu.designLine2Config(line, options)
	results = ''
	if configMap.has_key(runConfig):
		results='\t'.join(str(e) for e in configMap[runConfig])
	else:
		print 'aha'
	resultsFile.write(results+'\n')
resultsFile.close()