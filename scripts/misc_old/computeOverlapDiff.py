#!/usr/bin/env python2.7

import re,os,csv
import Utils_Config as cu
import subprocess as sp
import copy
import itertools

domFile = '../doms/cbmc/CBMC-21opts.txt'
statFile = '../stats/stats-cbmc-21opts-Mar4.txt'

domFile = open(domFile, 'r')
optionLines = domFile.read().splitlines()
domFile.close()

f = open(statFile,'r')
stats = f.read().splitlines()
f.close()


def diff(first, second):
	first =  set(first)
	second = set(second)
	diff1 = [item for item in first if item not in second]
	diff2 = [item for item in second if item not in first]
	return len(diff1), len(diff2)

optionMap = {}
optionIndxMap = {}
index = 0
for opt in optionLines:
	parts = opt.split(' ')
	optionMap[parts[0]] = parts[1].split(',')
	optionIndxMap[parts[0]] = index
	index += 1

#optSovertDom = optionMap['solvert'][1:]#['--cprover-smt2','--boolector','--cvc4','--mathsat','--yices','--z3']
for option,values in optionMap.iteritems():
	searchStrs = values
	if values[0] == 'bool':
		searchStrs = [option]
	elif option.startswith('--'):
		tmpValues=values[1:] if values[0]=='0' else values
		searchStrs = [option+' '+ v for v in tmpValues]
	elif 'off' == values[0]:
		searchStrs = values[1:]

	configMap={}
	if len(searchStrs)==1:
		configMap[searchStrs[0]] = [stat.split(',')[1] for stat in stats if searchStrs[0] in stat and ',0' in stat]
		configMap['Not'+searchStrs[0]] = [stat.split(',')[1] for stat in stats if searchStrs[0] not in stat and ',0' in stat]
		#searchStrs = [searchStrs[0],'Not'+searchStrs[0]]
	else:
		for searchStr in searchStrs:
			configMap[searchStr] = [stat.split(',')[1] for stat in stats if searchStr in stat and ',0' in stat]
			configMap['Not'+searchStr] = [stat.split(',')[1] for stat in stats if searchStr not in stat and ',0' in stat]
		#	searchStrs.append('Not'+searchStr)
		if values[0]=='0':
			configMap['Not'+option] = [stat.split(',')[1] for stat in stats if option not in stat and ',0' in stat]
		#	searchStrs.append('Not'+option)
		# if values[0]=='off': TODO
		# 	configMap['Not'+option] = [stat.split(',')[1] for stat in stats if ',0' in stat and option not in stat]
		# 	searchStrs.append('Not'+option)

	print option,searchStrs
	for searchStr in searchStrs:
		print searchStr
 		print diff(configMap[searchStr], configMap['Not'+searchStr])