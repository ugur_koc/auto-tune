#!/usr/bin/env python2.7

import re,os,csv,random, copy
import subprocess as sp

import Utils_ML as utils
import Utils_Config as conf

optionNames, optionSettings = conf.readDomFile('../doms/cbmc/CBMC-21opts-list.txt')
configs=conf.useCoveringArray(3, optionSettings=optionSettings)
# configs=conf.generateRandomConfigs(count=1000, optionSettings=optionSettings)
# configs=conf.enumarateNConfigs(count=1, optionSettings=optionSettings)


def createPrecisionTests(app):
	BASE_DIR='../data-ml/cbmc/precision'
	testFile = BASE_DIR+'/All-Apr18-filenames.arff'
	_, dataset = utils.readArff(testFile,0)
	metadata,_  = utils.readArff(BASE_DIR+'/cbmc-test-1.arff',0)

	programs={}
	for dp in dataset:
		label='TN' if 'true-' in dp[36] else 'TP'
		pName=dp[36]
		prog=','.join(dp[22:-4]) + ',' + label + ',' + dp[35]
		if not programs.has_key(pName):
			programs[pName]=prog

	for pName,prog in programs.iteritems():
		print pName
		saveFile = BASE_DIR+'/test-progs/'+os.path.basename(pName)+'-'+app+'.arff'
		datapoints=[]
		for config in configs:
			line = list(config)+ prog.split(',')
			datapoints.append(line)

		with open(saveFile, "wb") as sf:
			sf.write(metadata+'\n')
			writer = csv.writer(sf)
			writer.writerows(datapoints)
			sf.close()

		outs = sp.call(['java','-jar','predPrecision.jar', saveFile, pName])

def createPrecisionTestsPW(app):
	BASE_DIR='../data-ml/cbmc/precision'
	testFile = BASE_DIR+'/All-Apr18-filenames.arff'
	_, dataset = utils.readArff(testFile,0)
	metadata,_  = utils.readArff(BASE_DIR+'/cbmc-test-1.arff',0)

	programs={}
	for dp in dataset:
		label='TN' if 'true-' in dp[36] else 'TP'
		pName=dp[36]
		prog=','.join(dp[22:-4]) + ',' + label + ',' + dp[35]
		if not programs.has_key(pName):
			programs[pName]=prog

	for pName,prog in programs.iteritems():
		print pName
		saveFile = BASE_DIR+'/test-progs/'+os.path.basename(pName)+'-'+app+'.arff'
		datapoints=[]
		for config in configs:
			line = list(config)+ prog.split(',')
			datapoints.append(line)

		with open(saveFile, "wb") as sf:
			sf.write(metadata+'\n')
			writer = csv.writer(sf)
			writer.writerows(datapoints)
			sf.close()

		outs = sp.call(['java','-jar','predPrecision.jar', saveFile, pName])

def createTimeTests():
	BASE_DIR='../data-ml/cbmc/time'
	testFile = BASE_DIR+'/All-Apr2.arff'
	_, dataset = utils.readArff(testFile,0)
	metadata,_  = utils.readArff(BASE_DIR+'/cbmc-test-1.arff',0)

	programs = {}
	for dp in dataset:
		time=float(dp[0])
		case=','.join(dp[22:-2])
		if not programs.has_key(case) or programs[case] > time:
			programs[case] = time

	categories=['ReachSafety','MemSafety','ConcurrencySafety','Termination','NoOverflows']
	counter=0
	for prog,time in programs.iteritems():
		counter+=1
		for categ in categories:
			saveFile = BASE_DIR+'/test-progs/cbmc-test-prog-'+str(counter)+'-'+categ+'.arff'
			datapoints=[]
			for config in configs:
				line = ['?']+ list(config)+ prog.split(',')+[categ]
				datapoints.append(line)

			with open(saveFile, "wb") as sf:
				sf.write(metadata+'\n')
				writer = csv.writer(sf)
				writer.writerows(datapoints)
				sf.close()

createPrecisionTests('t=3')

#createTimeTests()