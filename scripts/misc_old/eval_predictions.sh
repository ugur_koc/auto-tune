#!/bin/bash

cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-15/cstrlen_malloc_true-termination.c.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe026_tso.oepc_true-unreach-call.i --full-slice --refine --z3
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted/Pure3Phase_false-no-overflow.c --full-slice --no-assumptions --partial-loops --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix015_pso.opt_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/rfi010_pso.opt_false-unreach-call.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/thin001_power.opt_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix003_rmo.oepc_false-unreach-call.i --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix004_power.opt_false-unreach-call.i --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/seq-mthreaded/rekh_aso_true-unreach-call.3.M1.c --no-assumptions --refine --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/recursive-simple/id_b3_o2_false-no-overflow.c --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-aaron3_false-no-overflow.c --drop-unused-functions --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix026_tso.oepc_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem19_label35_true-unreach-call.c --drop-unused-functions --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe024_power.opt_false-unreach-call.i --no-assumptions --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix054_pso.oepc_false-unreach-call.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted/Binary_Search_false-no-overflow.c --full-slice --no-assumptions --refine --cprover-smt2
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem03_label31_false-unreach-call.c --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe005_tso.opt_true-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix015_pso.opt_false-unreach-call.i --drop-unused-functions --full-slice --refine --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/recursive-simple/fibo_25_true-unreach-call.c --full-slice --no-assumptions --partial-loops --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/seq-mthreaded/rekcba_aso_false-unreach-call.4.M1.c --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix050_tso.opt_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix024_rmo.oepc_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix026_tso.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix039_pso.oepc_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix044_pso.opt_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix026_tso.oepc_false-unreach-call.i --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem04_label51_true-unreach-call.c --drop-unused-functions --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe021_rmo.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem03_label31_false-unreach-call.c --drop-unused-functions --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-memory-alloca/easySum-alloca_true-termination.c.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-aaron3_false-no-overflow.c --full-slice --no-assumptions --partial-loops --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem10_label37_true-unreach-call.c --drop-unused-functions --no-assumptions --cprover-smt2
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix044_pso.opt_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/seq-mthreaded/pals_STARTPALS_Triplicated_true-unreach-call.ufo.UNBOUNDED.pals.c --full-slice --no-assumptions --refine --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/MemSafety.prp sv-benchmarks/c/array-memsafety/reverse_array_unsafe_false-valid-deref.i --full-slice --refine --refine-strings --cprover-smt2
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe026_tso.oepc_true-unreach-call.i --drop-unused-functions --full-slice --refine --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/MemSafety.prp sv-benchmarks/c/array-memsafety/lis_unsafe_false-valid-deref.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem18_label24_true-unreach-call.c --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix019_pso.opt_false-unreach-call.i --no-assumptions --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-counterex1a_false-no-overflow.c --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/seq-mthreaded/pals_STARTPALS_Triplicated_true-unreach-call.ufo.UNBOUNDED.pals.c --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-aaron3_false-no-overflow.c --drop-unused-functions --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted/Binary_Search_false-no-overflow.c --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex2.17_false-no-overflow.c --full-slice --no-assumptions --refine --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem16_label37_false-unreach-call.c --drop-unused-functions --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted/Binary_Search_false-no-overflow.c --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe007_rmo.opt_false-unreach-call.i --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted/Pure3Phase_false-no-overflow.c --drop-unused-functions --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/recursive-simple/id_b3_o2_false-no-overflow.c --full-slice --no-assumptions --partial-loops --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe003_tso.oepc_true-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix044_pso.opt_false-unreach-call.i --full-slice --no-assumptions --partial-loops --refine --cvc4
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-counterex1a_false-no-overflow.c --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix026_tso.oepc_false-unreach-call.i --no-assumptions --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/recursive-simple/id_b3_o2_false-no-overflow.c --full-slice --no-assumptions --partial-loops --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex2.17_false-no-overflow.c --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-memory-alloca/openbsd_cstrlen-alloca_true-termination.c.i --full-slice --refine --z3
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem05_label49_true-unreach-call.c --drop-unused-functions --full-slice --no-assumptions --refine --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe021_power.oepc_false-unreach-call.i --full-slice --no-assumptions --refine --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix003_rmo.oepc_false-unreach-call.i --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe021_rmo.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/rfi008_pso.oepc_false-unreach-call.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/bitvector/jain_7_false-no-overflow.i --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix051_power.opt_false-unreach-call.i --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix019_pso.opt_false-unreach-call.i --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/recursive-simple/id_b3_o2_false-no-overflow.c --drop-unused-functions --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix040_power.oepc_false-unreach-call.i --full-slice --refine --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix003_rmo.oepc_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex2.17_false-no-overflow.c --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/ChawdharyCookGulwaniSagivYang-ESOP2008-aaron1_false-no-overflow.c --full-slice --no-assumptions --partial-loops --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-memory-alloca/cstrncpy-alloca_true-termination.c.i --full-slice --refine --z3
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/floats-esbmc-regression/digits_bad_while_false-unreach-call.i --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix025_rmo.opt_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/MemSafety.prp sv-benchmarks/c/array-memsafety/reverse_array_unsafe_false-valid-deref.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/bitvector/jain_7_false-no-overflow.i --no-assumptions --partial-loops --refine
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe008_pso.opt_false-unreach-call.i --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem09_label16_true-unreach-call.c --no-assumptions --refine --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/ChawdharyCookGulwaniSagivYang-ESOP2008-aaron1_false-no-overflow.c --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem13_label46_true-unreach-call.c --drop-unused-functions --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-memory-alloca/HarrisLalNoriRajamani-2010SAS-Fig1-alloca_true-termination.c.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe003_tso.oepc_true-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe008_pso.opt_false-unreach-call.i --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix050_tso.opt_false-unreach-call.i --drop-unused-functions --no-assumptions --refine-strings --z3
cbmc --propertyfile sv-benchmarks/c/MemSafety.prp sv-benchmarks/c/ldv-memsafety/memleaks_test15_false-valid-memtrack.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe027_tso.opt_true-unreach-call.i --full-slice --refine --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe021_power.oepc_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix026_tso.oepc_false-unreach-call.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/podwr000_pso.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix051_power.opt_false-unreach-call.i --full-slice --no-assumptions --refine --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix015_pso.opt_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe027_tso.opt_true-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem05_label49_true-unreach-call.c --drop-unused-functions --full-slice --no-assumptions --refine --z3
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem16_label37_false-unreach-call.c --drop-unused-functions --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix050_tso.opt_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/MemSafety.prp sv-benchmarks/c/ldv-memsafety/memleaks_test15_false-valid-memtrack.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/MemSafety.prp sv-benchmarks/c/array-memsafety/lis_unsafe_false-valid-deref.i --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex2.17_false-no-overflow.c --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe003_tso.oepc_true-unreach-call.i --full-slice --refine --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix025_power.oepc_false-unreach-call.i --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-15/cstrncmp_diffterm_alloca_true-termination.c.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-ext/30_Function_Pointer3_vs_true-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-aaron3_false-no-overflow.c --drop-unused-functions --full-slice --partial-loops --refine --yices
cbmc --propertyfile sv-benchmarks/c/MemSafety.prp sv-benchmarks/c/ldv-memsafety/memleaks_test15_false-valid-memtrack.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe007_rmo.opt_false-unreach-call.i --no-assumptions --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix054_pso.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe007_rmo.opt_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted/Binary_Search_false-no-overflow.c --full-slice --no-assumptions --partial-loops --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix019_pso.opt_false-unreach-call.i --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem04_label51_true-unreach-call.c --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem19_label35_true-unreach-call.c --drop-unused-functions --full-slice --no-assumptions --refine --boolector
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/recursive-simple/fibo_25_true-unreach-call.c --full-slice --no-assumptions --partial-loops --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix024_rmo.oepc_false-unreach-call.i --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/MemSafety.prp sv-benchmarks/c/ldv-memsafety/memleaks_test15_false-valid-memtrack.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix026_tso.oepc_false-unreach-call.i --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/rfi010_pso.opt_false-unreach-call.i
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted/Pure3Phase_false-no-overflow.c --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem13_label46_true-unreach-call.c --refine --refine-strings --cvc4
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/ChawdharyCookGulwaniSagivYang-ESOP2008-aaron1_false-no-overflow.c --drop-unused-functions --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/seq-mthreaded/rekcba_aso_false-unreach-call.4.M1.c --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-memory-alloca/openbsd_cstrlen-alloca_true-termination.c.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix026_tso.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/thin001_power.opt_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix025_rmo.opt_false-unreach-call.i --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/thin001_power.opt_false-unreach-call.i --full-slice --refine --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe027_tso.opt_true-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/loop-acceleration/diamond_true-unreach-call2.i --full-slice --partial-loops --refine --refine-strings --cprover-smt2
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe003_tso.oepc_true-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix004_power.opt_false-unreach-call.i --drop-unused-functions --full-slice --refine --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem16_label37_false-unreach-call.c --no-assumptions --refine --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-counterex1a_false-no-overflow.c --full-slice --no-assumptions --partial-loops --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/ChawdharyCookGulwaniSagivYang-ESOP2008-aaron1_false-no-overflow.c --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe008_pso.opt_false-unreach-call.i --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/NoriSharma-FSE2013-Fig7_false-no-overflow.c --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/MemSafety.prp sv-benchmarks/c/ldv-memsafety/ArraysOfVariableLength2_false-valid-deref-read.c --drop-unused-functions --full-slice --no-assumptions --partial-loops --cvc4
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem15_label16_true-unreach-call.c --no-assumptions --refine --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix044_pso.opt_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/seq-mthreaded/pals_STARTPALS_Triplicated_true-unreach-call.ufo.UNBOUNDED.pals.c --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/NoriSharma-FSE2013-Fig7_false-no-overflow.c --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe026_tso.oepc_true-unreach-call.i
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/recursive-simple/fibo_25_true-unreach-call.c --full-slice --no-assumptions --partial-loops --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-aaron3_false-no-overflow.c --drop-unused-functions --full-slice --no-assumptions --partial-loops --refine-strings --cvc4
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem05_label49_true-unreach-call.c --drop-unused-functions --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem15_label16_true-unreach-call.c --drop-unused-functions --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted/Pure3Phase_false-no-overflow.c --drop-unused-functions --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/rfi008_pso.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe021_rmo.oepc_false-unreach-call.i --drop-unused-functions --no-assumptions --refine --cvc4
cbmc --propertyfile sv-benchmarks/c/MemSafety.prp sv-benchmarks/c/array-memsafety/reverse_array_unsafe_false-valid-deref.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe024_power.opt_false-unreach-call.i --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe005_tso.opt_true-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/recursive-simple/fibo_25_true-unreach-call.c --full-slice --no-assumptions --partial-loops --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix004_power.opt_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix032_rmo.oepc_false-unreach-call.i --no-assumptions --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix024_rmo.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix025_rmo.opt_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/seq-pthread/cs_time_var_mutex_true-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem05_label49_true-unreach-call.c --drop-unused-functions --full-slice --no-assumptions --refine --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/thin001_power.opt_false-unreach-call.i --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe008_pso.opt_false-unreach-call.i --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix026_tso.oepc_false-unreach-call.i --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/recursive-simple/fibo_25_true-unreach-call.c --full-slice --no-assumptions --partial-loops --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted/Pure3Phase_false-no-overflow.c --drop-unused-functions --full-slice --partial-loops --refine
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem16_label37_false-unreach-call.c --refine --refine-strings --cvc4
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex2.17_false-no-overflow.c --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix054_pso.oepc_false-unreach-call.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/rfi007_rmo.oepc_false-unreach-call.i --full-slice --no-assumptions --refine --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/thin001_power.opt_false-unreach-call.i --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix040_power.oepc_false-unreach-call.i --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix024_rmo.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix044_pso.opt_false-unreach-call.i --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix002_rmo.opt_false-unreach-call.i --full-slice --no-assumptions --partial-loops --refine --cvc4
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/recursive-simple/fibo_25_true-unreach-call.c --full-slice --no-assumptions --partial-loops --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/rfi008_pso.oepc_false-unreach-call.i --full-slice --refine --z3
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-memory-alloca/cstrncpy-alloca_true-termination.c.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem05_label49_true-unreach-call.c --drop-unused-functions --full-slice --no-assumptions --refine --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix025_rmo.opt_false-unreach-call.i --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-counterex1a_false-no-overflow.c --full-slice --no-assumptions --refine --cprover-smt2
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-memory-alloca/easySum-alloca_true-termination.c.i --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix025_rmo.opt_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe007_rmo.opt_false-unreach-call.i --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-15/cstrlen_malloc_true-termination.c.i --full-slice --refine --z3
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-15/cstrncmp_diffterm_alloca_true-termination.c.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix019_pso.opt_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-memory-alloca/HarrisLalNoriRajamani-2010SAS-Fig1-alloca_true-termination.c.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe024_power.opt_false-unreach-call.i --no-assumptions --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix051_power.opt_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem13_label46_true-unreach-call.c --drop-unused-functions --full-slice --no-assumptions --refine --boolector
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix024_rmo.oepc_false-unreach-call.i --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe021_rmo.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-memory-alloca/easySum-alloca_true-termination.c.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/thin001_power.opt_false-unreach-call.i --no-assumptions --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted/Binary_Search_false-no-overflow.c --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-memory-alloca/openbsd_cstrlen-alloca_true-termination.c.i --drop-unused-functions --full-slice --refine --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-memory-alloca/cstrncpy-alloca_true-termination.c.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/bitvector/jain_7_false-no-overflow.i --full-slice --no-assumptions --refine --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix050_tso.opt_false-unreach-call.i --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem19_label35_true-unreach-call.c --drop-unused-functions --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/NoriSharma-FSE2013-Fig7_false-no-overflow.c --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix025_power.oepc_false-unreach-call.i --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted/Pure3Phase_false-no-overflow.c --drop-unused-functions --full-slice --partial-loops --refine --yices
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem19_label35_true-unreach-call.c --drop-unused-functions --full-slice --no-assumptions --refine --boolector
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix004_power.opt_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/recursive-simple/id_b3_o2_false-no-overflow.c --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix024_rmo.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/podwr000_pso.oepc_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix039_pso.oepc_false-unreach-call.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix054_pso.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/NoriSharma-FSE2013-Fig7_false-no-overflow.c --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe026_tso.oepc_true-unreach-call.i --full-slice --refine --z3
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem10_label24_false-unreach-call.c --drop-unused-functions --no-assumptions --cprover-smt2
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/ChawdharyCookGulwaniSagivYang-ESOP2008-aaron1_false-no-overflow.c --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/MemSafety.prp sv-benchmarks/c/ldv-memsafety/ArraysOfVariableLength2_false-valid-deref-read.c --full-slice --refine --refine-strings --cprover-smt2
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix050_tso.opt_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem05_label49_true-unreach-call.c --drop-unused-functions --full-slice --no-assumptions --refine --z3
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-memory-alloca/cstrncpy-alloca_true-termination.c.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix044_pso.opt_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem12_label00_false-unreach-call.c --drop-unused-functions --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix002_rmo.opt_false-unreach-call.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/MemSafety.prp sv-benchmarks/c/array-memsafety/lis_unsafe_false-valid-deref.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe008_pso.opt_false-unreach-call.i --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/thin001_power.opt_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex2.17_false-no-overflow.c --full-slice --no-assumptions --refine --cprover-smt2
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe008_pso.opt_false-unreach-call.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/NoriSharma-FSE2013-Fig7_false-no-overflow.c --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe007_rmo.opt_false-unreach-call.i --no-assumptions --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe007_rmo.opt_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/NoriSharma-FSE2013-Fig7_false-no-overflow.c --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix024_rmo.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix002_rmo.opt_false-unreach-call.i --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-memory-alloca/cstrncpy-alloca_true-termination.c.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe003_tso.oepc_true-unreach-call.i --full-slice --refine --z3
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/recursive-simple/fibo_25_true-unreach-call.c --full-slice --no-assumptions --partial-loops --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem07_label50_true-unreach-call.c --no-assumptions --refine --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix003_rmo.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix015_pso.opt_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/thin001_power.opt_false-unreach-call.i --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/ssh/s3_srvr.blast.02_false-unreach-call.i.cil.c --drop-unused-functions --no-assumptions --cprover-smt2
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix026_tso.oepc_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/bitvector/jain_7_false-no-overflow.i --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-15/cstrlen_malloc_true-termination.c.i --drop-unused-functions --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem05_label49_true-unreach-call.c --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem05_label49_true-unreach-call.c --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix050_tso.opt_false-unreach-call.i --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/recursive-simple/id_b3_o2_false-no-overflow.c --no-assumptions --partial-loops --refine
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem05_label34_true-unreach-call.c --drop-unused-functions --full-slice --no-assumptions --refine --z3
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/recursive-simple/id_b3_o2_false-no-overflow.c --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/podwr000_pso.oepc_false-unreach-call.i --drop-unused-functions --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix024_rmo.oepc_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/ChawdharyCookGulwaniSagivYang-ESOP2008-aaron1_false-no-overflow.c --full-slice --no-assumptions --partial-loops --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/ChawdharyCookGulwaniSagivYang-ESOP2008-aaron1_false-no-overflow.c --drop-unused-functions --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/ChawdharyCookGulwaniSagivYang-ESOP2008-aaron1_false-no-overflow.c --full-slice --no-assumptions --partial-loops --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/ChawdharyCookGulwaniSagivYang-ESOP2008-aaron1_false-no-overflow.c --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix004_power.opt_false-unreach-call.i --full-slice --refine --z3
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/NoriSharma-FSE2013-Fig7_false-no-overflow.c --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe008_pso.opt_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem05_label49_true-unreach-call.c --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/MemSafety.prp sv-benchmarks/c/ldv-memsafety/ArraysOfVariableLength2_false-valid-deref-read.c --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-recursive-malloc/chunk3_true-termination.c.i --full-slice --no-assumptions --partial-loops --refine --cvc4
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix050_tso.opt_false-unreach-call.i --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix026_tso.oepc_false-unreach-call.i --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/recursive-simple/fibo_25_true-unreach-call.c --full-slice --no-assumptions --partial-loops --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/thin001_power.opt_false-unreach-call.i --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem13_label46_true-unreach-call.c --drop-unused-functions --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe007_rmo.opt_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe003_tso.oepc_true-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-memory-alloca/easySum-alloca_true-termination.c.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-counterex1a_false-no-overflow.c --full-slice --no-assumptions --partial-loops --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe026_tso.oepc_true-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix040_power.oepc_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/thin001_power.opt_false-unreach-call.i --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/MemSafety.prp sv-benchmarks/c/ldv-memsafety/ArraysOfVariableLength2_false-valid-deref-read.c --drop-unused-functions --full-slice --no-assumptions --partial-loops --cvc4
cbmc --propertyfile sv-benchmarks/c/MemSafety.prp sv-benchmarks/c/ldv-memsafety/ArraysOfVariableLength2_false-valid-deref-read.c --full-slice --no-assumptions --refine --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem15_label06_true-unreach-call.c --drop-unused-functions --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix044_pso.opt_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-memory-alloca/easySum-alloca_true-termination.c.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/NoriSharma-FSE2013-Fig7_false-no-overflow.c --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/bitvector/jain_7_false-no-overflow.i --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix040_power.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/recursive-simple/fibo_25_true-unreach-call.c --full-slice --no-assumptions --partial-loops --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/ssh/s3_srvr.blast.02_false-unreach-call.i.cil.c --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/thin001_power.opt_false-unreach-call.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix026_tso.oepc_false-unreach-call.i --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem19_label35_true-unreach-call.c --drop-unused-functions --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/thin001_power.opt_false-unreach-call.i --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem16_label37_false-unreach-call.c --drop-unused-functions --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-ext/30_Function_Pointer3_vs_true-unreach-call.i --drop-unused-functions --full-slice --refine --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/recursive-simple/id_b3_o2_false-no-overflow.c --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix026_tso.oepc_false-unreach-call.i --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/podwr000_pso.oepc_false-unreach-call.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted/Pure3Phase_false-no-overflow.c --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted/Binary_Search_false-no-overflow.c --drop-unused-functions --full-slice --no-assumptions --partial-loops --cvc4
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem15_label16_true-unreach-call.c --drop-unused-functions --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix015_pso.opt_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/MemSafety.prp sv-benchmarks/c/array-memsafety/lis_unsafe_false-valid-deref.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix026_tso.oepc_false-unreach-call.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem19_label35_true-unreach-call.c --drop-unused-functions --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe021_power.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-counterex1a_false-no-overflow.c --full-slice --no-assumptions --partial-loops --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-memory-alloca/HarrisLalNoriRajamani-2010SAS-Fig1-alloca_true-termination.c.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-memory-linkedlists/cll_traverse-alloca_false-termination.c.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem16_label37_false-unreach-call.c --full-slice --no-assumptions --partial-loops --refine --cvc4
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/NoriSharma-FSE2013-Fig7_false-no-overflow.c --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/floats-esbmc-regression/digits_bad_while_false-unreach-call.i --full-slice --no-assumptions --partial-loops --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix032_rmo.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem10_label24_false-unreach-call.c --drop-unused-functions --no-assumptions --cprover-smt2
cbmc --propertyfile sv-benchmarks/c/MemSafety.prp sv-benchmarks/c/array-memsafety/lis_unsafe_false-valid-deref.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe021_rmo.oepc_false-unreach-call.i --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix002_rmo.opt_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/loop-acceleration/diamond_true-unreach-call2.i --full-slice --partial-loops --refine --refine-strings --cprover-smt2
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem04_label51_true-unreach-call.c --drop-unused-functions --full-slice --no-assumptions --refine --z3
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem16_label37_false-unreach-call.c --drop-unused-functions --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted/Binary_Search_false-no-overflow.c --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem07_label50_true-unreach-call.c --no-assumptions --refine --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-memory-alloca/easySum-alloca_true-termination.c.i --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix003_rmo.oepc_false-unreach-call.i --full-slice --refine --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe005_tso.opt_true-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/podwr000_pso.oepc_false-unreach-call.i --refine --refine-strings --cvc4
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-counterex1a_false-no-overflow.c --full-slice --no-assumptions --partial-loops --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix054_pso.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-15/cstrlen_malloc_true-termination.c.i --full-slice --refine --z3
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-15/cstrncmp_diffterm_alloca_true-termination.c.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix044_pso.opt_false-unreach-call.i --drop-unused-functions --full-slice --refine --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem09_label16_true-unreach-call.c --no-assumptions --refine --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/floats-esbmc-regression/digits_bad_while_false-unreach-call.i --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe007_rmo.opt_false-unreach-call.i --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe027_tso.opt_true-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix044_pso.opt_false-unreach-call.i --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/recursive-simple/fibo_25_true-unreach-call.c --full-slice --no-assumptions --partial-loops --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/NoriSharma-FSE2013-Fig7_false-no-overflow.c --full-slice --no-assumptions --partial-loops --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe021_power.oepc_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix024_rmo.oepc_false-unreach-call.i --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix026_tso.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted/Binary_Search_false-no-overflow.c --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-memory-alloca/HarrisLalNoriRajamani-2010SAS-Fig1-alloca_true-termination.c.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-aaron3_false-no-overflow.c --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/rfi010_pso.opt_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted/Binary_Search_false-no-overflow.c --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem05_label34_true-unreach-call.c --no-assumptions --refine --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe008_pso.opt_false-unreach-call.i --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix044_pso.opt_false-unreach-call.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem12_label16_true-unreach-call.c --drop-unused-functions --full-slice --no-assumptions --refine
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem12_label00_false-unreach-call.c --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix025_power.oepc_false-unreach-call.i --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem10_label24_false-unreach-call.c --drop-unused-functions --no-assumptions --cprover-smt2
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix040_power.oepc_false-unreach-call.i --full-slice --refine --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix024_rmo.oepc_false-unreach-call.i --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-recursive-malloc/chunk3_true-termination.c.i --full-slice --refine --z3
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted/Pure3Phase_false-no-overflow.c --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem04_label51_true-unreach-call.c --drop-unused-functions --full-slice --no-assumptions --refine --z3
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted/Binary_Search_false-no-overflow.c --full-slice --no-assumptions --refine --cprover-smt2
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-memory-alloca/easySum-alloca_true-termination.c.i --drop-unused-functions --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/rfi007_rmo.oepc_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix025_power.oepc_false-unreach-call.i --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe024_power.opt_false-unreach-call.i --full-slice --no-assumptions --refine --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/recursive-simple/id_b3_o2_false-no-overflow.c --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem05_label49_true-unreach-call.c --drop-unused-functions --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix004_power.opt_false-unreach-call.i --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix051_power.opt_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/thin001_power.opt_false-unreach-call.i --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem03_label31_false-unreach-call.c --drop-unused-functions --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe021_rmo.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/rfi007_rmo.oepc_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem04_label51_true-unreach-call.c --no-assumptions --refine --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix024_rmo.oepc_false-unreach-call.i --full-slice --no-assumptions --partial-loops --refine --cvc4
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-memory-alloca/easySum-alloca_true-termination.c.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix002_rmo.opt_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted/Binary_Search_false-no-overflow.c --drop-unused-functions --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix024_rmo.oepc_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe003_tso.oepc_true-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix039_pso.oepc_false-unreach-call.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-counterex1a_false-no-overflow.c --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted/Pure3Phase_false-no-overflow.c --full-slice --no-assumptions --partial-loops --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex2.17_false-no-overflow.c --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem12_label00_false-unreach-call.c --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/rfi008_pso.oepc_false-unreach-call.i --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/MemSafety.prp sv-benchmarks/c/array-memsafety/lis_unsafe_false-valid-deref.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/rfi007_rmo.oepc_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix040_power.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/thin001_power.opt_false-unreach-call.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix050_tso.opt_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/recursive-simple/id_b3_o2_false-no-overflow.c --drop-unused-functions --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix015_pso.opt_false-unreach-call.i --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted/Pure3Phase_false-no-overflow.c --full-slice --no-assumptions --partial-loops --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/recursive-simple/id_b3_o2_false-no-overflow.c --full-slice --no-assumptions --partial-loops --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem12_label16_true-unreach-call.c --full-slice --no-assumptions --partial-loops --refine --cvc4
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex2.17_false-no-overflow.c --drop-unused-functions --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe003_tso.oepc_true-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-aaron3_false-no-overflow.c --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-memory-linkedlists/cll_traverse-alloca_false-termination.c.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix039_pso.oepc_false-unreach-call.i --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix025_power.oepc_false-unreach-call.i --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/recursive-simple/id_b3_o2_false-no-overflow.c --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted/Binary_Search_false-no-overflow.c --drop-unused-functions --full-slice --no-assumptions --partial-loops --cvc4
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe003_tso.oepc_true-unreach-call.i --drop-unused-functions --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/rfi007_rmo.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/podwr000_pso.oepc_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix044_pso.opt_false-unreach-call.i --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix025_rmo.opt_false-unreach-call.i --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-15/cstrlen_malloc_true-termination.c.i --drop-unused-functions --full-slice --no-assumptions --cvc4
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix051_power.opt_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix054_pso.oepc_false-unreach-call.i --refine --refine-strings --cvc4
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex2.17_false-no-overflow.c --full-slice --no-assumptions --partial-loops --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-counterex1a_false-no-overflow.c --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix032_rmo.oepc_false-unreach-call.i --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted/Pure3Phase_false-no-overflow.c --drop-unused-functions --full-slice --partial-loops --refine --yices
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe005_tso.opt_true-unreach-call.i --full-slice --refine --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/rfi007_rmo.oepc_false-unreach-call.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/NoriSharma-FSE2013-Fig7_false-no-overflow.c --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix024_rmo.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/seq-mthreaded/rekh_aso_true-unreach-call.3.M1.c --no-assumptions --refine --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix051_power.opt_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted/Pure3Phase_false-no-overflow.c --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted/Pure3Phase_false-no-overflow.c --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/MemSafety.prp sv-benchmarks/c/ldv-memsafety/memleaks_test15_false-valid-memtrack.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/bitvector/jain_7_false-no-overflow.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem05_label49_true-unreach-call.c --drop-unused-functions --full-slice --no-assumptions --refine --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe003_tso.oepc_true-unreach-call.i --drop-unused-functions --full-slice --refine --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix051_power.opt_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-memory-alloca/easySum-alloca_true-termination.c.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix002_rmo.opt_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/rfi008_pso.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted/Pure3Phase_false-no-overflow.c --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix003_rmo.oepc_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe021_power.oepc_false-unreach-call.i --full-slice --refine --z3
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex2.17_false-no-overflow.c --full-slice --no-assumptions --refine --cprover-smt2
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix040_power.oepc_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix032_rmo.oepc_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-counterex1a_false-no-overflow.c --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/MemSafety.prp sv-benchmarks/c/array-memsafety/reverse_array_unsafe_false-valid-deref.i --full-slice --refine --refine-strings --cprover-smt2
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-memory-alloca/cstrncpy-alloca_true-termination.c.i --drop-unused-functions --full-slice --no-assumptions --cvc4
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe003_tso.oepc_true-unreach-call.i --full-slice --refine --z3
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem15_label06_true-unreach-call.c --drop-unused-functions --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem16_label37_false-unreach-call.c --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe021_power.oepc_false-unreach-call.i --full-slice --refine --z3
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/recursive-simple/fibo_25_true-unreach-call.c --full-slice --no-assumptions --partial-loops --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix019_pso.opt_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-aaron3_false-no-overflow.c --full-slice --no-assumptions --partial-loops --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix002_rmo.opt_false-unreach-call.i --full-slice --refine --z3
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-memory-linkedlists/cll_traverse-alloca_false-termination.c.i --drop-unused-functions --full-slice --no-assumptions --cvc4
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/ChawdharyCookGulwaniSagivYang-ESOP2008-aaron1_false-no-overflow.c --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/rfi007_rmo.oepc_false-unreach-call.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix024_rmo.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/bitvector/jain_7_false-no-overflow.i --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem16_label37_false-unreach-call.c --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe026_tso.oepc_true-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix015_pso.opt_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/recursive-simple/fibo_25_true-unreach-call.c --full-slice --no-assumptions --partial-loops --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/MemSafety.prp sv-benchmarks/c/ldv-memsafety/ArraysOfVariableLength2_false-valid-deref-read.c --drop-unused-functions --full-slice --no-assumptions --partial-loops --cvc4
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-memory-linkedlists/cll_traverse-alloca_false-termination.c.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/seq-mthreaded/pals_STARTPALS_Triplicated_true-unreach-call.ufo.UNBOUNDED.pals.c --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem10_label37_true-unreach-call.c --no-assumptions --refine --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix026_tso.oepc_false-unreach-call.i --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem18_label24_true-unreach-call.c --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-memory-alloca/openbsd_cstrlen-alloca_true-termination.c.i --full-slice --refine --z3
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem04_label51_true-unreach-call.c --drop-unused-functions --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe021_rmo.oepc_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix026_tso.oepc_false-unreach-call.i --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe007_rmo.opt_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem10_label37_true-unreach-call.c --drop-unused-functions --no-assumptions --cprover-smt2
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix002_rmo.opt_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-memory-linkedlists/cll_traverse-alloca_false-termination.c.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe024_power.opt_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/podwr000_pso.oepc_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/rfi008_pso.oepc_false-unreach-call.i --full-slice --refine --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe027_tso.opt_true-unreach-call.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe024_power.opt_false-unreach-call.i --full-slice --no-assumptions --refine --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-15/cstrlen_malloc_true-termination.c.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/bitvector/jain_7_false-no-overflow.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-memory-alloca/openbsd_cstrlen-alloca_true-termination.c.i --full-slice --refine --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe021_rmo.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/MemSafety.prp sv-benchmarks/c/ldv-memsafety/ArraysOfVariableLength2_false-valid-deref-read.c --drop-unused-functions --full-slice --no-assumptions --partial-loops --cvc4
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix002_rmo.opt_false-unreach-call.i --full-slice --refine --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix025_rmo.opt_false-unreach-call.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted/Pure3Phase_false-no-overflow.c --full-slice --no-assumptions --refine --cprover-smt2
cbmc --propertyfile sv-benchmarks/c/MemSafety.prp sv-benchmarks/c/ldv-memsafety/ArraysOfVariableLength2_false-valid-deref-read.c --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-memory-linkedlists/cll_traverse-alloca_false-termination.c.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/NoriSharma-FSE2013-Fig7_false-no-overflow.c --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/rfi007_rmo.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix044_pso.opt_false-unreach-call.i --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/recursive-simple/id_b3_o2_false-no-overflow.c --full-slice --no-assumptions --refine --cprover-smt2
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem04_label51_true-unreach-call.c --drop-unused-functions --full-slice --no-assumptions --refine --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix019_pso.opt_false-unreach-call.i --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex2.17_false-no-overflow.c --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem12_label16_true-unreach-call.c --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix025_rmo.opt_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/rfi007_rmo.oepc_false-unreach-call.i --full-slice --refine --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe026_tso.oepc_true-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem06_label53_true-unreach-call.c --drop-unused-functions --full-slice --no-assumptions --refine --z3
cbmc --propertyfile sv-benchmarks/c/MemSafety.prp sv-benchmarks/c/ldv-memsafety/ArraysOfVariableLength2_false-valid-deref-read.c --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/bitvector/jain_7_false-no-overflow.i --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/podwr000_pso.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe024_power.opt_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix054_pso.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/recursive-simple/id_b3_o2_false-no-overflow.c --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted/Binary_Search_false-no-overflow.c --full-slice --no-assumptions --refine --cprover-smt2
cbmc --propertyfile sv-benchmarks/c/MemSafety.prp sv-benchmarks/c/ldv-memsafety/memleaks_test15_false-valid-memtrack.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-15/cstrlen_malloc_true-termination.c.i
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-15/cstrncmp_diffterm_alloca_true-termination.c.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/MemSafety.prp sv-benchmarks/c/ldv-memsafety/ArraysOfVariableLength2_false-valid-deref-read.c --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix039_pso.oepc_false-unreach-call.i --full-slice --no-assumptions --partial-loops --refine --cvc4
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-aaron3_false-no-overflow.c --drop-unused-functions --no-assumptions --refine --cvc4
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/recursive-simple/id_b3_o2_false-no-overflow.c --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem05_label49_true-unreach-call.c --drop-unused-functions --full-slice --no-assumptions --refine --z3
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-15/cstrncmp_diffterm_alloca_true-termination.c.i
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/recursive-simple/fibo_25_true-unreach-call.c --full-slice --no-assumptions --partial-loops --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted/Binary_Search_false-no-overflow.c --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem16_label37_false-unreach-call.c --drop-unused-functions --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem15_label06_true-unreach-call.c --full-slice --no-assumptions --refine --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix040_power.oepc_false-unreach-call.i --full-slice --no-assumptions --refine --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix050_tso.opt_false-unreach-call.i --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe024_power.opt_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix054_pso.oepc_false-unreach-call.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-aaron3_false-no-overflow.c --drop-unused-functions --full-slice --no-assumptions --partial-loops --refine-strings --cvc4
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe021_rmo.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-aaron3_false-no-overflow.c --drop-unused-functions --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix054_pso.oepc_false-unreach-call.i --full-slice --refine --z3
cbmc --propertyfile sv-benchmarks/c/MemSafety.prp sv-benchmarks/c/ldv-memsafety/ArraysOfVariableLength2_false-valid-deref-read.c --drop-unused-functions --full-slice --partial-loops --refine --refine-strings --cvc4
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix024_rmo.oepc_false-unreach-call.i --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe003_tso.oepc_true-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix003_rmo.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix024_rmo.oepc_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix032_rmo.oepc_false-unreach-call.i --full-slice --no-assumptions --refine --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/NoriSharma-FSE2013-Fig7_false-no-overflow.c --full-slice --no-assumptions --refine --cprover-smt2
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix050_tso.opt_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix032_rmo.oepc_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem13_label46_true-unreach-call.c --full-slice --no-assumptions --partial-loops --refine --cvc4
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe005_tso.opt_true-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/ChawdharyCookGulwaniSagivYang-ESOP2008-aaron1_false-no-overflow.c --full-slice --no-assumptions --partial-loops --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe024_power.opt_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe024_power.opt_false-unreach-call.i --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/floats-esbmc-regression/digits_bad_while_false-unreach-call.i --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/rfi007_rmo.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix050_tso.opt_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-memory-alloca/openbsd_cstrlen-alloca_true-termination.c.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/ssh/s3_srvr.blast.02_false-unreach-call.i.cil.c --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem13_label46_true-unreach-call.c --drop-unused-functions --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem06_label53_true-unreach-call.c --drop-unused-functions --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/bitvector/jain_7_false-no-overflow.i --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/recursive-simple/fibo_25_true-unreach-call.c --full-slice --no-assumptions --partial-loops --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix050_tso.opt_false-unreach-call.i --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex2.17_false-no-overflow.c --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix032_rmo.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/thin001_power.opt_false-unreach-call.i --full-slice --refine --z3
cbmc --propertyfile sv-benchmarks/c/MemSafety.prp sv-benchmarks/c/array-memsafety/reverse_array_unsafe_false-valid-deref.i --full-slice --no-assumptions --refine --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix050_tso.opt_false-unreach-call.i --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix025_power.oepc_false-unreach-call.i --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-aaron3_false-no-overflow.c --full-slice --no-assumptions --refine --cprover-smt2
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix026_tso.oepc_false-unreach-call.i --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/floats-esbmc-regression/digits_bad_while_false-unreach-call.i --full-slice --no-assumptions --partial-loops --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix032_rmo.oepc_false-unreach-call.i --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix032_rmo.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-recursive-malloc/chunk3_true-termination.c.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/thin001_power.opt_false-unreach-call.i --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix051_power.opt_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/bitvector/jain_7_false-no-overflow.i --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted/Pure3Phase_false-no-overflow.c --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix040_power.oepc_false-unreach-call.i --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem10_label37_true-unreach-call.c --drop-unused-functions --no-assumptions --cprover-smt2
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-counterex1a_false-no-overflow.c --full-slice --no-assumptions --partial-loops --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/rfi010_pso.opt_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix054_pso.oepc_false-unreach-call.i --full-slice --refine --z3
cbmc --propertyfile sv-benchmarks/c/MemSafety.prp sv-benchmarks/c/array-memsafety/lis_unsafe_false-valid-deref.i --drop-unused-functions --full-slice --refine --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem10_label37_true-unreach-call.c --drop-unused-functions --no-assumptions --cprover-smt2
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem12_label00_false-unreach-call.c --drop-unused-functions --full-slice --no-assumptions --refine --boolector
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe003_tso.oepc_true-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix024_rmo.oepc_false-unreach-call.i --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix015_pso.opt_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem13_label46_true-unreach-call.c --drop-unused-functions --full-slice --no-assumptions --refine --boolector
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem03_label31_false-unreach-call.c --drop-unused-functions --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/thin001_power.opt_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex2.17_false-no-overflow.c --drop-unused-functions --full-slice --refine --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted/Pure3Phase_false-no-overflow.c --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix026_tso.oepc_false-unreach-call.i --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted/Binary_Search_false-no-overflow.c --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix024_rmo.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem19_label35_true-unreach-call.c --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/thin001_power.opt_false-unreach-call.i --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/ChawdharyCookGulwaniSagivYang-ESOP2008-aaron1_false-no-overflow.c --full-slice --partial-loops --refine --refine-strings
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix025_rmo.opt_false-unreach-call.i --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-aaron3_false-no-overflow.c --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem04_label51_true-unreach-call.c --drop-unused-functions --full-slice --no-assumptions --refine --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/rfi007_rmo.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix019_pso.opt_false-unreach-call.i --drop-unused-functions --no-assumptions --refine --cvc4
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-aaron3_false-no-overflow.c --drop-unused-functions --full-slice --no-assumptions --partial-loops --refine-strings --cvc4
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix004_power.opt_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/ChawdharyCookGulwaniSagivYang-ESOP2008-aaron1_false-no-overflow.c --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/bitvector/jain_7_false-no-overflow.i --no-assumptions --partial-loops --refine
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-15/cstrpbrk_diffterm_alloca_true-termination.c.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe021_power.oepc_false-unreach-call.i --full-slice --refine --z3
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem10_label24_false-unreach-call.c --refine --refine-strings --cvc4
cbmc --propertyfile sv-benchmarks/c/MemSafety.prp sv-benchmarks/c/ldv-memsafety/ArraysOfVariableLength2_false-valid-deref-read.c --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-15/cstrncmp_diffterm_alloca_true-termination.c.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-memory-alloca/easySum-alloca_true-termination.c.i --full-slice --refine --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix044_pso.opt_false-unreach-call.i --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem06_label53_true-unreach-call.c --drop-unused-functions --full-slice --no-assumptions --refine --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix026_tso.oepc_false-unreach-call.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix002_rmo.opt_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix039_pso.oepc_false-unreach-call.i --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe024_power.opt_false-unreach-call.i --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-aaron3_false-no-overflow.c --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted/Pure3Phase_false-no-overflow.c --no-assumptions --partial-loops --refine
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-recursive-malloc/chunk3_true-termination.c.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe008_pso.opt_false-unreach-call.i --drop-unused-functions --no-assumptions --refine --cvc4
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix025_power.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/recursive-simple/id_b3_o2_false-no-overflow.c --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/NoriSharma-FSE2013-Fig7_false-no-overflow.c --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix040_power.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-counterex1a_false-no-overflow.c --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/MemSafety.prp sv-benchmarks/c/ldv-memsafety/memleaks_test15_false-valid-memtrack.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/rfi010_pso.opt_false-unreach-call.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix024_rmo.oepc_false-unreach-call.i --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix025_power.oepc_false-unreach-call.i --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem15_label16_true-unreach-call.c --drop-unused-functions --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/seq-mthreaded/rekcba_aso_false-unreach-call.4.M1.c --no-assumptions --refine --refine-strings
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/recursive-simple/fibo_25_true-unreach-call.c --full-slice --no-assumptions --partial-loops --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe027_tso.opt_true-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe021_rmo.oepc_false-unreach-call.i --full-slice --refine --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe026_tso.oepc_true-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/seq-pthread/cs_time_var_mutex_true-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-memory-alloca/easySum-alloca_true-termination.c.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix025_rmo.opt_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe021_power.oepc_false-unreach-call.i --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex2.17_false-no-overflow.c --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem19_label35_true-unreach-call.c --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/rfi007_rmo.oepc_false-unreach-call.i --full-slice --refine --z3
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem19_label35_true-unreach-call.c --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-memory-linkedlists/cll_traverse-alloca_false-termination.c.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-aaron3_false-no-overflow.c --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/MemSafety.prp sv-benchmarks/c/ldv-memsafety/ArraysOfVariableLength2_false-valid-deref-read.c --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/MemSafety.prp sv-benchmarks/c/array-memsafety/lis_unsafe_false-valid-deref.i --drop-unused-functions --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/bitvector/jain_7_false-no-overflow.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix025_power.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem03_label31_false-unreach-call.c --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem03_label31_false-unreach-call.c --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/bitvector/jain_7_false-no-overflow.i --full-slice --partial-loops --refine --refine-strings --cprover-smt2
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/recursive-simple/fibo_25_true-unreach-call.c --full-slice --no-assumptions --partial-loops --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem10_label37_true-unreach-call.c --drop-unused-functions --no-assumptions --cprover-smt2
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix015_pso.opt_false-unreach-call.i --drop-unused-functions --full-slice --refine --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/floats-esbmc-regression/digits_bad_while_false-unreach-call.i --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix004_power.opt_false-unreach-call.i --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-memory-alloca/cstrncpy-alloca_true-termination.c.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix019_pso.opt_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-15/cstrlen_malloc_true-termination.c.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/NoriSharma-FSE2013-Fig7_false-no-overflow.c --full-slice --no-assumptions --refine --cprover-smt2
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem05_label34_true-unreach-call.c --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe024_power.opt_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-memory-linkedlists/cll_traverse-alloca_false-termination.c.i --full-slice --refine --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe026_tso.oepc_true-unreach-call.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem15_label06_true-unreach-call.c --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix044_pso.opt_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem19_label35_true-unreach-call.c --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/MemSafety.prp sv-benchmarks/c/ldv-memsafety/memleaks_test15_false-valid-memtrack.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix051_power.opt_false-unreach-call.i --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe008_pso.opt_false-unreach-call.i --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe026_tso.oepc_true-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem18_label24_true-unreach-call.c --no-assumptions --refine --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix024_rmo.oepc_false-unreach-call.i --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix032_rmo.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix002_rmo.opt_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix039_pso.oepc_false-unreach-call.i --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix050_tso.opt_false-unreach-call.i --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem04_label51_true-unreach-call.c --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/recursive-simple/id_b3_o2_false-no-overflow.c --full-slice --no-assumptions --partial-loops --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix025_rmo.opt_false-unreach-call.i --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-15/cstrlen_malloc_true-termination.c.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/MemSafety.prp sv-benchmarks/c/ldv-memsafety/ArraysOfVariableLength2_false-valid-deref-read.c --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix015_pso.opt_false-unreach-call.i --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe026_tso.oepc_true-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix040_power.oepc_false-unreach-call.i --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem19_label35_true-unreach-call.c --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/ChenFlurMukhopadhyay-SAS2012-Ex2.17_false-no-overflow.c --full-slice --no-assumptions --partial-loops --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix003_rmo.oepc_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/loop-acceleration/diamond_true-unreach-call2.i --full-slice --partial-loops --refine --refine-strings --cprover-smt2
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe021_power.oepc_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem07_label50_true-unreach-call.c --no-assumptions --refine --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix024_rmo.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix002_rmo.opt_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/MemSafety.prp sv-benchmarks/c/array-memsafety/lis_unsafe_false-valid-deref.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem18_label24_true-unreach-call.c --no-assumptions --refine --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix050_tso.opt_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix025_power.oepc_false-unreach-call.i --full-slice --no-assumptions --refine --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/rfi008_pso.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/MemSafety.prp sv-benchmarks/c/array-memsafety/reverse_array_unsafe_false-valid-deref.i --full-slice --refine --refine-strings --cprover-smt2
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/rfi007_rmo.oepc_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/rfi010_pso.opt_false-unreach-call.i --full-slice --refine --z3
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem13_label46_true-unreach-call.c --drop-unused-functions --full-slice --no-assumptions --refine --boolector
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix002_rmo.opt_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem19_label35_true-unreach-call.c --drop-unused-functions --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix025_rmo.opt_false-unreach-call.i --full-slice --refine --z3
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-aaron3_false-no-overflow.c --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-15/cstrlen_malloc_true-termination.c.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe007_rmo.opt_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/bitvector/jain_7_false-no-overflow.i --full-slice --no-assumptions --partial-loops --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/thin001_power.opt_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/rfi010_pso.opt_false-unreach-call.i --full-slice --refine --z3
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem04_label51_true-unreach-call.c --drop-unused-functions --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/MemSafety.prp sv-benchmarks/c/array-memsafety/lis_unsafe_false-valid-deref.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/seq-mthreaded/pals_STARTPALS_Triplicated_true-unreach-call.ufo.UNBOUNDED.pals.c --drop-unused-functions --no-assumptions --cprover-smt2
cbmc --propertyfile sv-benchmarks/c/MemSafety.prp sv-benchmarks/c/array-memsafety/lis_unsafe_false-valid-deref.i --full-slice --refine --refine-strings --cprover-smt2
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-memory-alloca/openbsd_cstrlen-alloca_true-termination.c.i --full-slice --refine --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/rfi007_rmo.oepc_false-unreach-call.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/array-tiling/skipped_true-unreach-call.i --full-slice --no-assumptions --partial-loops --refine --cvc4
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix025_power.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe021_rmo.oepc_false-unreach-call.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/MemSafety.prp sv-benchmarks/c/ldv-memsafety/memleaks_test15_false-valid-memtrack.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix002_rmo.opt_false-unreach-call.i --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem16_label37_false-unreach-call.c --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix024_rmo.oepc_false-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem13_label46_true-unreach-call.c --drop-unused-functions --full-slice --no-assumptions --refine --boolector
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem18_label24_true-unreach-call.c --full-slice --no-assumptions --refine --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted/Pure3Phase_false-no-overflow.c --drop-unused-functions --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/MemSafety.prp sv-benchmarks/c/array-memsafety/lis_unsafe_false-valid-deref.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/NoriSharma-FSE2013-Fig7_false-no-overflow.c --full-slice --partial-loops --refine --refine-strings --cprover-smt2
cbmc --propertyfile sv-benchmarks/c/MemSafety.prp sv-benchmarks/c/ldv-memsafety/memleaks_test15_false-valid-memtrack.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix050_tso.opt_false-unreach-call.i --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-aaron3_false-no-overflow.c --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-memory-linkedlists/cll_traverse-alloca_false-termination.c.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/recursive-simple/id_b3_o2_false-no-overflow.c --no-assumptions --partial-loops --refine
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem07_label50_true-unreach-call.c --refine --refine-strings --cvc4
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/floats-esbmc-regression/digits_bad_while_false-unreach-call.i --full-slice --no-assumptions --partial-loops --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem12_label00_false-unreach-call.c --drop-unused-functions --full-slice --no-assumptions --refine --boolector
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix024_rmo.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/thin001_power.opt_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/recursive-simple/fibo_25_true-unreach-call.c --full-slice --no-assumptions --partial-loops --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem13_label46_true-unreach-call.c --drop-unused-functions --full-slice --no-assumptions --refine --boolector
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe003_tso.oepc_true-unreach-call.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/recursive-simple/fibo_25_true-unreach-call.c --full-slice --no-assumptions --partial-loops --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem05_label49_true-unreach-call.c --drop-unused-functions --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-counterex1a_false-no-overflow.c --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem18_label24_true-unreach-call.c --full-slice --no-assumptions --refine --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-memory-alloca/easySum-alloca_true-termination.c.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe027_tso.opt_true-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-aaron3_false-no-overflow.c --drop-unused-functions --full-slice --no-assumptions --partial-loops --refine-strings --cvc4
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix025_power.oepc_false-unreach-call.i --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem04_label51_true-unreach-call.c --drop-unused-functions --full-slice --no-assumptions --refine --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe026_tso.oepc_true-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix040_power.oepc_false-unreach-call.i --full-slice --no-assumptions --partial-loops --refine --cvc4
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-memory-linkedlists/cll_traverse-alloca_false-termination.c.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix015_pso.opt_false-unreach-call.i --full-slice --no-assumptions --partial-loops --z3
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/AliasDarteFeautrierGonnord-SAS2010-counterex1a_false-no-overflow.c --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix026_tso.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix026_tso.oepc_false-unreach-call.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/thin001_power.opt_false-unreach-call.i --no-assumptions --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix024_rmo.oepc_false-unreach-call.i --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix004_power.opt_false-unreach-call.i --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/MemSafety.prp sv-benchmarks/c/ldv-memsafety/ArraysOfVariableLength2_false-valid-deref-read.c --full-slice --refine --refine-strings --cprover-smt2
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix026_tso.oepc_false-unreach-call.i --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/seq-mthreaded/rekcba_aso_false-unreach-call.4.M1.c --no-assumptions --refine --refine-strings
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix003_rmo.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix032_rmo.oepc_false-unreach-call.i --full-slice --refine --z3
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem10_label37_true-unreach-call.c --drop-unused-functions --no-assumptions --cprover-smt2
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-recursive-malloc/chunk3_true-termination.c.i --drop-unused-functions --full-slice --no-assumptions --cvc4
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem03_label31_false-unreach-call.c --refine --refine-strings --cvc4
cbmc --propertyfile sv-benchmarks/c/Termination.prp sv-benchmarks/c/termination-15/cstrpbrk_diffterm_alloca_true-termination.c.i --full-slice --mathsat
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix004_power.opt_false-unreach-call.i --full-slice --refine-strings --boolector
cbmc --propertyfile sv-benchmarks/c/NoOverflows.prp sv-benchmarks/c/termination-crafted-lit/ChawdharyCookGulwaniSagivYang-ESOP2008-aaron1_false-no-overflow.c --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem12_label00_false-unreach-call.c --drop-unused-functions --full-slice --no-assumptions --refine --boolector
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/recursive-simple/fibo_25_true-unreach-call.c --full-slice --no-assumptions --partial-loops --refine-strings --yices
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/mix044_pso.opt_false-unreach-call.i --full-slice --refine --z3
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/safe026_tso.oepc_true-unreach-call.i --full-slice --refine --z3
cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/eca-rers2012/Problem15_label06_true-unreach-call.c --drop-unused-functions --full-slice --no-assumptions --refine-strings
cbmc --propertyfile sv-benchmarks/c/ConcurrencySafety.prp sv-benchmarks/c/pthread-wmm/rfi008_pso.oepc_false-unreach-call.i --drop-unused-functions --full-slice --refine-strings --mathsat