#!/bin/bash

process_file()
{
	castxml --castxml-output=1 $filepath
 	OUT=$?
	if [ $OUT -eq 0 ]; then
		numIf=$(grep "if\s*(" $filepath | wc -l)
		numFor=$(grep "for\s*(" $filepath | wc -l)
		#numFunc=$(ctags -x --c-types=f $filepath | wc -l)
		cscope -b -u -f db.csc $filepath
		numCall=$(cscope -d -f db.csc -L -3 '.*' | wc -l)
	
	    filename="${filepath%?}xml"
		xmlFile=`basename $filename`
		numVar=$(grep "<Variable " $xmlFile | wc -l)
		numFundType=$(grep "<FundamentalType " $xmlFile | wc -l)
		numFunc=$(grep "<Function " $xmlFile | wc -l)
		numArrType=$(grep "<ArrayType " $xmlFile | wc -l)
		numPointerType=$(grep "<PointerType " $xmlFile | wc -l)
		numCompType=$(grep "<ElaboratedType  " $xmlFile | wc -l)
		numThings=$(grep "id=\"_" $xmlFile | wc -l)
		rm db.csc $xmlFile
	fi
}

if [[ $# == 1 ]]; then #process singlefile
	filepath="$1"
	numIf=$(grep "if\s*(" $filepath | wc -l)
	numFor=$(grep "for\s*(" $filepath | wc -l)
	numFunc=$(ctags -x --c-types=f $filepath | wc -l)
	cscope -b -u -f db.csc $filepath
	numCall=$(cscope -d -f db.csc -L -3 '.*' | wc -l)
	numVar=-1
	numFundType=-1
	numArrType=-1
	numPointerType=-1
	numCompType=-1
	numThings=-1
	LOC=-1
	process_file $filepath
	
	# getting LOC
	CLOCRSLT=$(cloc --csv --quiet $filepath)
	set -f                      # avoid globbing (expansion of *).
	array=(${CLOCRSLT//,/ })
	LOC=${array[18]}
	
	echo "$filepath,$numVar,$numIf,$numFor,$numFunc,$numCall,$numFundType,$numArrType,$numPointerType,$numCompType,$numThings,$LOC"
else # second argument should be the stats file
	while IFS=, read -r tool filepath comm LoC category status cpu mem energy label complete
	do
		process_file $filepath
		echo "$filepath,$category,$status,$label,$LoC,$numVar,$numIf,$numFor,$numFunc,$numCall,$numFundType,$numArrType,$numPointerType,$numCompType,$numThings,$complete"
	done < "$2" 
fi
