#!/bin/bash

#logFile='log_cbmc_tmp.txt'
logFile='logs/log_exp_p1.txt'

sed -i.bu 's/Generic Property Instrumentation//g' $logFile
sed -i.bu 's/Starting Bounded Model Checking//g' $logFile
sed -i.bu 's/simple slicing removed 0 assignments//g' $logFile
sed -i.bu 's/Unwinding loop .*//g' $logFile

sed -i.bu 's/Generated 0 new VCC(s) along current path segment//g' $logFile
sed -i.bu 's/Generated 0 VCC(s), 0 remaining after simplification//g' $logFile
sed -i.bu 's/size of program expression: //g' $logFile
sed -i.bu 's/Generating GOTO Program//g' $logFile

sed -i.bu 's/Converting//g' $logFile

sed -i.bu 's/Not unwinding loop .*//g' $logFile
sed -i.bu 's/slicing removed //g' $logFile

sed -i.bu 's/Passing problem to propositional reduction//g' $logFile
sed -i.bu 's/converting SSA//g' $logFile
sed -i.bu 's/Running propositional reduction//g' $logFile
sed -i.bu 's/Post-processing//g' $logFile

sed -i.bu 's/aborting path on assume.*//g' $logFile

sed -i.bu '/^$/d' $logFile