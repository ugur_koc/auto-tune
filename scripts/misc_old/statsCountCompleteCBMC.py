#!/usr/bin/env python2.7

import re,os
from subprocess import call
import Utils_ML as mlu
import Utils_Config as cu

#FIX fo the new folder organization

statsFile=open('../stats/stats_part1.txt')
lines = statsFile.read().splitlines()
statsFile.close()

statsFile=open('../stats/stats_part2.txt')
lines.extend(statsFile.read().splitlines())
statsFile.close()

featuresFile='../data-ml/cbmc/cbmc_1000_static_analysis_results.txt'
featureMap = mlu.readFeatures(featuresFile)

def getFeatureVector(cFile):
	assert (featureMap.has_key(cFile)), '{} is not in the feature map'.format(cFile)
	return featureMap[cFile]

domFile='../doms/cbmc/CBMC-21opts-list.txt'
optionNames, optionSettings = cu.readDomFile(domFile)

f = open('../designs/CBMC-t3-CA-new.txt','r')
configLines = f.read().splitlines()
f.close()
designMap = {}
for cLine in configLines:
	designMap[cu.designLine2Config(cLine,optionNames)]=cLine	

cFilesMap = {}
configsMap={}
for line in lines:
	config, cFile, task, result = line.split(',')
	assert (designMap.has_key(config)), 'Config is not in designMap:'+ config

	if not configsMap.has_key(config):
		configsMap[config] = [0,0,0,0,0]

	if not cFilesMap.has_key(cFile):
		cFilesMap[cFile] = [0,0,0,0,0]
	
	if result == '0' and 'true-' in cFile:#TN
		configsMap[config][0] = configsMap[config][0]+1
		cFilesMap[cFile][0] = cFilesMap[cFile][0]+1
	elif result == '10'  and 'false-' in cFile:#TP
		configsMap[config][1] = configsMap[config][1]+1
		cFilesMap[cFile][1] = cFilesMap[cFile][1]+1
	elif result == '0'  and 'false-' in cFile: #FN
		configsMap[config][2] = configsMap[config][2]+1
		cFilesMap[cFile][2] = cFilesMap[cFile][2]+1
	elif result == '10'  and 'true-' in cFile: #FP
		configsMap[config][3] = configsMap[config][3]+1
		cFilesMap[cFile][3] = cFilesMap[cFile][3]+1

	configsMap[config][4]=1+configsMap[config][4]
	cFilesMap[cFile][4] = cFilesMap[cFile][4]+1
	# print cFilesMap[cFile], cFile, result

# for cFile,counts in cFilesMap.iteritems():
#  	complete=counts[0]+counts[1]+counts[2]+counts[3]
#  	score=2.0*counts[0]+counts[1]-32.0*counts[2]-16.0*counts[3]
# 	falseRate=100.0*(counts[2]+counts[3])/(counts[0]+counts[1]+counts[2]+counts[3])
# 	print ','.join([str(x) for x in counts])+','+str(complete)+','+str(score)+','+str(falseRate)+','+','.join(getFeatureVector(cFile))

# print counter
# print len(cFilesMap)

# print len(configsMap)
for config,counts in configsMap.iteritems():
	assert (designMap.has_key(config)), 'Config is not in designMap:'+ config
	complete=counts[0]+counts[1]+counts[2]+counts[3]
	score=2.0*counts[0]+counts[1]-32.0*counts[2]-16.0*counts[3]
	falseRate=0.0 if complete==0 else 100.0*(counts[2]+counts[3])/(counts[0]+counts[1]+counts[2]+counts[3])
	#print ','.join([str(x) for x in counts])+','+str(complete)+','+str(score)+','+str(falseRate)+','+','.join(designMap[config].split(' '))
	print ','.join([str(x) for x in counts])+','+str(complete)+','+str(score)+','+str(falseRate)+','+config