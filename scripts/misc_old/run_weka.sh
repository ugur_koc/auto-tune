#!/bin/bash

#dataDir='/Users/ukoc/sv/data-ml/cbmc/time/'
dataDir='/Users/ukoc/sv/data-ml/cbmc'
split=''
#split='pw-'

declare -a randSeeds=("3486" "5442" "4440" "1008" "1256")
declare -a folds=("1" "2" "3" "4" "5")

for fold in "${folds[@]}"; do
	rawTrainFile=$dataDir/train-$split$fold.arff
	rawTestFile=$dataDir/test-$split$fold.arff

	trainFileP=$dataDir/regression/precision-train-$split$fold.arff
	testFileP=$dataDir/regression/precision-test-$split$fold.arff

	# trainFileT=$dataDir/time-train-$split$fold.arff
	# testFileT=$dataDir/time-test-$split$fold.arff
	# label prediction runs
	echo $trainFileP
	java -cp ~/weka/weka.jar weka.filters.unsupervised.attribute.Remove -R 1,81 -i $rawTrainFile -o $trainFileP
	java -cp ~/weka/weka.jar weka.filters.unsupervised.attribute.Remove -R 1,81 -i $rawTestFile -o $testFileP
	# java -cp ~/weka/weka.jar weka.filters.unsupervised.attribute.Remove -R 34,35 -i $rawTrainFile -o $trainFileT
	# java -cp ~/weka/weka.jar weka.filters.unsupervised.attribute.Remove -R 34,35 -i $rawTestFile -o $testFileT
	#for seed in "${randSeeds[@]}"; do
		# Precision prediction runs
		# java -cp ~/weka/weka.jar weka.classifiers.trees.J48 -c 33 -s $seed -t $trainFileP -T $testFileP \
		# 		-d $dataDir/models/J48-precision-$fold-$seed.mdl
		# java -cp ~/weka/weka.jar weka.classifiers.trees.RandomForest -c 33 -S $seed -t $trainFileP -T $testFileP \
		# 		-d $dataDir/models/RandomForest-precision-$fold-$seed.mdl

		# # Time prediction runs
		# java -cp ~/weka/weka.jar weka.classifiers.trees.M5P -c 1 -s $seed -t $trainFileT -T $testFileT \
		# 		-d $dataDir/models/M5P-time-$fold-$seed.mdl
		# java -cp ~/weka/weka.jar weka.classifiers.trees.RandomForest -c 1 -S $seed -t $trainFileT -T $testFileT \
		# 		-d $dataDir/models/RandomForest-time-$fold-$seed.mdl
		# java -cp ~/weka/weka.jar weka.classifiers.lazy.KStar -c 1 -s $seed -t $trainFileT -T $testFileT \
		# 		-d $dataDir/models/KStar-time-$fold-$seed.mdl
		
		# java -cp ~/weka/weka.jar weka.classifiers.functions.MultilayerPerceptron -c 1 -S $seed -t $trainFile -T $testFile -d $modelFile-MLP.model
		# LinearRegression not working 
		# java -cp ~/weka/weka.jar weka.classifiers.functions.LinearRegression -c 1 -S $seed $trainFile -T $testFile -d $modelFile-LinearRegression.model
		# ZeroR giving 0 acc
		# java -cp ~/weka/weka.jar weka.classifiers.rules.ZeroR -c 1 -s $seed -t $trainFile -T $testFile -d $modelFile-ZeroR.model
	#done
done

#IFS=""
#declare -a saTools=("2ls" "aprove" "cbmc" "cpa-bam-bnb" "cpa-bam-slicing" "cpa-seq" "depthk" "esbmc-incr" "esbmc-kind" "forester" "interpchecker" "map2check" "predatorhp" "skink" "symbiotic" "uautomizer" "ukojak" "utaipan" "veriabs" "viap" "yogar-cbmc")
#declare -a classifiers=("weka.classifiers.trees.J48 -C 0.25 -M 2 -s 1234" "weka.classifiers.bayes.NaiveBayes -s 1234" "weka.classifiers.functions.Logistic -R 1.0E-8 -M -1 -num-decimal-places 4 -s 1234" "weka.classifiers.trees.RandomForest -P 100 -I 100 -num-slots 1 -K 0 -M 1.0 -V 0.001 -S 1234 -s 1234")
# for classifier in "${classifiers[@]}"; do
	# for tool in "${saTools[@]}"; do
	#    	java -cp ~/weka/weka.jar weka.classifiers.trees.RandomForest -P 100 -I 100 -num-slots 1 -K 0 -M 1.0 -V 0.001 -S 1234 -s 1234 -t arff-files/$tool-all.arff
	# 	java -cp ~/weka/weka.jar weka.classifiers.trees.RandomForest -P 100 -I 100 -num-slots 1 -K 0 -M 1.0 -V 0.001 -S 1234 -s 1234 -t arff-files/$tool-no-overflow.arff
	# 	java -cp ~/weka/weka.jar weka.classifiers.trees.RandomForest -P 100 -I 100 -num-slots 1 -K 0 -M 1.0 -V 0.001 -S 1234 -s 1234 -t arff-files/$tool-termination.arff
	# 	java -cp ~/weka/weka.jar weka.classifiers.trees.RandomForest -P 100 -I 100 -num-slots 1 -K 0 -M 1.0 -V 0.001 -S 1234 -s 1234 -t arff-files/$tool-unreach-call.arff
	# 	java -cp ~/weka/weka.jar weka.classifiers.trees.RandomForest -P 100 -I 100 -num-slots 1 -K 0 -M 1.0 -V 0.001 -S 1234 -s 1234 -t arff-files/$tool-valid-dfm.arff
	# done
#done