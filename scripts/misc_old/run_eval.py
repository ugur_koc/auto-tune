#!/usr/bin/env python3.5

import sys, re,os
import subprocess as sp
sys.path.insert(0, '../scripts/')
import Utils_Config as conf

#BASE_DIR='/home/ukoc/sv'
BASE_DIR='..'


domFile = open('{}/doms/cbmc/CBMC-21opts.txt'.format(BASE_DIR), 'r')
options = [opt.split(' ')[0] for opt in domFile.readlines()]
domFile.close()

configsFile='{}/stats/cbmc-config-search-pw.txt'.format(BASE_DIR)

filePrePath='sv-benchmarks/c/'

f2 = open(configsFile,'r')
configs = f2.read().split('\n')
f2.close()

f = open("log_config_search_pw.txt", "w")
for config in configs:
	srcFile, dp = config.split(' :: ')
	dp = dp.split(',')
	config = conf.designLine2Config(','.join(dp[:21]), options)
	srcFile = filePrePath+srcFile
	probFile = filePrePath + dp[33] + ".prp"
	cmd = ['cbmc', '--propertyfile', probFile, srcFile] + config.split(' ')
	try:
		outs = sp.call(cmd, stdout=f, stderr=f)
		f.write(config + ',' + srcFile + ',' + str(outs)+'\n')
	except sp.TimeoutExpired:
		f.write(config + ',' + srcFile + ',timeout\n')
	except:
		f.write(config + ',' + srcFile + ',incomplete\n')
	f.flush()
f.close()