#!/bin/bash

datadir='../data-sv/All-Raw-2019'
wget 'https://sv-comp.sosy-lab.org/2019/results/results-verified/All-Raw.zip';
mkdir $datadir;
mv All-Raw.zip $datadir/;
cd $datadir && unzip All-Raw.zip;

for filename in *.xml.bz2; do
	bzip2 -d $filename;
done

removeStr='xml.bz2.merged.xml'
for filename in *merged.xml; do
	mv -f $filename ${filename%.$removeStr}.xml
done
