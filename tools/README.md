# Configuration Options

This directory should contains the source code of SA tools

All SA tools can be downloaded from: https://sv-comp.sosy-lab.org/2019/systems.php

## CBMC

cbmc 5.11 is available at https://www.cprover.org/cbmc

`wget https://www.cprover.org/cbmc/download/cbmc-5-11-linux-64.tgz`

Place the cbmc-binary executable under cbmc directory and give it executable rights.

Next, install following solvers that cbmc uses;
Yices 2.6.1,
Z3 version 4.4.1.
MathSAT5 version 20181102 (9dddce7e8e79) (Nov 20 2018 09:21:45, gmp 6.1.0, gcc 4.8.5, 64-bit),
boolector 1.5.118,
CVC4 version 1.6,

(boolector and z3 might be available thru packege manager. You can find the binaries for mathsat, yices, and cvc4 on their website).

Last test the following command under the sv-comp directory;

`cbmc --propertyfile sv-benchmarks/c/ReachSafety.prp sv-benchmarks/c/pthread-wmm/rfi010_tso.oepc_false-unreach-call.c`

This should print out __VERIFICATION FAILED__ at the end.

Use the `run_exp.pl` script to run the experiments. You will need to change the paths that are defined in the script.


## Symbiotic

Used 17 configuration options that might affect the analysis run.

## JayHorn

Used 12 configuration options that might affect the analysis run.

## JBMC

Place both jbmc and jbmc-binary executables under jbmc

Similar to cbmc, use the `run_exp.pl` script to run the experiments.